const state = () => {
  return {
    // TEMP AUTHENTICATED MOVE TO MIDDLEWARE OR AUTH STORE
    AUTH_PROCESS: false,
    CART: null,
    ANONYMOUS_CART_ID: '',
    PROCESSING_API_REQUEST: false,
    REQUEST_QUOTE_FLYOUT_ACTIVE: false,
    REQUEST_QUOTE_STEP: 0,
    ORDER_SUCCESS: false,
    ORDER_STATUS: null,
    CHECKING_OUT_ACTIVE: false,
    CART_CHECKOUT_STEP: 0,
    FEEDBACK: null,
    CARTEX: [],
    shipping: null,
    subtotal: null,
    tax: null,
    TOTAL_PRICE: 0,
    handling: null,
    orders: null,
    GUEST_USER: false,
    ANONYMOUS_ID: '',
    GUEST_EMAIL: '',
    orderedItems: [],
    card_choices: [],
    // added
    ACTIVE_FLYOUT_MENU: null, // PARAMS 'REGISTER' ||  'REQUEST_QUOTE'  || 'EMPTY_CART' || 'SIDE_CART'
    CART_RECENTLY_MERGED: false,
    CART_ID: null,
    LINE_ITEMS: [],
    REQUEST_QUOTE_FORM_DATA: {},
    CART_STATE: null, // Merged || Active
    MINIMUM_QUANTITY: [],
    TAX_PRICE: 0,
    SUB_TOTAL_PRICE: 0,
    DISCOUNT_PRICE: 0,
    HANDLING_PRICE: 0,
    SHIPPING_PRICE: 0,
    HAS_QUOTE: false,
    CART_ITEM: [],
    SHOW_QUOTE_FLOW: false, // Gives access FLAG to Quote flow
    TOGGLE_QUOTE_FLOW: false,
    QUOTE_ONLY_PRODUCT: [
      'RP1-US', // product Air pro RX
    ],
    HAS_QUOTE_PRODUCT: false,
    HAS_SUBSCRIPTION_PRODUCT: false,
    SHIPPING_METHOD_NAME: '',
    CART_UPDATE_ERROR: '',
    FREE_SHIPPING_DISCOUNT: false,
    DB_CACHE: {
      products: {},
    },
    CART_LINE_ITEMS_TEALIUM: {},
    FILTER_LINE_ITEMS_TEALIUM: {},
    IS_D2C: false,
    GIFT: false,
    PROMOTION_PRODUCT: '',
    PROMOTION_MESSAGE: {},
    CART_SUBSCRIPTION: {},
  }
}

// Actions
const actions = {
  SET_CART_ID({ commit, state }, payload) {
    if (!payload) {
      return
    }
    commit('SET_CART_ID', payload)
    sessionStorage.setItem('cartId', payload)
  },
  IS_GUEST_USER({ commit, state }, payload) {
    if (!payload) {
      return
    }
    commit('SET_GUEST_USER', payload)
  },
  async CREATE_CART({ commit, state }, payload) {
    // let {
    //   Tealium_visitor_id = null, ShippingAddress = null, InventoryMode = null, TaxMode = null, CustomerGroup = null, Country = null, Locale = null, deleteDaysAfterLastModification = 60
    // } = payload;
    if (state.processing_api_call)
      return commit('SET_FEEDBACK', 'The Last Api Message hasnt finished yet')
    try {
      // SET TO LOADING
      commit('SET_PROCESSING', true)
      // Make call
      const res = await this.$repositories.cart.create(payload)

      if (res.status === 200 && res.data && !res.data.errorMessage) {
        const { data } = res

        commit('SET_ANONYMOUS_CART_ID', data.id || '')
        commit(
          'SET_CART',
          await this.$utility.parseCartForSubscription(this, data)
        )
      } else {
        // Handle error here
      }
    } catch (e) {
      commit('SET_FEEDBACK', e)
    } finally {
      commit('SET_PROCESSING', false)
    }
  },
  async FETCH_CART({ commit, state }, payload) {
    const { cartID = sessionStorage.getItem('cartId') || null } = payload
    if (!cartID) return commit('SET_FEEDBACK', 'Invalid Cart Id.')
    if (state.processing_api_call)
      return commit('SET_FEEDBACK', 'The Last Api Message hasnt finished yet')
    try {
      commit('SET_PROCESSING', true)
      const res = await this.$repositories.cart.get(cartID)
      if (res.status === 200 && res.data) {
        const { data } = res
        // commit('SET_CART', data);
        await commit('SET_ANONYMOUS_CART_ID', data.id || '')
        await commit(
          'SET_CART',
          await this.$utility.parseCartForSubscription(this, data)
        )
      } else {
        // Handle error here
      }
    } catch (e) {
      commit('SET_FEEDBACK', e)
    } finally {
      commit('SET_PROCESSING', false)
    }
  },
  // FETCH_CART_FOR_SUBSCRIPTION
  async FETCH_CART_SUBSCRIPTION({ commit, state }, payload) {
    const { cartID = null } = payload
    const res = await this.$repositories.cart.get(cartID)
    if (res.status === 200 && res.data) {
      const { data } = res
      await commit(
        'SET_CART_SUBSCRIPTION',
        await this.$utility.parseCartForSubscription(this, data)
      )
    }
  },
  async ADD_REMOVE_FROM_CART({ commit, state }, payload) {
    const {
      action = 'ADD_LINE_ITEM',
      lineItemId = '',
      productId = null,
      variantSKU = '',
    } = payload
    const cartID = state.CART_ID

    if (!['SET_CART_AS_QUOTE', 'SET_CUSTOMER_FOR_QUOTE'].includes(action)) {
      // Check if cart id if no cart id create cart than continue checking like this temporarily but must update to one action when we have full user details
      if (!action || (!lineItemId && !productId && !variantSKU) || !cartID)
        return commit('SET_FEEDBACK', 'Unable to update that cart.')
    }
    commit('SET_PROCESSING', true)
    // handled for failing server response (once update-cart API is fixed it will e reverted)
    await this.$repositories.cart
      .update(cartID, payload)
      .then(async (res) => {
        commit('SET_PROCESSING', false)
        const { status, data } = res

        if (status === 200 && data && !data.errorMessage) {
          // commit('SET_CART', data);
          commit(
            'SET_CART',
            await this.$utility.parseCartForSubscription(this, data)
          )
        }
      })
      .catch((e) => {
        console.log(e)
        commit('SET_FEEDBACK', e)
        commit('SET_PROCESSING', false)
      })
  },
  EMPTY_CART({ commit, state }) {
    state.CART = null
    state.CARTEX = []
    state.CART_ID = null
    state.LINE_ITEMS = []
    state.CART_STATE = null
    state.GIFT = false
    // state.TOTAL_PRICE = 0;
    sessionStorage.removeItem('cartId')
  },
  // Removes a full item/product and associated line items from cart
  async UPDATE_CART_ACTION({ commit, state }, payload) {
    console.log(payload, '--payload--')
    const {
      action = '',
      giftMessage = '',
      gift = false,
      recipientsEmail = '',
      shippingAddress = {},
    } = payload
    const cartID = state.CART_ID
    let $data = {}
    // Check if cart id if no cart id create cart than continue checking like this temporarily but must update to one action when we have full user details
    if (!action || !cartID)
      return commit('SET_FEEDBACK', 'Unable to update that cart.')
    // handled for failing server response (once update-cart API is fixed it will e reverted)
    switch (action) {
      case 'SET_SHIPPING_ADDRESS':
        $data = { action, shippingAddress }
        if (gift) {
          $data.giftMessage = giftMessage
          $data.gift = gift
          $data.recipientsEmail = recipientsEmail
        }
        break
      default:
        $data = payload
        break
    }
    let cartUpdated = false
    await this.$repositories.cart
      .update(cartID, $data)
      .then(async (res) => {
        cartUpdated = res.status === 200 && res.data && !res.data.errorMessage
        if (cartUpdated) {
          if (res.data.error) {
            state.CART_UPDATE_ERROR = res.data.error.message
          } else {
            state.CART_UPDATE_ERROR = ''
          }
          !res.data.error &&
            commit(
              'SET_CART',
              await this.$utility.parseCartForSubscription(this, res.data)
            )
        }
        commit('SET_FEEDBACK', '')
      })
      .catch((e) => {
        console.log(e)
        console.log('Getting Error')
        commit('SET_FEEDBACK', e)
      })
    if (cartUpdated) {
      return
    }
    const res = await this.$repositories.cart.get(cartID)
    const { status, data } = res
    if (status === 200 && data && !data.errorMessage) {
      // commit('SET_CART', data);
      commit(
        'SET_CART',
        await this.$utility.parseCartForSubscription(this, data)
      )
    } else {
      // console.log(e)
      // commit('SET_FEEDBACK', e)
    }
  },
  // For Subscription Cart Update
  async UPDATE_CART_ACTION_SUBSCRIPTION({ commit, state }, payload) {
    const { action = '', shippingAddress = {}, cartID = null } = payload
    let $data = {}
    // Check if cart id if no cart id create cart than continue checking like this temporarily but must update to one action when we have full user details
    if (!action || !cartID)
      return commit('SET_FEEDBACK', 'Unable to update that cart.')
    // handled for failing server response (once update-cart API is fixed it will e reverted)
    switch (action) {
      case 'SET_SHIPPING_ADDRESS':
        $data = { action, shippingAddress }
        break
      default:
        $data = payload
        break
    }
    const res = await this.$repositories.cart.update(cartID, $data)
    if (res.status === 200) {
      commit(
        'SET_CART_SUBSCRIPTION',
        await this.$utility.parseCartForSubscription(this, res.data)
      )
    }
  },
  FULL_REMOVE_FROM_CART({ commit }, payload) {
    const {
      action = null,
      lineItemId = null,
      productId = null,
      quantity = 0,
      skuId = null,
    } = payload

    if (!productId || !lineItemId || !action || !quantity || !skuId)
      return commit('SET_FEEDBACK', 'Unable to add to cart.')
  },
  async SUBMIT_ORDER({ commit }, payload) {},
  REQUEST_QUOTE({ commit }, payload) {
    commit('SET_CURRENT_FLYOUT', payload)
  },
  async REGISTER_FOR_QUOTE({ commit }, payload) {},

  async MERGE_CART({ commit, state }) {
    const action = 'MERGE_CART'
    const customerId = this.state.USER_STORE.CUSTOMERID
    const payloadData = { action, customerId }

    if (!action || !customerId || !(state.ANONYMOUS_CART_ID || state.CART_ID)) {
      return // commit('SET_FEEDBACK', 'Unable to Merge the cart.');
    }

    await this.$repositories.cart
      .update(state.ANONYMOUS_CART_ID || state.CART_ID, payloadData)
      .then(async (res) => {
        const { status, data } = res
        if (status === 200 && data && !data.errorMessage && !data.errors) {
          // updates New cartId with customerId
          // commit('SET_CART', data);
          commit(
            'SET_CART',
            await this.$utility.parseCartForSubscription(this, data)
          )
          const hasLineItem = state.LINE_ITEMS.length
          if (hasLineItem) {
            commit('SET_CART_STATE', 'Merged')
          }
        }
      })
      .catch((e) => {
        commit('SET_FEEDBACK', e)
        commit('SET_PROCESSING', false)
      })
  },

  CLEAR_API_SERVICE_ERROR({ commit }) {
    commit('SET_FEEDBACK', '')
  },
  API_SERVICE_ERROR({ commit }, payload) {
    commit('SET_FEEDBACK', payload)
  },
  COLLAPSE_REGISTER_FLYOUT({ commit }, payload) {
    commit('SET_CURRENT_FLYOUT', payload)
  },
  RESET_FLYOUT({ commit }, payload) {
    commit('SET_CURRENT_FLYOUT', payload)
  },
  CONTINUE_REQUEST_QUOTE({ commit, state }, payload) {
    // TODO : PAYLOAD SHOULD INCLUDE ACCOUNT INFO
    // TODO : submit quote request to api if successfull go to anything else we should know else continue
    // TODO : SHOW CART_RECENTLY_MERGED DROPDOWN MESSAGE ON ORDERSUMMARY
    // On Success
    commit('SET_REQ_QUOTE_STEP', payload)
    // DUMMY DELETE ME NEXT WEEK DURING NEXT SPRIN
    if (state.REQUEST_QUOTE_STEP === 3) {
      commit('DELETE_ME_EMPTY_CART')
    }
  },
  RESET_REQUEST_QUOTE({ commit }, payload) {
    commit('SET_CURRENT_FLYOUT', payload)
  },
  DELETE_ME_SET_UI_REGISTER_STEP({ commit }) {
    commit('SET_CURRENT_FLYOUT', 'REGISTER')
  },
  TOGGLE_SIDE_CART({ commit, state }, payload) {
    if (state.ACTIVE_FLYOUT_MENU === 'SIDE_CART') {
      commit('SET_CURRENT_FLYOUT')
    } else if (payload === 'HIDE') {
      commit('SET_CURRENT_FLYOUT')
    } else {
      commit('SET_CURRENT_FLYOUT', 'SIDE_CART')
    }
  },

  CHECK_CART_HAS_QUOTE({ state, commit }) {
    if (state.LINE_ITEMS.length && !this.state.USER_STORE.AUTHENTICATED) {
      commit('SET_CART_ITEM')
      let item, minQty
      const priceObj = {
        id: '',
        quote: false,
        quantity: 0,
        minimumQuantity: 0,
      }

      for (let i = 0; i < state.LINE_ITEMS.length; i++) {
        item = JSON.parse(JSON.stringify(state.LINE_ITEMS[i])) // deep copy
        // item = {...state.LINE_ITEMS[i]}
        priceObj.id = item.id
        if (item.price && item.price.tiers && item.price.tiers.length) {
          item.price.tiers.sort((a, b) => a.minimumQuantity - b.minimumQuantity)
          minQty = item.price.tiers[0].minimumQuantity
          priceObj.minimumQuantity = item.price.tiers[0].minimumQuantity
          // commit('SET_MINIMUM_QTY',{id, minQty:(minQty || null)})
          if (item.quantity >= minQty) {
            priceObj.quote = true
            // commit('SET_QUOTE_FOR_CART', true)
          } else {
            priceObj.quote = false
            // commit('SET_QUOTE_FOR_CART', false)
          }
        } else {
          priceObj.quote = false
          priceObj.minimumQuantity = 0
        }
        commit('SET_CART_ITEM', priceObj)
      }
    }
  },
  TOGGLE_QUOTE_FLOW({ commit }, payload) {
    commit('SET_TOGGLE_QUOTE_FLOW', payload)
  },
  SET_D2C({ commit }, payload) {
    commit('SET_D2C_FLAG', payload)
  },
  HIDE_MSG({ commit }, payload) {
    commit('SET_CART_STATE', payload)
  },
}

// mutations
const mutations = {
  SET_CART_ID(state, payload) {
    state.CART_ID = payload
  },
  SET_CART(state, payload) {
    const {
      id = null,
      totalPrice = null,
      taxedPrice = null,
      custom = null,
      shippingInfo = null,
    } = payload
    if (payload.anonymousId && payload.customerEmail) {
      state.GUEST_EMAIL = payload.customerEmail
      state.CART_STATE = payload
      state.ANONYMOUS_ID = payload.anonymousId
    }
    state.HAS_SUBSCRIPTION_PRODUCT = !!payload.hasSubscription
    if (totalPrice) {
      const { centAmount = 0 } = totalPrice
      state.SUB_TOTAL_PRICE = centAmount
      state.TOTAL_PRICE = centAmount
    }
    state.CART = payload
    window.vCart = payload
    // updating cartId
    if(id){
      state.CART_ID = id
    }
    id && sessionStorage.setItem('cartId', id)

    if (payload.lineItems && !payload.lineItems.length) {
      state.LINE_ITEMS = []
      state.TOTAL_PRICE = 0
      state.CART_STATE = null
      return (state.ACTIVE_FLYOUT_MENU = 'EMPTY_CART')
    }
    if (
      taxedPrice &&
      taxedPrice.totalGross &&
      taxedPrice.totalGross.centAmount
    ) {
      state.TOTAL_PRICE = taxedPrice.totalGross.centAmount
    }
    if (shippingInfo && shippingInfo.shippingMethodName) {
      state.SHIPPING_METHOD_NAME = shippingInfo.shippingMethodName
    }
    if (custom && custom.fields) {
      const {
        subTotal = 0,
        shippingCost = 0,
        handlingCost = 0,
        totalTax = 0,
        totalDiscountPrice = 0,
        orderTotal = 0,
        gift = false,
        promotionMessage = '',
        promotionProduct = {},
      } = custom.fields
      state.SUB_TOTAL_PRICE = parseFloat(subTotal)
      if (state.SHIPPING_METHOD_NAME === 'Contact Carrier') {
        state.SHIPPING_PRICE = 'Contact carrier'
      } else if (
        state.SHIPPING_METHOD_NAME === 'FedEx - Ground-Business Delivery' &&
        shippingInfo &&
        shippingInfo.price.centAmount === 0
      ) {
        state.SHIPPING_PRICE = 'Free ground'
      } else {
        if (
          custom.fields.freeShippingDiscount &&
          custom.fields.freeShippingDiscount > 0
        ) {
          state.FREE_SHIPPING_DISCOUNT = true
        }
        state.SHIPPING_PRICE = parseFloat(shippingCost)
      }
      state.HANDLING_PRICE = parseFloat(handlingCost)
      state.TAX_PRICE = parseFloat(totalTax)
      state.DISCOUNT_PRICE = parseFloat(totalDiscountPrice)
      state.TOTAL_PRICE = parseFloat(orderTotal)
      state.GIFT = gift
      state.PROMOTION_MESSAGE = promotionMessage
      state.PROMOTION_PRODUCT = promotionProduct
    }
    this.$utility.quoteFlowCheck(
      this.state.USER_STORE.AUTHENTICATED,
      state.SHOW_QUOTE_FLOW,
      'LINE_ITEM',
      payload.lineItems,
      this
    )
    if (payload.lineItems && payload.lineItems.length) {
      for (let i = 0; i < payload.lineItems.length; i++) {
        const lineItem = payload.lineItems[i]
        lineItem.quoteOnlyProduct = false
        const productSku = (lineItem.variant && lineItem.variant.sku) || ''
        if (!productSku) {
          continue
        }
        if (
          state.QUOTE_ONLY_PRODUCT.includes(productSku) &&
          !this.state.USER_STORE.AUTHENTICATED
        ) {
          lineItem.quoteOnlyProduct = true
        }
        payload.lineItems[i] = lineItem
      }
    }
    state.LINE_ITEMS = payload.lineItems
    state.CART_LINE_ITEMS_TEALIUM = this.$utility.setCartTealium(
      this,
      state.LINE_ITEMS,
      state.TOTAL_PRICE,
      state.CART_ID
    )
    state.FILTER_LINE_ITEMS_TEALIUM = this.$utility.setFilterTealium(
      this,
      state.LINE_ITEMS
    )
  },
  SET_PROCESSING(state, payload) {
    state.FEEDBACK = null
    state.PROCESSING_API_REQUEST = payload
  },
  SET_FEEDBACK(state, payload) {
    state.FEEDBACK = payload
  },
  UPDATE_CART(state, payload) {
    state.CART = Object.assign({}, state.CART, payload)
  },
  SET_CURRENT_FLYOUT(state, payload) {
    if (!payload) return (state.ACTIVE_FLYOUT_MENU = null)
    state.ACTIVE_FLYOUT_MENU = payload
  },
  SET_REQ_QUOTE_STEP(state, payload) {
    if (!state.REQUEST_QUOTE_STEP === payload) return
    state.REQUEST_QUOTE_STEP = payload
  },
  RESET_REQ_QUOTE(state) {
    state.REQUEST_QUOTE_FORM_DATA = Object.assign(
      {},
      state.REQUEST_QUOTE_FORM_DATA
    )
  },

  DELETE_ME_EMPTY_CART(state) {
    state.CART = null
    state.CARTEX = []
  },

  QUOTE_FLYOUT(state, payload) {
    const addQuote = { ...payload, needs_quote: true }
    state.CARTEX.push(addQuote)
  },
  SET_CART_STATE(state, payload) {
    state.CART_STATE = payload
  },
  DEMO_WITH_QUOTE_FLYOUT(state) {
    const dummyobject = {
      // unique id of item tied to db to handle in stock checking, and data validation, and vue binding etc.
      sku: '1254125125',
      // optionally add category to handle similar suggestions, etc (optional)
      item_category: 'filters',
      // Name of the item
      title: 'air',
      // Url to image of item
      image: 'https://images.prismic.io/molekule/83d64275-4571-4c8e-80b8.png',
      // description of item (optional)
      description: 'Best air pro filter on the market',
      // array of items to appear at bottom of card in list form (optionally construct logic based on above details)
      additional_details: ['Auto-refills', '1 PECO-filter', '$49 every 6mos.'],
      // price of an item in pennies for stripe n(quantity)
      price: 330000,
      // discount price of an item in pennies for stripe n(quantity)
      discount_price: 280000,
      // quantity of item
      qty: 5,
      // Filter object if included
      filter: {
        // product id of filter
        sku: 'id22',
        // url img of filter
        image: 'image of filter',
        // plain text version of filter (optional)
        title: 'Includes a 6mos. of filters',
        // price of filter in pennies for stripe n(qty)
        price: 65000,
        // months || years || weeks
        time_type: 'months',
        // length of how long filter lasts
        time_value: 6,
      },
      // if has auto refils
      auto_refils: true,
      // if includes filter
      includes_filter: true,
      // if item needs quote
      needs_quote: true,
      // if item has discount
      has_discount: false,
    }
    state.CARTEX.push(dummyobject)
  },
  SET_MINIMUM_QTY(state, minValue) {
    state.MINIMUM_QUANTITY.push(minValue)
  },
  SET_QUOTE_FOR_CART(state, hasQuote) {
    state.HAS_QUOTE = hasQuote
  },
  SET_CART_ITEM(state, itemObj) {
    !itemObj && (state.CART_ITEM = [])
    state.CART_ITEM.push({ ...itemObj })
  },
  SET_TOGGLE_QUOTE_FLOW(state, payload) {
    state.TOGGLE_QUOTE_FLOW = payload
  },
  SET_QUOTE_ONLY_PRODUCT(state, payload) {
    state.HAS_QUOTE_PRODUCT = payload
  },
  SET_AUTH_PROCESS(state, payload) {
    state.AUTH_PROCESS = payload
  },
  SET_ANONYMOUS_CART_ID(state, payload) {
    state.ANONYMOUS_CART_ID = payload
  },
  SET_D2C_FLAG(state, payload) {
    state.IS_D2C = payload
  },
  SET_GUEST_USER(state, payload) {
    state.GUEST_USER = payload
  },
  SET_CART_SUBSCRIPTION(state, payload) {
    state.CART_SUBSCRIPTION = payload
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
}
