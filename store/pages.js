import isEmpty from 'lodash/isEmpty';
import { processRegularPrismicDoc } from '@/custom-apis/pages/utils';
import { toPrLocale } from '@/utils/store-helpers';

export const state = () => ({
	careersPagePostings: [],
});

export const actions = {
	async FETCH_PAGE_DATA({ commit, state, getters, rootGetters, rootState }, { pageName, req }) {
		if (isEmpty(state[pageName])) {
			const [uid, type] = pageName.split('/').reverse();
			const { standardLocale: locale } = rootGetters;
			const prLocale = toPrLocale(locale);
			const options = process.server ? { headers: { Cookie: req.headers.cookie || '' } } : {};
			const pageData = await (process.server
				? processRegularPrismicDoc(
						{ url: req.url, params: { locale }, headers: req.headers, prismicUid: uid },
						await req.locals.anattaPrismicRequest(`/api/${locale}/${type ? `${type}/` : ''}${uid}.json`)
				  )
				: this.$axios.$get(`/custom-apis/pages/${locale}/${type ? `${type}/` : ''}${uid}`, options));

			commit('SET_PAGE_DATA', { pageName, pageData });

			if (pageData.reviewsSnippets) {
				pageData.reviewsSnippets.forEach(
					(snippet) =>
						isEmpty(rootState.reviews.prSnippets[prLocale][snippet.page_id]) &&
						commit(
							'reviews/SET_POWERREVIEWS_SNIPPET',
							{ locale: prLocale, pageId: snippet.page_id, snippet },
							{ root: true }
						)
				);
			}

			return Promise.resolve(pageData);
		}

		return Promise.resolve(state[pageName]);
	},

	async FETCH_CAREER_PAGE_DATA({ commit, state }) {
		if (isEmpty(state.careersPagePostings)) {
			const postings = await this.$axios.$get('/custom-apis/careers-postings');

			commit('SET_CAREER_PAGE_DATA', postings);
			return Promise.resolve(postings);
		}

		return Promise.resolve(state.careersPagePostings);
	},
};

export const mutations = {
	SET_PAGE_DATA: (state, { pageName, pageData }) => (state[pageName] = pageData),

	SET_CAREER_PAGE_DATA: (state, postings) => (state.careersPagePostings = postings),
};
