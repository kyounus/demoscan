const state = () => {
	return {
		STEP: 1,
		SHIPPING_METHODS: {},
		SHOW_PAGE_SPINNER: false,
		SURVEY_QUESTIONS: null,
		INACTIVE_SUBSCRIPTION: {},
		FEATURE_AFFIRM: true,
		FEATURE_GIFT: true,
		FEATURE_CLYDE: true,
		FEATURE_SUBSCRIPTION: true,
	};
};

// Actions
const actions = {
	UPDATE_STEP({ commit }, payload) {
		commit('UPDATE_STEP', payload);
	},
	async API_BILLING_INFO_UPDATE({ commit }, payload) {
		if (state.API_CALL_IN_PROGRESS) return commit('SET_FEEDBACK', 'The Last Api Message hasnt finished yet');
		const requestPayload = {};
		const { carrier_name: carrierName, carrier_account_number: carrierAccountNumber } = payload;
		if (payload.both === true) {
			this.deliveryType = 'BOTH';
		} else {
			this.deliveryType = payload.delivery_type;
		}
		requestPayload.carriers = [
			{
				billingAddress: {},
				carrierName,
				carrierAccountNumber,
				type: this.deliveryType,
				defaultCarrier: false,
			},
		];
		requestPayload.carriers[0].billingAddress = this.$utility.makeAddressModel(
			payload.carrier_billing_address,
			'carrier_billing_address_'
		);

		if (this.state.USER_STORE.CUSTOMERID) {
			requestPayload.customerId = this.state.USER_STORE.CUSTOMERID;
		}
		await this.$repositories.user
			.update_carrier(requestPayload)
			.then((res) => {})
			.catch((e) => {
				// console.log(e)
			});
	},
	async GET_SHIPPING_METHODS({ commit, state }) {
		const cartId = sessionStorage.getItem('cartId') || null;

		if (cartId) {
			const res = await this.$repositories.checkout.getShippingMethods(cartId);
			if (res.status === 200 && res.data) {
				const { data } = res;
				commit('SET_SHIPPING_METHODS_RESPONSE', data);
			} else {
				// Handle error here
			}
		}
	},
	async REMOVE_SUBSCRIPTION() {
		const cartData = this.state.CART_STORE.CART;
		let payload = {};
		if (cartData.hasSubscription && cartData.lineItems.length) {
			for (const item of cartData.lineItems) {
				if (item.cartItemHasSubscription && item.cartItemSubscriptionData && item.cartItemSubscriptionData.selected) {
					payload = {
						action: 'REMOVE_LINE_ITEM',
						productId: item.productId || null,
						subscriptionEnabled: true,
						quantity: item.quantity,
					};
					await this.dispatch('CART_STORE/UPDATE_CART_ACTION', payload);
				}
			}
		}
	},
	async LOAD_SURVEYS({ commit, state }) {
		const res = await this.$repositories.checkout.getCheckoutSurveys();
		if (res.status === 200 && res.data) {
			const { data } = res;
			commit('SET_SURVEYS', data);
		} else {
			// Handle error here
		}
	},
	async GET_INACTIVE_SUBSCRIPTION({ commit, state, dispatch }, payload) {
		const res = await this.$repositories.user.getInactiveSubscription(payload);
		if (res.status === 200 && res.data) {
			const { data } = res;
			commit('SET_INACTIVE_SUBSCRIPTION', data);
		} else {
			commit('SET_INACTIVE_SUBSCRIPTION', []);
			// Handle error here
		}
	},
	CHECK_LOCALE_FOR_FEATURE({ commit, state, dispatch, rootState }) {
		const { isEurope } = rootState;
		if (isEurope) {
			commit('SET_FEATURE_LIST', false);
		} else {
			commit('SET_FEATURE_LIST', true);
		}
	},
};

// mutations
const mutations = {
	SET_PROCESSING(state, payload) {
		state.FEEDBACK = null;
		state.PROCESSING_API_REQUEST = payload;
	},
	SET_FEEDBACK(state, payload) {
		state.FEEDBACK = payload;
	},
	UPDATE_STEP(state, payload) {
		state.STEP = payload;
	},
	UPDATE_CHECKOUT_INPUT(state, payload) {
		const { key = null, value = null } = payload;
		if (!key) return null;
		state[key] = value;
	},
	SET_SHIPPING_METHODS_RESPONSE(state, payload) {
		state.SHIPPING_METHODS = payload;
	},
	RESET_CHECKOUT(state) {
		state.STEP = 1;
		state.SHIPPING_METHODS = {};
	},
	TOGGLE_PAGE_SPINNER(state) {
		state.SHOW_PAGE_SPINNER = !state.SHOW_PAGE_SPINNER;
	},
	SET_SURVEYS(state, payload) {
		state.SURVEY_QUESTIONS = payload.value;
	},
	SET_INACTIVE_SUBSCRIPTION(state, payload) {
		state.INACTIVE_SUBSCRIPTION = payload;
	},
	SET_FEATURE_LIST(state, payload) {
		state.FEATURE_AFFIRM = payload;
		state.FEATURE_GIFT = payload;
		state.FEATURE_CLYDE = payload;
		state.FEATURE_SUBSCRIPTION = payload;
	},
};

export default {
	namespaced: true,
	state,
	actions,
	mutations,
};
