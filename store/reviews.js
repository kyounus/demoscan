import isEmpty from 'lodash/isEmpty';
import { fromEntries } from '@/utils';
import { toPrLocale } from '@/utils/store-helpers';

const formatProductReview = (review) => ({
	results: review.results[0].reviews,
	rollup: review.results[0].rollup,
	pageId: review.results[0].page_id,
	paging: review.paging,
});

export const state = () => ({
	prSnippets: {
		en_US: {},
		en_CA: {},
	},
});

export const actions = {
	async FETCH_PRODUCT_REVIEW_SNIPPET({ commit, state, rootGetters }, { pageId }) {
		const { standardLocale } = rootGetters;
		const locale = toPrLocale(standardLocale);
		if (isEmpty(state.prSnippets[locale][pageId])) {
			const { readApiKey, merchantIdUS, merchantIdCA } = this.$config.powerReviews;
			const merchantId = locale === 'en_CA' ? merchantIdCA : merchantIdUS;
			try {
				let snippet = await this.$axios.$get(
					`/custom-apis/power-reviews/m/${merchantId}/l/${locale}/product/${pageId}/snippet?apikey=${readApiKey}`
				);

				snippet = snippet.results[0];

				commit('SET_POWERREVIEWS_SNIPPET', { locale, pageId, snippet });
				return Promise.resolve(snippet);
			} catch (err) {
				console.error(err.message);
			}
		}

		return Promise.resolve(state.prSnippets[locale][pageId]);
	},

	async FETCH_PRODUCT_REVIEWS_SNIPPETS({ dispatch }) {
		return await Promise.all([
			dispatch('FETCH_PRODUCT_REVIEW_SNIPPET', { pageId: 'molekule-air' }),
			dispatch('FETCH_PRODUCT_REVIEW_SNIPPET', { pageId: 'molekule-air-mini' }),
			dispatch('FETCH_PRODUCT_REVIEW_SNIPPET', { pageId: 'molekule-air-mini-plus' }),
			dispatch('FETCH_PRODUCT_REVIEW_SNIPPET', { pageId: 'molekule-air-pro' }),
		]);
	},

	async FETCH_PRODUCT_REVIEWS({ state, rootGetters }, config) {
		if (!Array.isArray(config)) config = [config];
		const { readApiKey, merchantIdUS, merchantIdCA } = this.$config.powerReviews;
		const { standardLocale } = rootGetters;
		const locale = toPrLocale(standardLocale);
		const merchantId = locale === 'en_CA' ? merchantIdCA : merchantIdUS;
		const urls = config.map(
			({ pageId, sort = 'HighestRating', pageFrom = 0, pageSize = 8 }) =>
				`/custom-apis/power-reviews/m/${merchantId}/l/${locale}/product/${pageId}/reviews?apikey=${readApiKey}&sort=${sort}&paging.from=${pageFrom}&paging.size=${pageSize}`
		);

		try {
			const reviews = await Promise.all(urls.map((url) => this.$axios.$get(url)));

			return reviews.length === 1
				? formatProductReview(reviews[0])
				: fromEntries(config.map((c, i) => [c.pageId, formatProductReview(reviews[i])]));
		} catch (err) {
			console.error(err.message);
		}
	},
};

export const mutations = {
	SET_POWERREVIEWS_SNIPPET: (state, { locale, pageId, snippet }) => {
		state.prSnippets = {
			...state.prSnippets,
			[locale]: {
				...state.prSnippets[locale],
				[pageId]: snippet,
			},
		};
	},
};

export const getters = {
	reviewsSnippetsById: ({ prSnippets }, _, __, { standardLocale }) => prSnippets[toPrLocale(standardLocale)],
};
