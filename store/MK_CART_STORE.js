const state = () => {
  return {
    ACTIVE_FLYOUT_MENU: null,
    IS_A_GIFT: false,
  }
}
const actions = {
  TOGGLE_SIDE_CART({ commit, state }, payload) {
    console.log(payload)
    if (payload === 'MK_SIDE_CART') {
      commit('SET_CURRENT_FLYOUT', 'MK_SIDE_CART')
    } else if (payload === 'HIDE') {
      commit('SET_CURRENT_FLYOUT')
    } else {
      commit('SET_CURRENT_FLYOUT', 'SIDE_CART')
    }
  },
  GIFT({ commit, state }, payload) {
    commit('SET_IS_GIFT', payload)
  },
}
const mutations = {
  SET_CURRENT_FLYOUT(state, payload) {
    if (!payload) return (state.ACTIVE_FLYOUT_MENU = null)
    state.ACTIVE_FLYOUT_MENU = payload
  },
  SET_IS_GIFT(state, payload) {
    state.IS_A_GIFT = payload
  },
}
export default {
  namespaced: true,
  state,
  actions,
  mutations,
}
