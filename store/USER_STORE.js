/* global getPaymentIntent */
import { Auth } from 'aws-amplify';

// You can get the current config object
Auth.configure({
	// REQUIRED only for Federated Authentication - Amazon Cognito Identity Pool ID
	identityPoolId: process.env.aws_identity_pool,

	// REQUIRED - Amazon Cognito Region
	region: process.env.aws_region,

	// OPTIONAL - Amazon Cognito User Pool ID
	userPoolId: process.env.aws_userpool,

	// OPTIONAL - Amazon Cognito Web Client ID (26-char alphanumeric string)
	userPoolWebClientId: process.env.aws_web_client_id,
	// cookieStorage: {
	//   // REQUIRED - Cookie domain (only required if cookieStorage is provided)
	//   domain: 'localhost:3000',
	//   // OPTIONAL - Cookie path
	//   path: '/',
	//   // OPTIONAL - Cookie expiration in days
	//   expires: 365,
	//   // OPTIONAL - See: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie/SameSite
	//   //   sameSite: "lax",
	//   // OPTIONAL - Cookie secure flag
	//   // Either true or false, indicating if the cookie transmission requires a secure protocol (https).
	//   secure: false
	// },
});

const state = () => {
	return {
		AUTHENTICATED: false,
		USER_GROUPS: [],
		USER_ATTRIBUTES: null,
		ACCOUNTID: null,
		CUSTOMERID: null,
		CUSTOMER_EMAIL: null,
		USER: {},
		USER_NAME: null,
		SIGN_IN_ERROR: null,
		RESET_PASSWORD_ERROR: null,
		CHANGE_PW_ERROR: null,
		FORGOT_PASSWOERD_ERROR: null,
		COG_FORM_LOADING: false,
		CUSTOMER: '',
		COMPANY_NAME: '',
		USER_DISPLAY_NAME: '',
		SHIPPING_ADDRESSES: [],
		BILLING_ADDRESSES: [],
		ADDRESS: [],
		PAYMENTS: [],
		DEFAULT_PAYMENT_METHOD: 'CREDITCARD',
		DEFAULT_PAYMENT_DETAIL: {},
		PAYMENT_OBJ: {},
		PAYMENT_FORM_DATA: {},
		SALES_REPRESENTATIVE: {},
		SHIPPING_METHODS: [],
		TAX_EXCEPTION_LIST: [],
		CREDIT_CARD_DETAILS: [],
		PAYMENT_ACH_ID: '',
		PAYMENT_TERMS_ID: '',
		PAYMENT_TERMS_BUSINESS: {},
		ADD_NEW_USER_RESPONSE: [],
		CARRIERS_LIST: [],
		DEFAULT_CARRIERID: '',
		DEFAULT_ADDRESS: {},
		DEFAULT_SHIPPING_ADDRESS: {},
		DEFAULT_SHIPPING_ID: '',
		ADDITIONAL_FREIGHT_INFO: [],
		SHIPPING_RESPONSE: [],
		CREDIT_BILLING_RESPONSE: [],
		ADD_PAYMENT_RESPONSE: [],
		ORDER: {},
		ORDERS: {},
		USER_PROFILE: {},
		CUSTOMER_PROFILE: {},
		CUSTOMER_DETAILS_TEALIUM: {},
		CUSTOMER_DETAILS_D2C_TEALIUM: {},
		INVOICE_RESPONSE: {},
		API_RESPONSE_WAIT: false,
		SUBSCRIPTIONS: [],
		PROFILE_RESPONSE: {},
		SURVEYS: [],
		DASHBOARD: {},
		CART_ID: '',
		SUBSCRIPTION_RESPONSE_WAIT: false,
		SUBSCRIPTION_RESPONSE: {},
		ACCOUNT_RESPONSE_WAIT: false,
		USER_STATUS_DATA: {},
		USER_EXIST: false,
		ORDERS_LOADED: false,
		ORDER_LOADED: false,
		FORGOT_PASSWORD_RESPONSE: null,
		CUSTOMER_SHIPPING_ADDRESS: [],
		CUSTOMER_BILLING_ADDRESS: [],
		CUSTOMER_CREDIT_CARD_DETAILS: [],
		RECENT_SHIPPING: null,
		ACCESS_TOKEN: '',
		RECENT_PAYMENT: null,
		CUSTOMER_DATA: {},
		DEVICE: {},
		DEVICE_ERROR_MSG: '',
		PASSWORD_ERROR: null,
		VALID_ADDRESS: true,
		ACCEPT_CONDITION: {
			termsOfService: false,
			marketingOffers: false,
		},
	};
};

/**
 * Getters
 */
const getters = {
	CURRENT_USER: async ({ commit }) => {
		try {
			const currentUser = await Auth.currentAuthenticatedUser();
			await commit('SET_USER', currentUser);
			return currentUser;
		} catch (error) {
			return error;
		}
	},
	AUTHENTICATED: (state) => {
		return state.AUTHENTICATED;
	},
	CUSTOMERID: (state) => {
		return state.CUSTOMERID;
	},
	ACCOUNT_DATA: (state) => {
		return state.ACCOUNT_DATA;
	},
	AUTH_ERROR_MESSAGE: (state) => {
		return state.AUTH_ERROR_MESSAGE;
	},
	USER_NAME: (state) => {
		return state.USER_NAME;
	},
	USER: (state) => {
		return state.USER;
	},
	PASSWORD_RECOVERY_DETAILS: (state) => {
		return state.PASSWORD_RECOVERY_DETAILS;
	},
	AUTH_MODULE_STATE: (state) => {
		return state.AUTH_MODULE_STATE;
	},
	LATEST_AUTH_MODULE_STATE: (state) => {
		return [...state.AUTH_MODULE_STATE].pop();
	},
	FORGOT_PASSWORD_ERROR: (state) => {
		return state.FORGOT_PASSWORD_ERROR;
	},
	CodeDeliveryDetails: (state) => {
		return state.CodeDeliveryDetails;
	},
	USER_ATTRIBUTES: (state) => {
		return state.USER_ATTRIBUTES;
	},
	USER_GROUPS: (state) => {
		return state.USER_GROUPS;
	},
};

// Actions
const actions = {
	// start cognito changes
	async USER_LOGGED_STATE({ commit, dispatch }, user) {
		try {
			if (!user) {
				const cartId = sessionStorage.getItem('cartId');
				if (cartId) {
					await this.dispatch('CART_STORE/SET_CART_ID', cartId);
				}
				return; // logged out reset the user
			}
			await commit('SET_USER', user);
			await dispatch('FETCH_CUSTOMER');
			// await dispatch('GET_C_PROFILE')
		} catch (e) {
			return e;
		}
	},
	SET_GUEST_DATA({ commit }) {
		const cartId = sessionStorage.getItem('cartId');
		if (cartId) {
			// Set Guest user's CART ID from browser session;
			commit('SET_CART_ID', cartId);
		}
	},
	async GET_C_PROFILE({ commit, state }) {
		try {
			const accountId = state.ACCOUNTID;
			const customerId = state.CUSTOMERID;

			if (!accountId || !customerId) return;
			const res = await this.$repositories.account.getProfileInfo(accountId, customerId);

			if (res.status === 200 && res.data) {
				const { data } = res;
				commit('SET_PROFILE', data[0]);
				// Set user's CART ID from CT's persistent cart;
				if (data[0].cartId) {
					commit('SET_CART_ID', data[0].cartId);
					this.dispatch('CART_STORE/SET_CART_ID', data[0].cartId);
				}
			}
			return;
		} catch (error) {
			console.log('GET_C_PROFILEerror', error);
			return error;
		}
	},

	async LOGOUT({ dispatch, state, commit }, payload) {
		try {
			await Auth.signOut({
				global: true,
			});
			commit('RESET_USER');
			this.dispatch('CART_STORE/EMPTY_CART');
			localStorage.removeItem('token');
			state.CUSTOMERID = null;
			state.AUTHENTICATED = false;
		} catch (e) {
			console.log('error logging out TODO', e);
		} finally {
			if (payload && payload.path) {
				if (payload.path.includes('D2C/')) {
					this.$router.push(`/${payload.path}`);
				} else if (payload.path.includes('/shop')) {
					this.$router.push('/shop');
				} else {
					this.$router.push(`/${payload.path}/?from=reset`);
				}
			} else {
				window && window.location.replace('/login/');
			}
		}
	},
	async ACCOUNT_SIGN_IN({ commit, state, dispatch }, payload) {
		let msg = 'fail';
		if (state.COG_FORM_LOADING) return;
		commit('SET_COG_FORM_LOADING');
		commit('SET_SIGN_IN_ERROR');
		try {
			const { email, password } = payload;
			if (!email || !password) return commit('SET_SIGN_IN_ERROR', 'Please fill in both your email and password.');
			await Auth.signIn(email, password)
				.then(async (accountData) => {
					msg = 'pass';
					await commit('SET_USER', accountData);
					await dispatch('FETCH_CUSTOMER');
					// await dispatch('GET_C_PROFILE', accountData);
				})
				.catch((err) => {
					commit('SET_SIGN_IN_ERROR', err);
					if (err.code === 'NotAuthorizedException') {
						msg = 'login fails';
					} else if (err.code === 'UserLambdaValidationException') {
						if (err.message.search('maximum login attempts') !== -1) {
							msg = 'locked';
						} else {
							msg = 'invalid credentials';
						}
					}
				});

			return msg;
		} catch (e) {
			commit('SET_SIGN_IN_ERROR', e);
		} finally {
			commit('SET_COG_FORM_LOADING');
		}
	},
	async SIGN_IN({ commit, state, dispatch }, payload) {
		const userData = {
			attributes: {},
		};
		let msg = 'fail';
		if (state.COG_FORM_LOADING) return;
		commit('SET_COG_FORM_LOADING');
		commit('SET_SIGN_IN_ERROR');
		try {
			const { email, password } = payload;
			console.log(payload);
			if (!email || !password) return commit('SET_SIGN_IN_ERROR', 'Please fill in both your email and password.');
			await this.$repositories.user
				.signIn(payload)
				.then(async (accountData) => {
					msg = 'pass';
					if (accountData.data.__type) {
						const errData = accountData.data.__type;
						const errMsg = accountData.data.message;
						commit('SET_SIGN_IN_ERROR', {
							message: errMsg.includes('Incorrect username') ? 'Invalid login or password.' : '',
						});

						if (errData.includes('NotAuthorizedException')) {
							msg = 'login fails';
						} else if (errData.includes('UserLambdaValidationException')) {
							msg = 'locked';
						}
						return msg;
					} else {
						for (const val of accountData.data.userAttributes.UserAttributes) {
							userData.attributes[val.Name] = val.Value;
						}
						userData.attributes.accessToken = accountData.data.tokens?.AuthenticationResult?.AccessToken;
						localStorage.setItem('token', JSON.stringify(accountData.data.tokens));
						await commit('SET_USER', userData);
						await dispatch('FETCH_CUSTOMER');
					}
				})
				.catch((error) => {
					const errData = error.response.data;
					const errMsg = errData.includes('Incorrect username') ? 'Incorrect username or password.' : '';
					commit('SET_SIGN_IN_ERROR', { message: errMsg });

					if (errData.includes('NotAuthorizedException')) {
						msg = 'login fails';
					} else if (errData.includes('UserLambdaValidationException')) {
						msg = 'locked';
					}
				});
			return msg;
		} catch (e) {
			commit('SET_SIGN_IN_ERROR', e);
		} finally {
			commit('SET_COG_FORM_LOADING');
		}
	},
	async SIGN_OUT({ commit, state }, payload) {
		const { path = '/shop' } = payload;
		try {
			const res = await this.$repositories.user.signOut({ accessToken: state.ACCESS_TOKEN });
			if (res.status === 200) {
				commit('RESET_USER');
				this.dispatch('CART_STORE/EMPTY_CART');
				localStorage.removeItem('token');
				state.CUSTOMERID = null;
				state.AUTHENTICATED = false;
				state.ACCESS_TOKEN = '';
				this.$router.push(`${path}`);
			}
		} catch (exp) {}
	},
	async CHECK_USER_EXISTS({ commit, state, dispatch }, payload) {
		const { email, password } = payload;

		if (!email || !password) {
			return;
		}

		await Auth.signIn(email, password).then((accountData) => {
			console.log('USER SIGNED IN');
		});
	},

	async FORGOT_PW_START({ commit, state }, username) {
		if (state.COG_FORM_LOADING) return;
		commit('SET_COG_FORM_LOADING');
		let message;
		try {
			if (!username) {
				return commit('SET_FORGOT_PASSWORD_ERROR', 'Please fill in both your email and password.');
			}
			await Auth.forgotPassword(username);
			commit('SET_FORGOT_PASSWORD_ERROR');
			commit('RESET_COG_FORMS');
			message = 'success';
			return message;
		} catch (e) {
			commit('SET_FORGOT_PASSWORD_ERROR', e);
			message = e.message;
			return message;
		} finally {
			commit('SET_COG_FORM_LOADING');
		}
	},
	async FORGOT_PASSWORD_NW({ commit, state }, email) {
		let msg = 'fail';
		await this.$repositories.user
			.forgotPassword(email)
			.then((res) => {
				if (res.data) {
					const { data } = res;
					if (data.__type) {
						const errMsg = data.message;
						msg = 'success';
						commit('SET_PASSWORD_ERROR', errMsg);
					} else {
						msg = 'fail';
						commit('SET_FORGOT_PASSWORD_RESPONSE', data);
					}
				}
			})
			.catch((error) => {
				const errData = error.response;
				const { data } = errData;
				if (data.__type) {
					const errMsg = data.message;
					msg = 'fail';
					commit('SET_PASSWORD_ERROR', errMsg);
				}
			});
		return msg;
	},
	async RESET_PASSWORD({ commit, state }, payload) {
		let msg = 'fail';
		await this.$repositories.user
			.resetPassword(payload)
			.then((res) => {
				if (res.data) {
					const { data } = res;
					if (data.__type) {
						const errMsg = data.message;
						msg = 'fail';
						commit('SET_PASSWORD_ERROR', errMsg);
					} else {
						msg = 'success';
					}
				}
			})
			.catch((error) => {
				const errData = error.response;
				const { data } = errData;
				if (data.__type) {
					const errMsg = data.message;
					msg = 'fail';
					commit('SET_PASSWORD_ERROR', errMsg);
				}
			});
		return msg;
	},
	async FORGOT_PASSWORD({ commit, state }, payload) {
		let message;
		if (state.COG_FORM_LOADING) return;
		commit('SET_COG_FORM_LOADING');
		try {
			const { username, code, new_password: newPasword } = payload;
			if (!username || !code || !newPasword)
				return commit('SET_FORGOT_PASSWORD_ERROR', 'Please fill in both your email and password.');
			await Auth.forgotPasswordSubmit(username, code, newPasword);
			commit('SET_FORGOT_PASSWORD_ERROR');
			commit('RESET_COG_FORMS');
			message = 'success';
			return message;
		} catch (e) {
			commit('SET_FORGOT_PASSWORD_ERROR', e);
			message = e.message;
			if (e.code === 'ExpiredCodeException') {
				message = 'Reset password link expired, please request again';
			}
			return message;
		} finally {
			commit('SET_COG_FORM_LOADING');
		}
	},
	async CHANGE_PASSWORD({ commit, state }, payload) {
		let message;
		if (state.COG_FORM_LOADING) return;
		commit('SET_COG_FORM_LOADING');
		try {
			const user = await Auth.currentAuthenticatedUser();
			const { oldPassword, newPassword } = payload;
			if (!oldPassword || !newPassword) return commit('SET_CHANGE_PW_ERROR', 'Please fill in the required fields.');
			await Auth.changePassword(user, oldPassword, newPassword);
			commit('SET_CHANGE_PW_ERROR');
			commit('RESET_COG_FORMS');
			message = 'success';
			return message;
		} catch (e) {
			commit('SET_CHANGE_PW_ERROR', e);
			message = e.message;
			return message;
		} finally {
			commit('SET_COG_FORM_LOADING');
		}
	},
	// end cognito changes
	async FETCH_ACCOUNT({ state }) {
		if (state.AUTHENTICATED) {
			await this.dispatch('USER_STORE/GET_ACCOUNT', {
				accountId: state.ACCOUNTID,
				customerId: state.CUSTOMERID,
			});
		}
	},
	async GET_ACCOUNT({ commit, state }, payload) {
		const { accountId = '', customerId = '' } = payload;
		if (state.AUTHENTICATED) {
			console.log('FETCH ACCOUNT DETAILS');
			commit('SET_ACCOUNT_RESPONSE_WAIT', true);
			const res = await this.$repositories.checkout.getAccount(accountId, customerId);
			if (res.status === 200 && res.data) {
				const { data } = res;
				commit('SET_ACCOUNT', data);
				commit('SET_ACCOUNT_RESPONSE_WAIT', false);
			} else {
				// Handle error here
				commit('SET_ACCOUNT_RESPONSE_WAIT', false);
			}
		}
	},
	async RESET_ACCESS_TOKEN({ commit, state }, payload) {
		let $data = {};
		if (payload) {
			$data = { refreshToken: payload };
			const res = await this.$repositories.user.resetAccess($data);
			if (res.status === 200 && res.data) {
				localStorage.setItem('token', JSON.stringify(res.data));
				return res.data;
			}
		}
	},
	async GET_PROFILE({ commit, state }, payload) {
		const accountId = state.ACCOUNTID || null;
		const customerId = state.CUSTOMERID || null;
		if (state.AUTHENTICATED || accountId) {
			const res = await this.$repositories.account.getProfileInfo(accountId, customerId, payload);
			if (res.status === 200 && res.data) {
				const { data } = res;
				commit('SET_PROFILE', data[0]);
			} else {
				// Handle error here
			}
		}
	},
	async UPDATE_PROFILE_OLD({ commit, state }, payload) {
		payload.accountId = payload.accountId ? payload.accountId : state.ACCOUNTID;
		payload.customerId = payload.customerId ? payload.customerId : state.CUSTOMERID;
		const res = await this.$repositories.account.updateProfile(payload);
		await this.dispatch('USER_STORE/GET_PROFILE');
		if (res.status === 200 && res.data) {
			state.PROFILE_RESPONSE = {
				message: 'success',
				response: res.data,
			};
		} else {
			state.PROFILE_RESPONSE = {
				message: 'Error',
				response: res.data,
			};
		}
	},
	async UPDATE_INVOICE({ commit, state }, payload) {
		const customerId = state.CUSTOMERID || null;
		const invoiceEmails = payload.invoiceEmails;

		if (state.AUTHENTICATED) {
			const result = await this.$repositories.account.updateInvoice(customerId, invoiceEmails);

			if (result.status === 200 && result.data) {
				const { data } = result;
				commit('SET_INVOICE_RESPONSE', data);
			} else {
				// Handle error here
			}
		}
	},
	async ADD_NEW_USER({ commit, state }, payload) {
		const accountID = state.ACCOUNTID;
		if (state.AUTHENTICATED) {
			const result = await this.$repositories.account.addNewUser(accountID, payload);
			if (result.status === 200 && result.data) {
				const { data } = result;
				commit('SET_ADD_NEW_USER', data);
			} else {
				// Handle error here
			}
		}
	},
	async UPDATE_SHIPPING({ commit, state }, payload) {
		const customerID = state.CUSTOMERID;
		let shippingID = false;
		if (payload.shippingAddress) {
			shippingID = payload.shippingAddress.shippingAddressId || payload.shippingAddress.id;
		} else if (payload.additionalFreightInfo) {
			shippingID = payload.additionalFreightInfo.shippingAddressId;
		}
		if (state.AUTHENTICATED) {
			let result = '';
			if (shippingID) {
				result = await this.$repositories.account.updateShipping(customerID, payload, shippingID);
			} else {
				result = await this.$repositories.account.addShipping(customerID, payload);
			}
			if (result.status === 200 && result.data) {
				await this.dispatch('USER_STORE/FETCH_ACCOUNT');
			} else {
				// Handle error here
			}
		}
	},
	async DELETE_SHIPPING({ commit, state }, payload) {
		const customerID = state.CUSTOMERID;
		if (state.AUTHENTICATED) {
			if (payload) {
				const result = await this.$repositories.account.deleteShipping(customerID, payload.data);
				if (result.status === 200 && result.data) {
					const { data } = result;
					commit('SET_SHIPPING_RESPONSE', data);
				}
			}
		}
	},
	async UPDATE_CREDIT_BILLING({ commit, state }, payload) {
		const customerID = state.CUSTOMERID;
		state.API_RESPONSE_WAIT = true;
		if (state.AUTHENTICATED) {
			const result = await this.$repositories.account.updateCreditBilling(customerID, payload.data, payload.paymentId);
			if (result.status === 200 && result.data) {
				const { data } = result;
				commit('SET_CREDIT_BILLING_RESPONSE', data);
			} else {
				// Handle error here
			}
			state.API_RESPONSE_WAIT = false;
		}
	},
	async ADD_PAYMENTS({ commit, state }, payload) {
		const customerID = state.CUSTOMERID;
		if (state.AUTHENTICATED) {
			const result = await this.$repositories.account.addPayment(customerID, payload);
			if (result.status === 200 && result.data) {
				const { data } = result;
				commit('SET_ADD_PAYMENT_RESPONSE', data);
			} else {
				// Handle error here
			}
		}
	},
	async ADD_CREDIT_CARD({ commit, state }, payload) {
		let statusMsg = '';
		state.API_RESPONSE_WAIT = true;
		const intentCallResponse = await this.$repositories.checkout.setupIntent({
			paymentKeyOnly: true,
			customerId: state.CUSTOMERID,
		});
		const {
			business_address: businessAddress,
			billingAddress,
			is_shipping_address: isShippingAddress = false,
			defaultPayment = false,
		} = payload;
		const clientSecret = (intentCallResponse.data && intentCallResponse.data.clientSecret) || '';

		// const billingDetails = {
		//   "address": {
		//     "city": this.billing_address_city,
		//     "country": this.billing_address_country,
		//     "line1": this.billing_address_address_1,
		//     "line2": this.billing_address_address_2,
		//     "postal_code": this.billing_address_postal_code,
		//     "state": this.billing_address_state
		//   },
		//   "email": "",
		//   "name": this.billing_address_first_name + ' ' + this.billing_address_last_name,
		//   "phone": this.billing_address_phone_number
		// };

		const paymentIntentId = await getPaymentIntent(clientSecret);
		const requestPayload = {
			accountId: state.ACCOUNTID,
			customerId: state.CUSTOMERID,
			paymentPreferences: 'CREDITCARD',
		};
		requestPayload.defaultPayment = defaultPayment;
		if (billingAddress) {
			requestPayload.billingAddress = billingAddress;
		}
		requestPayload.paymentToken = '';
		if (paymentIntentId) {
			requestPayload.paymentToken = paymentIntentId;
			if (isShippingAddress) {
				requestPayload.billingAddress = this.$utility.makeAddressModel(businessAddress, 'shipping_address_');
			} else if (!requestPayload.billingAddress) {
				requestPayload.billingAddress = this.$utility.makeAddressModel(businessAddress, 'new_ba_');
			}
			await this.$repositories.user.update_payment_option(requestPayload);
			statusMsg = 'success';
		} else {
			this.dispatch('STRIPE_STORE/CARD_VALIDATION', {
				error: true,
			});
			statusMsg = 'failed';
		}
		state.API_RESPONSE_WAIT = false;
		return statusMsg;
	},
	async GET_ORDERS({ commit, state }) {
		const customerId = state.CUSTOMERID || null;

		if (state.AUTHENTICATED || customerId) {
			commit('SET_ORDERS_LOADED', true);
			const res = await this.$repositories.user.getOrders(customerId);
			if (res.status === 200 && res.data) {
				const { data } = res;
				commit('SET_ORDERS', data);
				commit('SET_ORDERS_LOADED', false);
			} else {
				// Handle error here
				commit('SET_ORDERS_LOADED', false);
			}
		}
	},
	async GET_ORDER({ commit, state }, payload) {
		const { orderId } = payload;

		if (state.AUTHENTICATED || orderId) {
			commit('SET_ORDER_LOADED', true);
			const res = await this.$repositories.user.getOrder(orderId);
			if (res.status === 200 && res.data) {
				const { data } = res;
				commit('SET_ORDER', await this.$utility.parseCartForSubscription(this, data));
				commit('SET_ORDER_LOADED', false);
			} else {
				// Handle error here
				commit('SET_ORDER_LOADED', false);
			}
		}
	},
	async FETCH_DASHBOARD({ commit, state }, payload) {
		if (state.AUTHENTICATED) {
			const $data = { customerId: state.CUSTOMERID, accountId: state.ACCOUNTID };
			const res = await this.$repositories.account.getDashboardItems($data);
			if (res.status === 200 && res.data) {
				const { data } = res;
				commit('SET_DASHBOARD', data);
				// state.DASHBOARD = data
			} else {
				// Handle error here
			}
		}
	},
	async UPDATE_READ_MESSAGE({ commit, state, dispatch }, payload) {
		if (state.AUTHENTICATED) {
			const res = await this.$repositories.user.updateReadMessage(payload);
			if (res.status === 200 && res.data) {
				//  await dispatch('FETCH_DASHBOARD');
			} else {
				// Handle error here
			}
		}
	},
	async GET_SUBSCRIPTIONS({ commit, state }, payload) {
		commit('SET_SUBSCRIPTION_RESPONSE', true);
		const customerId = state.CUSTOMERID;
		const { shippingAddressId } = payload;
		const { paymentId } = payload;
		const res = await this.$repositories.user.getSubscriptions({
			customerId,
			shippingAddressId,
			paymentId,
		});
		if (res.status === 200 && res.data) {
			const { data } = res;
			commit('SET_SUBSCRIPTION', data.results);
		} else {
			commit('SET_SUBSCRIPTION', res);
			// Handle error here
		}
	},
	async GET_SURVEYS({ commit, state }, channel) {
		const res = await this.$repositories.user.getSurveys(channel);
		if (res.status === 200 && res.data) {
			const { data } = res;
			commit('SET_SURVEYS', data);
		} else {
			// Handle error here
		}
	},
	async UPDATE_SUBSCRIPTION_QUANTITY({ commmit, state }, payload) {
		state.API_RESPONSE_WAIT = true;
		if (!payload.customerId) {
			payload.customerId = state.CUSTOMERID;
		}
		const res = await this.$repositories.user.updateSubscriptionQuantity(payload);
		state.API_RESPONSE_WAIT = false;
		if (res.status === 200 && res.data) {
			const { data } = res;
			state.SUBSCRIPTION_RESPONSE = data;
		} else {
			state.SUBSCRIPTION_RESPONSE = res.data;
			// Handle error here
		}
	},
	async USER_EXISTENCE_CHECK({ commit, state }, payload) {
		const { email, source } = payload;

		if (!(email && source)) {
			return;
		}
		try {
			const response = await this.$repositories.user.getUserStatus(payload);
			if (response.status === 200 && response && response.data) {
				commit('SET_USER_STATUS_DATA', response.data);
				commit('SET_USER_EXIST', true);
				return true;
			}
		} catch (error) {
			console.log(error);
			return false;
		}
	},
	async UPDATE_PASSWORD({ state }, payload) {
		let result = '';
		if (state.AUTHENTICATED) {
			const res = await this.$repositories.user.changePassword(payload);
			if (res.status === 200 && res.data) {
				// const { data } = res
				result = res.data;
			} else {
				// Handle error here
				result = res;
			}
		}
		return result;
	},
	async UPDATE_PROFILE({ state }, payload) {
		let result = '';
		if (state.AUTHENTICATED) {
			const res = await this.$repositories.user.updateProfileInfo(state.CUSTOMERID, payload);
			if (res.status === 200 && res.data) {
				// const { data } = res
				result = res.data;
			} else {
				// Handle error here
				result = res;
			}
		}
		return result;
	},
	// Customer API start
	async FETCH_CUSTOMER({ commit, state }) {
		if (!state.CUSTOMERID) {
			console.log('FETCH_CUSTOMER - No CustomerID');
			return;
		}
		const payload = {
			customerId: state.CUSTOMERID,
		};
		try {
			const response = await this.$repositories.user.getCustomerDetails(payload);
			if (response.status === 200 && response && response.data) {
				commit('SET_CUSTOMER_DATA', response.data);
				commit('SET_USER_EXIST', true);
				if (response.data.cartId) {
					commit('SET_CART_ID', response.data.cartId);
					this.dispatch('CART_STORE/SET_CART_ID', response.data.cartId);
				}
				return true;
			}
		} catch (error) {
			console.log(error);
			return false;
		}
	},
	async UPDATE_CUSTOMER_BY_ID({ commit, state }, payload) {
		let message;
		const customerID = state.CUSTOMERID;
		if (state.AUTHENTICATED) {
			if (
				payload.action === 'ADD_PAYMENT' ||
				(payload.action === 'UPDATE_PAYMENT' && payload.editCreaditCard === true)
			) {
				const intentCallResponse = await this.$repositories.checkout.setupIntent({
					paymentKeyOnly: true,
					customerId: state.CUSTOMERID,
				});
				const clientSecret = (intentCallResponse.data && intentCallResponse.data.clientSecret) || '';
				const paymentIntentId = await getPaymentIntent(clientSecret);
				if (paymentIntentId) {
					payload.paymentToken = paymentIntentId;
					payload.paymentType = 'CREDITCARD';
				} else {
					this.dispatch('STRIPE_STORE/CARD_VALIDATION', {
						error: true,
					});
				}
				if (payload.paymentToken) {
					await this.$repositories.user.updateCustomer(customerID, payload);
					message = 'Success';
				} else {
					message = 'Error';
				}
			} else {
				await this.$repositories.user.updateCustomer(customerID, payload);
				message = 'Success';
			}
			return message;
		}
	},
	// end
	// Device registration
	async DEVICE_REGISTER({ commit, state }, payload) {
		try {
			const response = await this.$repositories.user.deviceRegsiter(payload);
			if (response.status === 200 && response && response.data) {
				commit('SET_DEVICE_DATA', response.data.value);
			} else if (response.errorMessage) {
				commit('SET_DEVICE_ERROR_MSG', response.errorMessage);
			}
		} catch (error) {
			console.log(error);
			return false;
		}
	},
	// Update Subscriptions
	async DEVICE_REGISTER_UPDATE({ commit, state }, payload) {
		try {
			const response = await this.$repositories.user.deviceRegsiterUpdate(payload);
			console.log(response);
		} catch (error) {
			console.log(error);
			return false;
		}
	},
	// Create Subscription after registered
	async CREATE_SUBSCRIPTION({ commit, state }, payload) {
		try {
			const response = await this.$repositories.user.createSubscription(payload);
			console.log(response);
		} catch (error) {
			console.log(error);
			return false;
		}
	},
	SET_VALID_ADDRESS({ commit }, payload) {
		commit('SET_VALIDATED_ADDRESS', payload);
	},
};
// mutations
const mutations = {
	SET_VALIDATED_ADDRESS(state, payload) {
		state.VALID_ADDRESS = payload;
	},
	SET_USER(state, user) {
		if (user === undefined || user === null) {
			state.AUTHENTICATED = false;
			state.USER = null;
			state.ACCOUNTID = null;
			state.CUSTOMERID = null;

			// TEMPORARY FIX WE NEED TO replace sessionStorage throughout the application THESE
			sessionStorage.removeItem('accountId');
			sessionStorage.removeItem('customerId');
			state.AUTHENTICATED = false;

			return;
		}
		state.USER = user;
		// if (user['attributes'] && user['attributes']['custom:accountId'] && user['attributes']['custom:customerId']) {
		// accountId for B2B
		if (user.attributes && user.attributes['custom:customerId']) {
			state.AUTHENTICATED = true;
			state.ACCOUNTID = user.attributes['custom:accountId'];
			state.CUSTOMERID = user.attributes['custom:customerId'];
			state.CUSTOMER_EMAIL = user.attributes.email;
			state.ACCESS_TOKEN = user.attributes.accessToken;
		}
	},
	SET_SIGN_IN_ERROR(state, payload) {
		if (!payload) return (state.SIGN_IN_ERROR = null);
		if (!payload.message) state.SIGN_IN_ERROR = 'Couldnt sign in at this time';
		state.SIGN_IN_ERROR = payload.message;
		if (payload) {
			const teliumPayload = {
				event_category: 'Account',
				event_action: 'Login Failure',
				event_label: state.SIGN_IN_ERROR,
				event_value: 'NA',
				event_error: state.SIGN_IN_ERROR,
				page_type: 'Sign-in',
				error_message: state.SIGN_IN_ERROR,
			};
			this.$telium.setTealiumPayload('user_login_failure', teliumPayload);
		}
	},
	SET_PASSWORD_ERROR(state, payload) {
		state.PASSWORD_ERROR = payload;
	},
	SET_FORGOT_PASSWORD_ERROR(state, payload) {
		if (!payload) return (state.FORGOT_PASSWORD_ERROR = null);
		state.FORGOT_PASSWORD_ERROR = payload;
	},
	SET_RESET_PW_ERROR(state, payload) {
		if (!payload) return (state.RESET_PASSWORD_ERROR = null);
		state.RESET_PASSWORD_ERROR = payload;
	},
	SET_COG_FORM_LOADING(state) {
		state.COG_FORM_LOADING = !state.COG_FORM_LOADING;
	},
	SET_CHANGE_PW_ERROR(state, payload) {
		if (!payload) return (state.CHANGE_PW_ERROR = null);
		state.CHANGE_PW_ERROR = payload;
	},
	RESET_COG_FORMS(state) {
		state.CHANGE_PW_ERROR = null;
		state.FORGOT_PASSWORD_ERROR = null;
		state.SIGN_IN_ERROR = null;
		state.RESET_PASSWORD_ERROR = null;
		state.SIGN_IN_ERROR = null;
	},
	SET_DASHBOARD(state, payload) {
		state.DASHBOARD = payload;
	},
	RESET_USER(state) {
		state.USER = null;
		state.ACCOUNTID = '';
		state.AUTHENTICATED = false;
		state.CUSTOMER_PROFILE = {};
		state.USER_PROFILE = {};
		state.PROFILE_RESPONSE = {};
		sessionStorage.removeItem('cartId');
	},
	// end cognito mutations
	SET_ACCOUNT(state, payload) {
		try {
			if (payload.key) {
				if (payload.value) {
					state.CUSTOMER = payload.value;
					const $value = payload.value;
					state.COMPANY_NAME = $value.companyName;
					state.USER_DISPLAY_NAME = $value.companyName;
					if ($value.addresses) {
						const shippingAddress = $value.addresses.shippingAddresses ? $value.addresses.shippingAddresses : [];
						shippingAddress.map((ele) => {
							if (ele.shippingAddressId === $value.defaultShippingAddressId) {
								ele.isDefault = true;
							} else {
								ele.isDefault = false;
							}
							return ele;
						});
						state.SHIPPING_ADDRESSES = shippingAddress.sort((a, b) => {
							return b.isDefault - a.isDefault;
						});
						state.DEFAULT_SHIPPING_ID = $value.defaultShippingAddressId;
						state.BILLING_ADDRESSES = $value.addresses.billingAddresses ? $value.addresses.billingAddresses : [];
						const $shippingAddress = state.SHIPPING_ADDRESSES.filter(($obj) => {
							if ($obj.shippingAddressId === $value.defaultShippingAddressId) {
								return $obj;
							}
							return false;
						});
						state.DEFAULT_SHIPPING_ADDRESS = $shippingAddress.length > 0 ? $shippingAddress[0] : null;
					}
					if ($value.salesRepresentative && $value.salesRepresentative.id) {
						state.SALES_REPRESENTATIVE = $value.salesRepresentative;
					}
					if ($value.payments && $value.payments.length) {
						state.PAYMENTS = $value.payments;
						const cardDetails = [];
						const $shipping = Object.assign([{}], state.SHIPPING_ADDRESSES);
						const allAddresses = $shipping.concat(state.BILLING_ADDRESSES);
						state.ADDRESS = allAddresses || [];
						$value.payments.forEach((item) => {
							// if(item.customerId !== state.CUSTOMERID) {
							//   return;
							// }
							if (item.paymentType === 'CREDITCARD') {
								const { cardObject, paymentToken } = item;
								const { expMonth = 'xx', expYear = 'xxxx', last4 = 'xxxx', brand = 'Xxxx' } = cardObject || {};
								let cardImage = 'card-icon-generic.png'; // TODO;
								const cardImageSet = {
									visa: 'visa1.png',
									mastercard: 'mastercard.png',
									discover: 'discover.png',
									amex: 'amex.png',
									unionpay: 'unionpay.png',
									jcb: 'jcb.png',
									diners: 'diners.png',
								};
								if (cardImageSet[brand]) {
									cardImage = cardImageSet[brand];
								}
								let status = false;
								if (item.paymentId === $value.defaultPaymentId) {
									status = true;
								}
								let expired = false;
								const $date = new Date();
								const $currentDate = new Date($date.getFullYear(), $date.getMonth(), 1);
								const $cardDate = new Date(expYear, parseInt(expMonth), 0);
								if ($cardDate < $currentDate) {
									expired = true;
								}
								// let defaultShippingAddress = state.SHIPPING_ADDRESSES.find(address => address.shippingAddressId === item.billingAddressId)
								// state.DEFAULT_SHIPPING_ADDRESS = defaultShippingAddress
								const $billingAddress = allAddresses.filter((address) => {
									if (
										address.shippingAddressId === item.billingAddressId ||
										address.billingAddressId === item.billingAddressId
									) {
										return address;
									}
									return false;
								});
								state.DEFAULT_ADDRESS = $billingAddress[0];
								cardDetails.push({
									paymentToken,
									name: `${brand} ${last4}`,
									brand: `${brand}`,
									expiry: `${expMonth}/${expYear.substr(2)}`,
									image: cardImage,
									imageName: cardImage,
									companyName: state.COMPANY_NAME,
									defaultBillingAddress: state.DEFAULT_ADDRESS,
									defaultStatus: status,
									billingAddressId: item.billingAddressId,
									paymentId: item.paymentId,
									expiredStatus: expired,
									...cardObject,
								});
							}
							if (item.paymentType === 'ACH') {
								state.PAYMENT_ACH_ID = item.paymentId;
							}
							if (item.paymentType === 'PAYMENTTERMS') {
								state.PAYMENT_TERMS_ID = item.paymentId;
								const $businessObj = item.business;
								const $item = Object.assign({}, item);
								delete $item.business;
								// $businessObj = {...$businessObj, ...$item}
								state.PAYMENT_TERMS_BUSINESS = {
									...$businessObj,
									...$item,
								};
							}
						});
						state.CREDIT_CARD_DETAILS = cardDetails;
						// TODO: Refactor below;
						let $payments = $value.payments[0];
						const $index = $value.payments.findIndex(function ($obj) {
							return $obj.paymentId === $value.defaultPaymentId;
						});
						if ($index > -1) {
							$payments = $value.payments[$index];
						}
						state.DEFAULT_PAYMENT_METHOD = $payments.paymentType;
						state.DEFAULT_PAYMENT_DETAIL = $payments;
						state.PAYMENT_OBJ = mergeStoreToPaymentForm(
							this,
							state.PAYMENT_FORM_DATA,
							state.DEFAULT_PAYMENT_DETAIL,
							state
						);
					} else {
						state.CREDIT_CARD_DETAILS = [];
					}
					if ($value.shippingMethods && $value.shippingMethods.length) {
						state.SHIPPING_METHODS = $value.shippingMethods;
					}
					if ($value.taxExemptReviewList && $value.taxExemptReviewList.length) {
						state.TAX_EXCEPTION_LIST = $value.taxExemptReviewList;
					}
					state.DEFAULT_CARRIERID = $value.defaultCarrierId;
					if ($value.carriers && $value.carriers.length) {
						const $carrierList = $value.carriers;
						$carrierList.forEach((el) => {
							if (el.carrierId === state.DEFAULT_CARRIERID) {
								el.billing_address_is_default = true;
								el.isDefault = true;
							} else {
								el.isDefault = false;
							}
						});
						state.CARRIERS_LIST = $carrierList.sort((a, b) => {
							return b.isDefault - a.isDefault;
						});
					}
					if ($value.additionalFreightInfos && $value.additionalFreightInfos.length) {
						state.ADDITIONAL_FREIGHT_INFO = $value.additionalFreightInfos;
					}
				}
			}
		} catch (exp) {
			console.log(exp);
		}
	},
	SET_ADD_NEW_USER(state, payload) {
		state.ADD_NEW_USER_RESPONSE = payload;
	},
	SET_SHIPPING_RESPONSE(state, payload) {
		state.SHIPPING_RESPONSE = payload;
	},
	SET_CREDIT_BILLING_RESPONSE(state, payload) {
		state.CREDIT_BILLING_RESPONSE = payload;
	},
	SET_ADD_PAYMENT_RESPONSE(state, payload) {
		state.ADD_PAYMENT_RESPONSE = payload;
	},
	SET_ORDERS(state, payload) {
		state.ORDERS = payload;
	},
	SET_ORDER(state, payload) {
		state.ORDER = payload;
	},
	SET_ORDER_LOADED(state, payload) {
		state.ORDER_LOADED = payload;
	},
	SET_ORDERS_LOADED(state, payload) {
		state.ORDERS_LOADED = payload;
	},
	SET_ACCOUNT_RESPONSE_WAIT(state, payload) {
		state.ACCOUNT_RESPONSE_WAIT = payload;
	},
	SET_FORGOT_PASSWORD_RESPONSE(state, payload) {
		state.FORGOT_PASSWORD_RESPONSE = payload;
		console.log(payload, 'payload1111');
	},
	SET_PROFILE(state, dataValue) {
		const payload = dataValue.customerResponseBean ? dataValue.customerResponseBean : dataValue;
		state.CUSTOMER_PROFILE = payload;
		state.CUSTOMERID = payload && payload.id;
		state.CUSTOMER_EMAIL = payload.email;
		if (state.CUSTOMERID) {
			const customerDetails = payload;
			const customerPhone = payload.custom.fields.phone;
			customerDetails.phone = customerPhone;
			state.CUSTOMER_DETAILS_TEALIUM = this.$utility.setAddress(
				'customer_',
				customerDetails,
				customerDetails.id,
				'tealium'
			);
		}
		state.AUTHENTICATED = true;
	},
	SET_CART_ID(state, cartId) {
		if (!cartId) {
			return;
		}
		sessionStorage.setItem('cartId', cartId);
		state.CART_ID = cartId;
		this.state.CART_STORE.CART_ID = cartId;
	},
	SET_INVOICE_RESPONSE(state, payload) {
		state.INVOICE_RESPONSE = payload;
	},
	SET_SUBSCRIPTION_RESPONSE(state, payload) {
		state.SUBSCRIPTION_RESPONSE_WAIT = payload;
	},
	SET_SUBSCRIPTION(state, payload) {
		state.SUBSCRIPTIONS = payload;
		state.SUBSCRIPTION_RESPONSE_WAIT = false;
	},
	SET_SURVEYS(state, payload) {
		state.SURVEYS = payload.results;
	},
	SET_USER_STATUS_DATA(state, payload) {
		state.USER_STATUS_DATA = payload;
	},
	// backup Customer API start
	SET_CUSTOMER_DATA(state, payload) {
		if (payload) {
			// All address
			state.CUSTOMER_DATA = payload;
			state.CUSTOMER_ADDRESS = payload.addressList;
			const shippingAddressIds = payload.shippingAddressIds;
			const billingAddressIds = payload.billingAddressIds;
			state.RECENT_SHIPPING = payload.recentlyUsedAddressId;
			state.RECENT_PAYMENT = payload.recentlyUsedPayment;
			state.CUSTOMER_SHIPPING_ADDRESS = [];
			shippingAddressIds.forEach((id) => {
				state.CUSTOMER_ADDRESS.forEach((ele) => {
					if (ele.id === id) {
						state.CUSTOMER_SHIPPING_ADDRESS.push(ele);
					}
				});
			});
			billingAddressIds.forEach((id) => {
				state.CUSTOMER_ADDRESS.forEach((ele) => {
					if (ele.id === id) {
						state.CUSTOMER_BILLING_ADDRESS.push(ele);
					}
				});
			});
			if (payload.payments && payload.payments.length) {
				state.PAYMENTS = payload.payments;
				const cardDetails = [];
				payload.payments.forEach((item) => {
					if (item.paymentType === 'CREDITCARD') {
						const { cardObject, paymentToken } = item;
						const { expMonth = 'xx', expYear = 'xxxx', last4 = 'xxxx', brand = 'Xxxx' } = cardObject || {};
						let cardImage = 'card-icon-generic.png'; // TODO;
						const cardImageSet = {
							visa: 'visa1.png',
							mastercard: 'mastercard.png',
							discover: 'discover.png',
							amex: 'amex.png',
							unionpay: 'unionpay.png',
							jcb: 'jcb.png',
							diners: 'diners.png',
						};
						if (cardImageSet[brand]) {
							cardImage = cardImageSet[brand];
						}
						let expired = false;
						const $date = new Date();
						const $currentDate = new Date($date.getFullYear(), $date.getMonth(), 1);
						const $cardDate = new Date(expYear, parseInt(expMonth), 0);
						if ($cardDate < $currentDate) {
							expired = true;
						}
						const $billingAddress = state.CUSTOMER_ADDRESS.filter((address) => {
							if (address.id === item.billingAddressId) {
								return address;
							}
							return false;
						});
						cardDetails.push({
							paymentToken,
							name: `${brand} ${last4}`,
							brand: `${brand}`,
							expiry: `${expMonth}/${expYear.substr(2)}`,
							image: cardImage,
							imageName: cardImage,
							companyName: state.COMPANY_NAME,
							defaultBillingAddress: $billingAddress[0],
							defaultStatus: false,
							billingAddressId: item.billingAddressId,
							paymentId: item.paymentId,
							expiredStatus: expired,
							...cardObject,
						});
					}
				});
				state.CUSTOMER_CREDIT_CARD_DETAILS = cardDetails;
				// TODO: Refactor below;
				let $payments = payload.payments[0];
				const $index = payload.payments.findIndex(function ($obj) {
					return $obj.paymentId === payload.defaultPaymentId;
				});
				if ($index > -1) {
					$payments = payload.payments[$index];
				}
				state.DEFAULT_PAYMENT_METHOD = $payments.paymentType;
				state.DEFAULT_PAYMENT_DETAIL = $payments;
				state.PAYMENT_OBJ = mergeStoreToPaymentForm(this, state.PAYMENT_FORM_DATA, state.DEFAULT_PAYMENT_DETAIL, state);
			} else {
				state.CUSTOMER_CREDIT_CARD_DETAILS = [];
			}
			const customerDetails = payload;
			customerDetails.phone = payload.phoneNumber;
			state.CUSTOMER_DETAILS_D2C_TEALIUM = this.$utility.setAddress(
				'customer_',
				customerDetails,
				customerDetails.customerId,
				'tealium'
			);
		}
	},
	SET_USER_EXIST(state, payload) {
		state.USER_EXIST = payload;
	},
	SET_DEVICE_DATA(state, payload) {
		state.DEVICE = payload;
	},
	SET_DEVICE_ERROR_MSG(state, payload) {
		state.DEVICE_ERROR_MSG = payload;
	},
	SET_TERMS_VALUE(state) {
		state.ACCEPT_CONDITION.termsOfService = !state.ACCEPT_CONDITION.termsOfService;
	},
	SET_SUBSCRIBE_VALUE(state) {
		state.ACCEPT_CONDITION.marketingOffers = !state.ACCEPT_CONDITION.marketingOffers;
	},
	// end
};
export default {
	namespaced: true,
	state,
	actions,
	mutations,
	getters,
};

/*
Payment preferences:
CREDITCARD,ACH,PAYMENTTERMS

Bussiness Type :
PUBLIC,PRIVATE,GOVERNMENT,NONPROFIT
*/

const mergeStoreToPaymentForm = ($parent, formDataObj, storeData, stateObj) => {
	const { paymentType, business } = storeData;
	// Payment Type;
	formDataObj.business_type = paymentType;

	// Billing Address;
	formDataObj.billingAddress = {};
	if (storeData.billingAddressId !== null) {
		const billingAddress = stateObj.BILLING_ADDRESSES.filter(
			(address) => address.billingAddressId === stateObj.CUSTOMER.defaultBillingAddressId
		);
		if (billingAddress.length) {
			formDataObj.billingAddress = $parent.$utility.setAddress('new_ba_', billingAddress[0]);
		}
	}
	formDataObj.business = business;
	// Payment Detail;
	switch (paymentType) {
		case 'CREDITCARD':
			break;
		case 'PAYMENTTERMS':
			if (business.businessType === 'PUBLIC') {
				formDataObj.business.stock_ticker_name = business.stockTicker;
				formDataObj.business.stock_exchange = business.stockExchange;
			} else if (business.businessType === 'PRIVATE') {
				formDataObj.business.business_reference_one_address = $parent.$utility.setAddress(
					'business_reference_one_',
					business.references.firstReference
				);
				formDataObj.business.business_reference_two_address = $parent.$utility.setAddress(
					'business_reference_two_',
					business.references.secondReference
				);
				formDataObj.business.private_type_bank_address = $parent.$utility.setAddress(
					'private_type_bank_',
					business.bankInformation
				);
			} else if (business.businessType === 'GOVERNMENT') {
				formDataObj.business.government_email = business.email;
				formDataObj.business.department_phone_number = business.phoneNumber;
			}
			formDataObj.business.termsStatus = storeData.termsStatus;
			break;
		case 'ACH':
			break;
		default:
			break;
	}
	return formDataObj;
};
