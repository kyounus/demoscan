describe('/webinars/uofm', () => {
  it('has the correct title', () => {
    cy.visit('/webinars/uofm')
    cy.title().should('equal', 'Title')
  })
})
