// Create custom Cypress commands and overwrite existing ones.
// https://on.cypress.io/custom-commands

import { getStore } from './utils';

const productClass = '[class*="product_"]:visible';

Cypress.Commands.add('logIn', ({ username = 'admin', password = 'password' } = {}) => {
	// Manually log the user in
	cy.location('pathname').then((pathname) => {
		if (pathname === 'blank') {
			cy.visit('/');
		}
	});
	getStore().then((store) => store.dispatch('auth/logIn', { username, password }));
});

Cypress.Commands.add('addToCart', (name) => {
	cy.get('h2:visible,h3:visible')
		.contains(name)
		.parent()
		.parent()
		.contains('Add to cart')
		.click();
});

Cypress.Commands.add('cartUpdateQuantity', (name, qty) => {
	cy.server();
	cy.route('PUT', '/go/rest/V1/*carts/*/items/*').as('updateProduct');
	cy.route('GET', '/go/rest/V1/*carts/*/items').as('getItems');
	cy.get('h6:visible')
		.contains(name)
		.closest(productClass)
		.find('select')
		.select(String(qty));

	cy.wait('@updateProduct').then((obj) => {
		const { request, response } = obj;
		expect(request.body).to.have.deep.property('cartItem.qty', qty);
		expect(response.body).to.have.deep.property('name', name);
		expect(response.body).to.have.deep.property('qty', qty);
	});

	cy.wait('@getItems');
});

Cypress.Commands.add('cartDelete', (name, lengthAfterDelete) => {
	cy.server();
	cy.route('DELETE', '/go/rest/V1/*carts/*/items/*').as('deleteProduct');
	cy.route('GET', '/go/rest/V1/*carts/*/items').as('getItems');
	cy.get('h6:visible')
		.contains(name)
		.closest('[class*="product_"]:visible')
		.find('button')
		.click();

	cy.wait('@deleteProduct')
		.its('responseBody')
		.should('eq', true);

	cy.get('[class*="product_"]:visible')
		.should('have.length', lengthAfterDelete)
		.should('not.contain', name);
});

Cypress.Commands.add('cartOpen', () => {});

Cypress.Commands.add('cartClose', () => {
	cy.get('#cart-panel [class*="top_"] button[class*="close_"]:visible').click();
});

Cypress.Commands.add('goToCheckout', (open = false) => {
	if (open) {
		// TODO
	}

	// Ignore JS errors when loading checkout
	cy.on('uncaught:exception', () => false);

	cy.get('#cart-panel:visible')
		.find('a')
		.contains('Checkout')
		.click();
});

Cypress.Commands.add('magentoSummaryContains', (name, price, qty) => {
	cy.get('.cart-items .aw-sidebar-product-name')
		.contains(name)
		.closest('.aw-sidebar-product-card')
		.within(($el) => {
			if (price) {
				cy.wrap($el).should('contain', price);
			}

			cy.contains('Qty: ')
				.parent()
				.children()
				.last()
				.should(typeof qty === 'string' ? 'have.text' : 'satisfy', qty);
		});
});
