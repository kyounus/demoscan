import { object } from '@storybook/addon-knobs';
export const addControls = (argTypes, controls) => {
	const obj = Object.assign({}, argTypes);
	Object.entries(controls).forEach(([key, values]) => {
		values.forEach((propName) => {
			if (key === 'json') {
				obj[propName] = {
					control: { disable: true },
					description: 'See [Knobs] for details',
				};
			} else {
				obj[propName] = {
					control: key,
				};
			}
		});
	});
	return obj;
};

const htmlDecode = (input) => {
	const doc = new DOMParser().parseFromString(input, 'text/html');
	return doc.documentElement.textContent;
};

const replaceDoublequotesInObject = (obj = {}) => {
	const stringifiedObj = JSON.stringify(obj);
	const decodedStringifiedObj = htmlDecode(stringifiedObj);
	const escapedDecodedStringifiedObj = decodedStringifiedObj.replaceAll(/([a-z]*)=(")([^"]*)(")/g, "$1='$3'");
	return JSON.parse(escapedDecodedStringifiedObj);
};

export const initJsonKnobs = (args, argTypes, props = []) => {
	Object.keys(argTypes).forEach((key) => {
		if (props.includes(key)) {
			const reactiveObj = object(key, args[key]);
			args[key] = replaceDoublequotesInObject(reactiveObj);
		}
	});
};
