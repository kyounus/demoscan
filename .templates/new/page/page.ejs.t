---
to: "pages/<%= h.page(path) %>.vue"
---
<template>
  <div>
    <%= h.inflection.titleize(path.replace(/-/g, '_')) %>
  </div>
</template>

<script>
export default {
  head: {
    title: 'Title',
    meta: [
      {
        name: 'description',
        content: 'Add description',
      },
    ],
  },
}
</script>
<%
if (useStyles) { %>
<style lang="scss" module>
@import '@design';
</style>
<% } %>
