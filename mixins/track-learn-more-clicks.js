export default {
	mounted() {
		document.addEventListener('click', this.trackLearnMoreClicksDocumentClickHandler, true);
	},

	beforeDestroy() {
		document.removeEventListener('click', this.trackLearnMoreClicksDocumentClickHandler, true);
	},

	methods: {
		trackLearnMoreClicksDocumentClickHandler(e) {
			if (!(e.target && e.target.tagName === 'A')) return;
			const el = e.target;
			const text = el.textContent.trim().toLowerCase();
			if (text !== 'learn more') return;
			const fullUrl = el.href;
			this.$tealiumEvent('learn_more_click', {
				event_category: 'Learn More',
				event_action: 'Clicked Learn More on / and /air-purifiers',
				event_label: fullUrl,
				click_url: fullUrl,
			});
		},
	},
};
