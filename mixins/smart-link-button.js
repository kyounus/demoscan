import scrollToHash from '@/utils/scroll-to-hash';
import { mapGetters, mapState } from 'vuex';

export default {
	inject: ['openPromoModal', 'openCart'],

	props: {
		to: {
			type: String,
			default: '',
		},

		href: {
			type: String,
			default: '',
		},

		smart: {
			type: String,
			default: '',
		},

		addToCart: {
			type: [String, Array],
			default: '',
		},

		isWorking: {
			type: Boolean,
			default: false,
		},

		disabled: {
			type: Boolean,
			default: false,
		},

		addToCartSource: {
			type: String,
			default: '',
		},
	},

	data() {
		return {
			isAddingToCart: false,
			showAddToCart: true,
			showAddToQuote: false,
			cartLoading: false,
			showDiscount: false,
			quoteOnlyProduct: true,
			productImages: [],
		};
	},

	computed: {
		...mapGetters('magento', ['cart', 'skusWithWarranties']),
		...mapState('CART_STORE', ['CART', 'CART_ID', 'AUTH_PROCESS', 'DISCOUNT_PRICE', 'SUB_TOTAL_PRICE']),
		...mapState('USER_STORE', ['AUTHENTICATED', 'CUSTOMERID']),

		isAddToCart() {
			const { smart } = this;
			return (
				this.addToCart ||
				(smart && (smart.startsWith('https://molekule.com/add-to-cart?') || smart.startsWith('/add-to-cart?')))
			);
		},

		promoCode() {
			const { promo = '' } = this.$router.resolve(this.smart).location.query;

			return promo;
		},

		addToCartSkus() {
			if (!this.isAddToCart) return [];
			const skus = this.addToCart || this.$router.resolve(this.smart).location.query.skus;
			const { products } = this.$store.state.magento;
			const { isUSWebsite } = this.$store.state;
			const returnVal = (Array.isArray(skus) ? skus : (skus || '').split(','))
				.map((skuRaw) => {
					let sku = (skuRaw || '').trim();
					const location = sku.substr(0, 2);
					const hasLocation = sku.startsWith('US:') || sku.startsWith('CA:');
					if (hasLocation) {
						if ((isUSWebsite && location !== 'US') || (!isUSWebsite && location === 'US')) return null;
						sku = sku.substr(3);
					}
					return products[sku] && sku;
				})
				.filter(Boolean)
				.reduce(
					(obj, sku) => {
						obj[0][sku] = (obj[0][sku] || 0) + 1;
						return obj;
					},
					[{}]
				)
				.map((obj) => Object.entries(obj).map(([sku, qty]) => ({ sku, qty })))[0];
			return returnVal.length > 0 ? returnVal : [{ qty: 1, sku: skus }];
		},

		outOfStockToggle() {
			const { outOfStockToggles = {} } = this.$store.state.globalData.magento;
			const toggle = Object.entries(outOfStockToggles).find(
				([sku, value]) => this.addToCartSkus.map((sku) => sku.sku).includes(sku) && value
			);

			return toggle && toggle[1];
		},

		addToCartText() {
			const { addToCartSkus: products, outOfStockToggle } = this;
			const texts = ['Add to cart', 'Pre-order', 'Coming Soon'];

			const innerVNodes = this.$scopedSlots.default && this.$scopedSlots.default();
			if (Array.isArray(innerVNodes) && innerVNodes.length === 1 && innerVNodes[0].text) {
				const textParts = innerVNodes[0].text.split(';').map((t) => t.trim());
				const loopMax = Math.min(textParts.length, 3);

				for (let i = 0; i < loopMax; i++) {
					const text = textParts[i];
					if (text) texts[i] = text;
				}
			}

			if (products.length === 0 || outOfStockToggle)
				return outOfStockToggle ? outOfStockToggle.ctaText || texts[2] : texts[2];

			if (products.length === 1 && this.$store.state.globalData.magento.preOrders[products[0].sku]) {
				return texts[1];
			}

			return texts[0];
		},

		isWorkingNormalized() {
			return this.isAddingToCart || this.isWorking;
		},

		dataShared() {
			const { to, href, isWorkingNormalized } = this;
			let { smart } = this;
			const props = {};

			if (this.isAddToCart) {
				props.type = 'button';
				props.disabled = this.disabled || isWorkingNormalized;
			} else if (smart) {
				if (smart.startsWith('https://molekule.com/')) {
					smart = smart.substr(20);
				}

				if (smart.startsWith(`${this.$route.path}#`)) {
					smart = `https://id/${smart.slice(smart.indexOf('#') + 1)}`;
				}

				if (smart.startsWith('https://id/')) {
					const hashUrl = `#${smart.substr(11)}`;
					if (this.$route.hash === hashUrl) {
						props['data-hash-current'] = true;
					} else {
						props.to = hashUrl;
					}
				} else if (smart.startsWith('http')) {
					props.href = smart;
				} else {
					props.to = smart;
				}
			} else if (to) {
				props.to = to;
			} else if (href) {
				props.href = href;
			} else {
				props.type = 'button';
				props.disabled = this.disabled || isWorkingNormalized;
			}

			return {
				props,
				element: props.to ? 'nuxt-link' : props.href ? 'a' : 'button',
			};
		},
	},

	methods: {
		async addProductToCart(product) {
			try {
				this.isAddingToCart = true;
				const newCartPayload = {
					tealiumVisitorId: '',
				};
				if (this.AUTHENTICATED) {
					newCartPayload.customerId = this.CUSTOMERID;
				}
				const newProductPayload = [];
				let productObj = {};
				if (this.$route.query.debug) {
					productObj = {
						action: 'ADD_LINE_ITEM',
						productId: '7f6cae55-e6e3-4056-8813-9d85be025c90',
						quantity: 1,
						variantId: 1,
					};
					if (this.AUTHENTICATED) {
						productObj.customerId = this.CUSTOMERID;
					}
					newProductPayload.push(productObj);
				} else {
					for (const item of product) {
						productObj = {
							action: 'ADD_LINE_ITEM',
							variantSKU: item.sku,
							quantity: item.qty,
						};
						if (this.AUTHENTICATED) {
							productObj.customerId = this.CUSTOMERID;
						}
						newProductPayload.push(productObj);
					}
				}
				try {
					if (!this.CART_ID) {
						await this.$store.dispatch('CART_STORE/CREATE_CART', newCartPayload);
					}
					for (let i = 0; i < newProductPayload.length; i++) {
						await this.$store.dispatch('CART_STORE/ADD_REMOVE_FROM_CART', newProductPayload[i]);
					}
					// await this.$store.dispatch('CART_STORE/ADD_REMOVE_FROM_CART', newProductPayload)
					this.$nextTick(() => {
						this.$store.dispatch('MK_CART_STORE/TOGGLE_SIDE_CART', 'MK_SIDE_CART');
					});
				} catch (error) {
					// eslint-disable-next-line no-console
					console.log(error);
				}
			} catch (exp) {
				// eslint-disable-next-line no-console
				console.log(exp);
			} finally {
				this.isAddingToCart = false;
			}
		},

		click() {
			if (this.isAddToCart) {
				// eslint-disable-next-line no-console
				console.log('Add to cart product', this.addToCartSkus);
				this.addProductToCart(this.addToCartSkus);
			} else {
				const innerVNodes = this.$scopedSlots.default && this.$scopedSlots.default();
				const eventLabel = (Array.isArray(innerVNodes) && innerVNodes[0] && innerVNodes[0].text) || '';
				const eventProps = {
					event_category: 'Interaction',
					event_action: 'Clicked a button',
					event_label: eventLabel.trim(),
				};

				const { to, href } = this.dataShared.props;

				if (to || href) eventProps.event_value = to ? `${window.location.origin}${to}` : href;

				this.$tealiumEvent('clicked_cta', eventProps);
			}

			if (this.dataShared.props['data-hash-current']) {
				scrollToHash(this.$route.hash);
				return;
			}

			this.$emit('click');
		},
	},
};
