let youTubeAPIPromise = null;
function loadYouTubeAPI() {
	if (youTubeAPIPromise) return youTubeAPIPromise;

	youTubeAPIPromise = new Promise((resolve) => {
		window.onYouTubeIframeAPIReady = function onYouTubeIframeAPIReady() {
			resolve();
			delete window.onYouTubeIframeAPIReady;
		};
	});

	const tag = document.createElement('script');
	tag.src = 'https://www.youtube.com/iframe_api';
	const firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

	return youTubeAPIPromise;
}

export default {
	data: () => ({
		player: null,
		pressedPlayBeforeLoading: false,
	}),

	methods: {
		tealiumTrackYouTubeVideo(videoIframe, { playerCallback } = {}) {
			// const videoInfo = { title: 'Title Unavailable' };
			let videoDuration = '';
			let videoUrl = '';
			let trackedStart = false;
			let playerTimeout = 0;
			let destroyed = false;
			const milestones = [10, 25, 50, 75, 90, 100];
			const milestonesTimeout = 1000;

			const parseDuration = (time) => {
				const minutes = Math.floor(time / 60);
				const seconds = Math.floor(time % 60);
				return `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
			};

			const track = (name, milestone) => {
				if (milestone && name !== 'milestone')
					throw new Error('milestone should be provided only for milestone events');

				this.$tealiumEvent(`video_${name}`, {
					event_category: 'Videos',
					event_action: this.videoTrackingName,
					event_label: milestone ? `video_milestone - ${milestone}%` : `video_${name}`,
					video_length: videoDuration,
					video_name: this.videoTrackingName,
					video_url: videoUrl,
					video_platform: 'Youtube',
					video_milestone: milestone ? `${milestone}%` : '',
				});
			};

			const trackMilestones = () => {
				const { player } = this;
				const progress = (player.getCurrentTime() / player.getDuration()) * 100;

				while (milestones[0] && progress >= milestones[0]) {
					const milestone = milestones.shift();
					track('milestone', milestone);
				}

				if (milestones.length > 0 && player.getPlayerState() === window.YT.PlayerState.PLAYING) {
					playerTimeout = setTimeout(trackMilestones, milestonesTimeout);
				} else {
					playerTimeout = 0;
				}
			};

			const onReady = (e) => {
				const { player } = this;
				// try {
				// 	if (typeof player.getVideoData === 'function') {
				// 		Object.assign(videoInfo, player.getVideoData());
				// 	}
				// } catch (e) {
				// 	//
				// }

				videoDuration = parseDuration(player.getDuration());
				videoUrl = player.getVideoUrl();
				track('load');

				if (this.pressedPlayBeforeLoading) this.playYtVideo();
			};

			const onStateChange = ({ data: state }) => {
				if (!trackedStart && state === window.YT.PlayerState.PLAYING) {
					trackedStart = true;
					track('start');
				}

				if (milestones.length > 0 && state === window.YT.PlayerState.PLAYING) {
					if (playerTimeout !== 0) return;
					playerTimeout = setTimeout(trackMilestones, milestonesTimeout);
				}
			};

			loadYouTubeAPI().then(() => {
				if (destroyed) return;

				this.player = new window.YT.Player(videoIframe, {});
				const { player } = this;
				if (playerCallback) {
					playerCallback(player);
				}

				player.addEventListener('onReady', onReady);
				player.addEventListener('onStateChange', onStateChange);
			});

			return () => {
				const { player } = this;
				destroyed = true;
				if (!player) return;

				if (player.removeEventListener) {
					player.removeEventListener('onReady', onReady);
					player.removeEventListener('onStateChange', onStateChange);
				}

				player.destroy();
				if (playerTimeout) clearTimeout(playerTimeout);
				this.player = null;
			};
		},

		playYtVideo() {
			if (this.player) {
				this.player.playVideo();
				this.player.f.focus();
			} else {
				this.pressedPlayBeforeLoading = true;
			}
		},
	},
};
