import BlockForm from './-block-form.vue';

describe('@views/write-a-review/BlockForm', () => {
	it('exports a valid component', () => {
		expect(BlockForm).toBeAComponent();
	});
});
