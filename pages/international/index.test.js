import InternationalPage from './index';

describe('@views/international', () => {
	it('is a valid view', () => {
		expect(InternationalPage).toBeAPageComponent();
	});
});
