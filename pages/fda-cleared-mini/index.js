import createSlicesPage from '@/utils/create-slices-page';

export default createSlicesPage('fda_cleared_mini', {
	additionalSliceProps: (vm) => ({ multipleProductReviews: vm.page.productReviews }),
	pageComponentData: (vm) => ({ class: 'no-block-spacing' }),
})();
