import createSlicesPage from '@/utils/create-slices-page';

export default createSlicesPage('minimo', {
	additionalSliceProps: (vm) => ({ multipleProductReviews: vm.page.productReviews }),
	pageComponentData: (vm) => ({ class: 'no-block-spacing' }),
})();
