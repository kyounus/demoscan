import BlockListing from './-block-listing.vue';

describe('@views/reviews/BlockListing', () => {
	it('exports a valid component', () => {
		expect(BlockListing).toBeAComponent();
	});
});
