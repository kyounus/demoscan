import BlockListingItem from './-block-listing-item.vue';

describe('@views/reviews/BlockListingItem', () => {
	it('exports a valid component', () => {
		expect(BlockListingItem).toBeAComponent();
	});
});
