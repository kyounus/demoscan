import IndexPage from './-index';

describe('@views/reviews', () => {
	it('is a valid view', () => {
		expect(IndexPage).toBeAPageComponent();
	});
});
