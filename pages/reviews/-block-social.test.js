import BlockSocial from './-block-social.vue';

describe('@views/reviews/BlockSocial', () => {
	it('exports a valid component', () => {
		expect(BlockSocial).toBeAComponent();
	});
});
