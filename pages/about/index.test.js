import AboutPage from './';

describe('@views/about', () => {
	it('is a valid view', () => {
		expect(AboutPage).toBeAPageComponent();
	});
});
