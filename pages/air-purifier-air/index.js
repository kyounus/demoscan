import createSlicesPage from '@/utils/create-slices-page';

export default createSlicesPage('air_purifier_mh1', {
	additionalSliceProps: (vm) => ({ productReviews: vm.page.productReviews }),
	pageComponentData: (vm) => ({ class: 'no-block-spacing' }),
})();
