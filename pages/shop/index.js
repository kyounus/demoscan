import createSlicesPage from '@/utils/create-slices-page';

export default createSlicesPage('shop', {
	additionalRequests: async ({ store }) => await store.dispatch('reviews/FETCH_PRODUCT_REVIEWS_SNIPPETS'),
	pageComponentData: (vm) => ({ class: 'no-block-spacing' }),
})();
