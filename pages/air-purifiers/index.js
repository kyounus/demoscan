import createSlicesPage from '@/utils/create-slices-page';
import SlicesRenderer from '@components/slices-renderer';
import trackLearnMoreClicks from '@/mixins/track-learn-more-clicks';

export default createSlicesPage('product_page', {
	additionalRequests: async ({ store }) => {
		await store.dispatch('reviews/FETCH_PRODUCT_REVIEWS_SNIPPETS');
	},
})({
	name: 'ProductPageWrapper',

	props: {
		sliceComponents: {
			type: Array,
			required: true,
		},
	},

	mixins: [trackLearnMoreClicks],

	render(createElement) {
		return createElement(SlicesRenderer, { props: { sliceComponents: this.sliceComponents } });
	},
});
