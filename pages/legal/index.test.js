import IndexPage from './';

describe('@views/legal', () => {
	it('is a valid view', () => {
		expect(IndexPage).toBeAPageComponent();
	});
});
