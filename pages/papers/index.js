import createSlicesPage from '@/utils/create-slices-page';

export default createSlicesPage('papers', {
	additionalSliceProps: (vm) => ({
		track: ({ item }) => {
			vm.$tealiumEvent('whitepaper_download', {
				event_category: 'Download Papers',
				event_action: 'Downloaded White Paper',
				event_label: item.file.url,
				click_url: item.file.url,
			});
		},
	}),
})();
