import BlockScience from './-block-science.vue';

describe('@views/technology/BlockScience', () => {
	it('exports a valid component', () => {
		expect(BlockScience).toBeAComponent();
	});
});
