import IndexPage from './';

describe('@views/technology', () => {
	it('is a valid view', () => {
		expect(IndexPage).toBeAPageComponent();
	});
});
