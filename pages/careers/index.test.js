import IndexPage from './';

describe('@views/careers', () => {
	it('is a valid view', () => {
		expect(IndexPage).toBeAPageComponent();
	});
});
