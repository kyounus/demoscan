# PAGES

This directory contains your Application Views and Routes. The framework reads all the `*.vue` files inside this directory and create the router of your application.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/guide/routing).

To deploy in test use this command

`git tag -d "test"; git tag -a test -m "deploy" && git push origin test --no-verify -f`

This commands deletes and existing tag. Adds a new tag with a commit message. Force push that tag to override existing tag if any
