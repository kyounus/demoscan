import IndexPage from './';

describe('@views/healthcare', () => {
	it('is a valid view', () => {
		expect(IndexPage).toBeAPageComponent();
	});
});
