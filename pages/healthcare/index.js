import createSlicesPage from '@/utils/create-slices-page';

export default createSlicesPage('healthcare_landing', {
	pageComponentData: (vm) => ({ class: 'no-block-spacing' }),
})();
