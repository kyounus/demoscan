import createSlicesPage from '@/utils/create-slices-page';
import getFullUrl from '@/utils/get-full-url';

export default createSlicesPage('air_pro_rx', {
	additionalSliceProps: (vm) => ({ productReviews: vm.page.productReviews }),

	pageComponentData: (vm) => ({
		class: 'no-block-spacing',
		on: {
			click: (e) => {
				const aElement = e.target && e.target.closest('a');
				if (!aElement) return;

				if (!getFullUrl(aElement.href).startsWith(window.location.origin)) {
					vm.$tealiumEvent('clicked_external_link', {
						event_category: 'Learn More',
						event_action: 'Clicked To External Link',
						event_label: aElement.href,
						event_value: vm.$route.path,
					});
				}
			},
		},
	}),
})();
