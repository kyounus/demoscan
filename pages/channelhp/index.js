import TemplateHomepage from '@components/template-homepage';
import createSlicesPage from '@/utils/create-slices-page';

export default createSlicesPage('channel_hp', {
	additionalRequests: async ({ store }) => {
		await store.dispatch('reviews/FETCH_PRODUCT_REVIEWS_SNIPPETS');
	},
	pageComponentData: (vm) => ({ class: 'no-block-spacing' }),
})(TemplateHomepage);
