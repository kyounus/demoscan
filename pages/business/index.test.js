import IndexPage from './';

describe('@views/business', () => {
	it('is a valid view', () => {
		expect(IndexPage).toBeAPageComponent();
	});
});
