import BlockTestimonials from './-block-testimonials.vue';

describe('@views/home/BlockTestimonials', () => {
	it('exports a valid component', () => {
		expect(BlockTestimonials).toBeAComponent();
	});
});
