const accountBaseUrl = process.env.baseApi.account_base_url + '/accounts';
const userBaseUrl = process.env.baseApi.account_base_url + '/user';
// const auroraBaseUrl = process.env.baseApi.aurora_base_url+'/accounts'

export default ($axios, countryCode) => ({
	getCategories() {
		return $axios.get(`${accountBaseUrl}/categories`);
	},
	// getAccount(id){
	//   return $axios.get(`/user/account/${id}`)
	// },
	addNewUser(accountId, payload) {
		return $axios.post(`${accountBaseUrl}/${accountId}/users`, payload);
	},
	addShipping(customerId, payload) {
		return $axios
			.post(`${userBaseUrl}/${customerId}/shipping-options`, payload)
			.then((res) => {
				return res;
			})
			.catch((error) => {
				console.log('error', error.response.data.statusCode, error.response.data.errorMessage);
				return {};
			});
	},
	updateShipping(customerId, payload, shippingId) {
		return $axios
			.post(`${userBaseUrl}/${customerId}/shipping-options/${shippingId}`, payload)
			.then((res) => {
				return res;
			})
			.catch((error) => {
				console.log('error', error.response.data.statusCode, error.response.data.errorMessage);
				return {};
			});
	},
	deleteShipping(customerId, shippingId) {
		return $axios
			.delete(`${userBaseUrl}/${customerId}/shipping-options/${shippingId}`)
			.then((res) => {
				return res;
			})
			.catch((error) => {
				console.log('error', error.response.data.statusCode, error.response.data.errorMessage);
				return {};
			});
	},
	addPayment(customerId, payload) {
		return $axios.post(`${userBaseUrl}/${customerId}/payments?country=${countryCode}`, payload);
	},
	updateCreditBilling(customerId, payload, paymentId) {
		return $axios.post(`${userBaseUrl}/${customerId}/payment-options/${paymentId}?country=${countryCode}`, payload);
	},
	getProfileInfo(accountId, customerId) {
		return $axios.get(`${accountBaseUrl}/${accountId}/users/${customerId}?country=${countryCode}`);
	},
	updateProfile(payload) {
		const { accountId, customerId } = payload;
		return $axios.post(`${accountBaseUrl}/${accountId}/users/${customerId}`, payload);
	},
	updateInvoice(customerId, invoiceEmails) {
		return $axios.post(`${userBaseUrl}/${customerId}/invoice-email?invoiceEmail=${invoiceEmails}`);
	},
	getDashboardItems(payload) {
		const { customerId } = payload;
		// return $axios.get(`${accountBaseUrl}/${accountId}/users/${customerId}/dashboard?limit=10&offset=0`,payload) B2B request
		return $axios.get(
			`${accountBaseUrl}/users/${customerId}/dashboard?limit=3&offset=0&country=${countryCode}`,
			payload
		); // D2C request
	},
});
