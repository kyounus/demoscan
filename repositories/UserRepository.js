// const resource = '/user';
// const resource_users = '/users'
// const AURORA_BASE_URL = process.env.ct_api_url
const orderBaseUrl = process.env.baseApi.order_base_url + '/users';
const userBaseUrl = process.env.baseApi.account_base_url + '/user';
const customerBaseUrl = process.env.baseApi.account_base_url + '/users';
const deviceBaseUrl = process.env.baseApi.account_base_url + '/users/devices';
export default ($axios, countryCode) => ({
	create(payload) {
		payload.country = countryCode;
		return $axios.post(`${userBaseUrl}/signup?country=${countryCode}`, payload);
	},
	update_company_info(payload) {
		payload.country = countryCode;
		return $axios.post(`${userBaseUrl}/signup/company-info`, payload);
	},
	update_shipping_choices(payload) {
		const { customerId } = payload;
		delete payload.customerId;
		delete payload.accountId;
		return $axios
			.post(`${userBaseUrl}/${customerId}/shipping-options`, payload)
			.then((res) => {
				console.log('SHIPPING CHOICE RESPONSE');
				return res;
			})
			.catch((error) => {
				console.log('error', error.response.data.statusCode, error.response.data.errorMessage);
				return {};
			});
	},
	update_carrier(payload) {
		const { customerId } = payload;
		delete payload.customerId;
		delete payload.accountId;
		return $axios.post(`${userBaseUrl}/${customerId}/carriers`, payload);
	},
	// create_payment(payload, customerId) {
	//   return $axios.post(`${userBaseUrl}/${customerId}/payments`, payload);
	// },
	update_payment_option(payload) {
		const { customerId, paymentId = '' } = payload;
		delete payload.customerId;
		if (paymentId) {
			return $axios.post(`${userBaseUrl}/${customerId}/payment-options/${paymentId}?country=${countryCode}`, payload);
		} else {
			return $axios.post(`${userBaseUrl}/${customerId}/payments?country=${countryCode}`, payload);
		}
	},
	upload_tax_documents(payload) {
		const { customerId, data } = payload;
		const headers = {
			headers: {
				'Content-Type': 'multipart/form-data',
			},
		};
		return $axios.post(`${userBaseUrl}/files/${customerId}`, data, headers);
	},
	delete_tax_documents(payload) {
		const { fileName, data } = payload;
		const headers = {
			headers: {
				'Content-Type': 'multipart/form-data',
			},
		};
		return $axios.delete(`${userBaseUrl}/files/${fileName}`, data, headers);
	},
	getOrders(customerId) {
		return $axios.get(`${orderBaseUrl}/${customerId}/orders?limit=50&offset=0&country=${countryCode}`);
	},
	getOrder(orderId) {
		return $axios.get(`${orderBaseUrl}/orders/${orderId}?country=${countryCode}`);
	},
	createCarrier(customerId, payload) {
		return $axios.post(`${userBaseUrl}/${customerId}/carriers`, payload);
	},
	updateCarrier(customerId, payload, carrierId) {
		return $axios.post(`${userBaseUrl}/${customerId}/carriers/${carrierId}`, payload);
	},
	getSubscriptions(payload) {
		const { customerId, shippingAddressId = '', paymentId = '' } = payload;
		let $query = '';
		if (shippingAddressId) {
			$query = `&shippingAddressId=${shippingAddressId}`;
		} else if (paymentId) {
			$query = `&paymentId=${paymentId}`;
		}
		return $axios
			.get(`${customerBaseUrl}/${customerId}/subscriptions?limit=200&offset=0${$query}`)
			.then((res) => {
				return res;
			})
			.catch((error) => {
				console.log('error', error.response.data.statusCode, error.response.data.errorMessage);
				return {
					results: [],
					errorCode: error.response.data.statusCode,
					errorMessage: error.response.data.errorMessage,
				};
			});
	},
	updateSubscriptionQuantity(payload) {
		const { customerId, container, key, data } = payload;
		return $axios.post(`${customerBaseUrl}/${customerId}/subscriptions/${container}/${key}`, data);
	},
	getSurveys(channel) {
		return $axios.get(`${customerBaseUrl}/subscriptions/surveys?channel=${channel}`);
	},
	updateReadMessage(payload) {
		const { orderId, readFlag } = payload;
		return $axios.post(`${orderBaseUrl}/orderId/readStatus/${orderId}/${readFlag}`);
	},
	getUserStatus(payload) {
		const { email, source } = payload;
		payload.country = countryCode;
		return $axios.get(
			`${customerBaseUrl}?customerSource=${source}&emailId=${encodeURIComponent(
				email.toLowerCase()
			)}&limit=5&country=${countryCode}`
		);
	},
	forgotPassword(email) {
		return $axios.post(`${userBaseUrl}/forgot-password/?country=${countryCode}`, { email });
	},
	resetPassword(payload) {
		return $axios.post(`${userBaseUrl}/reset-password/?country=${countryCode}`, payload);
	},
	changePassword(payload) {
		return $axios.post(`${userBaseUrl}/change-password/?country=${countryCode}`, payload);
	},
	updateProfileInfo(customerId, payload) {
		return $axios.post(`${customerBaseUrl}/${customerId}?country=${countryCode}`, payload);
	},
	getInactiveSubscription(payload) {
		const { customerId, orderId } = payload;
		return $axios.get(`${customerBaseUrl}/${customerId}/subscriptions/${orderId}`);
	},
	// start Get Customer API
	getCustomerDetails(payload) {
		const { customerId } = payload;
		payload.country = countryCode;
		return $axios.get(`${customerBaseUrl}?customerId=${customerId}&limit=5&country=${countryCode}`);
	},
	// update shipping and payment
	updateCustomer(customerId, payload) {
		payload.country = countryCode;
		return $axios
			.post(`${customerBaseUrl}/${customerId}?country=${countryCode}`, payload)
			.then((res) => {
				return res;
			})
			.catch((error) => {
				console.log('error', error.response.data.statusCode, error.response.data.errorMessage);
				return {};
			});
	},
	signIn(payload) {
		payload.country = countryCode;
		return $axios.post(`${userBaseUrl}/signin/?country=${countryCode}`, payload);
	},
	signOut(payload) {
		payload.country = countryCode;
		return $axios.post(`${userBaseUrl}/logout/?country=${countryCode}`, payload);
	},
	cognitoAttributes(payload) {
		return $axios.post(`${userBaseUrl}/user-attributes/`, payload);
	},
	resetAccess(payload) {
		return $axios.post(`${userBaseUrl}/refresh/?country=${countryCode}`, payload);
	},
	// end
	// Device registration
	deviceRegsiter(payload) {
		return $axios
			.post(deviceBaseUrl, payload)
			.then((res) => {
				return res;
			})
			.catch((error) => {
				console.log('error', error.response.data.statusCode, error.response.data.errorMessage);
				return error.response.data;
			});
	},
	deviceRegsiterUpdate(payload) {
		return $axios
			.post(`${deviceBaseUrl}/${payload.deviceId}`, payload)
			.then((res) => {
				return res;
			})
			.catch((error) => {
				console.log('error', error.response.data.statusCode, error.response.data.errorMessage);
				return error.response.data;
			});
	},
	checkDeviceSerial(id) {
		return $axios
			.post(`${orderBaseUrl}/orders/serialNumber/${id}`)
			.then((res) => {
				return res;
			})
			.catch((error) => {
				console.log(error);
			});
	},
	createSubscription(payload) {
		return $axios
			.post(`${customerBaseUrl}/subscriptions/create/?country=${countryCode}`, payload)
			.then((res) => {
				return res;
			})
			.catch((error) => {
				console.log(error);
			});
	},
});
