const productBaseUrl = process.env.baseApi.product_base_url + '/products';
const categoryBaseUrl = process.env.baseApi.product_base_url + '/categories/products';
export default ($axios, countryCode) => ({
	get(id, customerId) {
		const cusParam = '';
		// if (false) {
		//   // D2C
		//   cusParam = `?customerId=${customerId}`
		// }
		return $axios.get(`${productBaseUrl}/${id}${cusParam}?country=${countryCode}`);
	},
	getBySku(sku, customerId) {
		let cusParam = '';
		if (customerId) {
			cusParam = `&customerId=${customerId}`;
		}
		return $axios.get(`${productBaseUrl}/sku?sku=${sku}${cusParam}&country=${countryCode}`);
	},
	getAllProducts(customerId) {
		const $query = '';
		// if (customerId && false) {
		//   // D2C customer id no need to pass
		//   $query = `&customerId=${customerId}`
		// }
		return $axios.get(`${productBaseUrl}?limit=200&offset=0${$query}&country=${countryCode}`);
	},
	getProductsByCategory(payload) {
		return $axios.get(`${categoryBaseUrl}?categoryKeys=${payload}`);
	},
});
