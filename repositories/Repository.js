import CartRepository from '~/repositories/CartRepository';
import UserRepository from '~/repositories/UserRepository';
import ProductRepository from '~/repositories/ProductRepository';
import checkoutRepository from '~/repositories/checkoutRepository';
import AccountRepository from '~/repositories/AccountRepository';
export default ($axios, countryCode) => ({
	cart: CartRepository($axios, countryCode),
	user: UserRepository($axios, countryCode),
	product: ProductRepository($axios, countryCode),
	checkout: checkoutRepository($axios, countryCode),
	account: AccountRepository($axios, countryCode),
});
