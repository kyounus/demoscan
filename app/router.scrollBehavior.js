import scrollToHash from '@/utils/scroll-to-hash';

// https://github.com/nuxt/nuxt.js/blob/c02ded2d8622c290ba28c696d363426b825a0977/packages/vue-app/template/utils.js#L90
const getMatchedComponents = (route, matches = false, prop = 'components') => {
	return Array.prototype.concat.apply(
		[],
		route.matched.map((m, index) => {
			return Object.keys(m[prop]).map((key) => {
				matches && matches.push(index);
				return m[prop][key];
			});
		})
	);
};

// https://github.com/nuxt/nuxt.js/blob/dev/packages/vue-app/template/router.scrollBehavior.js#L7
if (process.client) {
	if ('scrollRestoration' in window.history) {
		window.history.scrollRestoration = 'manual';

		// reset scrollRestoration to auto when leaving page, allowing page reload
		// and back-navigation from other pages to use the browser to restore the
		// scrolling position.
		window.addEventListener('beforeunload', () => {
			window.history.scrollRestoration = 'auto';
		});

		// Setting scrollRestoration to manual again when returning to this page.
		window.addEventListener('load', () => {
			window.history.scrollRestoration = 'manual';
		});
	}
}

export default function(to, from, savedPosition) {
	// if the returned position is falsy or an empty object,
	// will retain current scroll position.
	let position = false;

	// if no children detected and scrollToTop is not explicitly disabled
	const Pages = getMatchedComponents(to);
	if (Pages.length < 2 && Pages.every((Page) => Page.options.scrollToTop !== false)) {
		// scroll to the top of the page
		position = { x: 0, y: 0 };
	} else if (Pages.some((Page) => Page.options.scrollToTop)) {
		// if one of the children has scrollToTop option set to true
		position = { x: 0, y: 0 };
	}

	// savedPosition is only available for popstate navigations (back button)
	if (savedPosition) {
		position = savedPosition;
	}

	const nuxt = window.$nuxt;
	let isHashDiffOnly = false;

	// triggerScroll is only fired when a new component is loaded
	if (to.path === from.path && to.hash !== from.hash) {
		isHashDiffOnly = true;
		nuxt.$nextTick(() => nuxt.$emit('triggerScroll'));
	}

	return new Promise((resolve) => {
		// wait for the out transition to complete (if necessary)
		nuxt.$once('triggerScroll', () => {
			// coords will be used if no selector is provided,
			// or if the selector didn't match any element.
			if (to.hash) {
				let hash = to.hash;
				// CSS.escape() is not supported with IE and Edge.
				if (typeof window.CSS !== 'undefined' && typeof window.CSS.escape !== 'undefined') {
					hash = '#' + window.CSS.escape(hash.substr(1));
				}

				if (isHashDiffOnly) {
					scrollToHash(to.hash);
				} else {
					try {
						if (document.querySelector(hash)) {
							const headerHeight = document.body.style.getPropertyValue('--header-height');
							// scroll to anchor by returning the selector
							position = {
								selector: hash,
								offset: { y: parseFloat(headerHeight) },
							};
						}
					} catch (e) {
						console.warn(
							'Failed to save scroll position. Please add CSS.escape() polyfill (https://github.com/mathiasbynens/CSS.escape).'
						);
					}
				}
			}
			resolve(position);
		});
	});
}
