import Vue from 'vue';

export default ({ app, store }) => {
	if (process.client && !Vue.prototype.$isNotFirstPageLoad) {
		Vue.set(app, '$isNotFirstPageLoad', true);
		Vue.set(app.context, '$isNotFirstPageLoad', true);
		Vue.set(store, '$isNotFirstPageLoad', true);
		Vue.prototype.$isNotFirstPageLoad = true;
	}
};
