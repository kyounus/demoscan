const _ = require('lodash');
// Use a random port number for the mock API by default,
// to support multiple instances of Jest running
// simultaneously, like during pre-commit lint.
const MOCK_API_PORT = process.env.MOCK_API_PORT || _.random(9000, 9999);

module.exports = {
	setupFiles: ['<rootDir>/tests/unit/setup'],
	setupFilesAfterEnv: ['<rootDir>/tests/unit/matchers'],
	testMatch: ['**/*.{test,unit,spec}.js', '**/__tests__/*.test.js'],
	moduleFileExtensions: ['js', 'json', 'vue'],
	transform: {
		'^.+\\.vue$': 'vue-jest',
		'^.+\\.js$': 'babel-jest',
	},
	moduleNameMapper: {
		// Transform any static assets to empty strings
		'\\.(jpe?g|png|gif|webp|svg|mp4|webm|ogg|mp3|wav|flac|aac|woff2?|eot|ttf|otf)$':
			'<rootDir>/tests/unit/fixtures/empty-string.js',
		...require('./aliases.config').jest,
		'^~\\/(.*)$': '<rootDir>/$1',
		'^@\\/(.*)$': '<rootDir>/$1',
		'^@design$': '<rootDir>/design/index.scss',
		'^~accoutrement\\/(.*)$': '<rootDir>/node_modules/accoutrement/$1',
	},
	snapshotSerializers: ['jest-serializer-vue'],
	coverageDirectory: '<rootDir>/tests/unit/coverage',
	collectCoverageFrom: [
		'components/*.vue',
		'layouts/*.vue',
		'middleware/**/*.js',
		'pages/**/*.vue',
		'plugins/**/*.js',
		'store/**/*.js',
		'!**/node_modules/**',
	],
	// https://facebook.github.io/jest/docs/en/configuration.html#testurl-string
	// Set the `testURL` to a provided base URL if one exists, or the mock API base URL
	// Solves: https://stackoverflow.com/questions/42677387/jest-returns-network-error-when-doing-an-authenticated-request-with-axios
	testURL: process.env.API_BASE_URL || `http://localhost:${MOCK_API_PORT}`,

	reporters: [
		'default',
		['jest-junit', { outputDirectory: '<rootDir>/tests/tests-results/jest', outputName: 'results.xml' }],
	],
};
