export const toPrLocale = (locale) =>
	['en-us', 'en-ca'].includes(locale) ? `${locale.slice(0, 2)}_${locale.slice(3).toUpperCase()}` : 'en_US';
