import jump from 'jump.js';

// https://github.com/vuejs/vue-router/blob/a4ec3e25d87d41898f25998bd76508e83ce3d9f5/src/util/scroll.js#L96
const getElementPosition = (el, offset) => {
	const docEl = document.documentElement;
	const docRect = docEl.getBoundingClientRect();
	const elRect = el.getBoundingClientRect();
	return {
		x: elRect.left - docRect.left - (offset.x || 0),
		y: elRect.top - docRect.top - (offset.y || 0),
	};
};

const handleBlur = (e) => {
	e.target.removeAttribute('tabindex');
	e.target.removeEventListener('blur', handleBlur);
};

export default (hash, { animate = true } = {}) => {
	if (!hash) return;

	if (typeof window.CSS !== 'undefined' && typeof window.CSS.escape !== 'undefined') {
		hash = '#' + window.CSS.escape(hash.substr(1));
	}

	const el = document.querySelector(hash);

	if (el) {
		const headerHeight = parseFloat(document.body.style.getPropertyValue('--header-height'));

		if (animate) {
			jump(el, { duration: 300, offset: headerHeight * -1 });
		} else {
			window.scrollTo(0, getElementPosition(el, { y: headerHeight }).y);
		}

		if (el.tabIndex < 0 && !el.hasAttribute('tabindex')) {
			el.setAttribute('tabindex', -1);
			el.addEventListener('blur', handleBlur);
		}

		el.focus();
	}
};
