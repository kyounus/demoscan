import Prismic from 'prismic-javascript';
import axios from 'axios';
import { prismicLinkResolver } from '../utils/prismic-helpers';
import { asArray } from '../utils';

const {
	PRISMIC_ENDPOINT,
	PRISMIC_ACCESS_TOKEN,
	POWERREVIEWS_READ_URL,
	POWERREVIEWS_MERCHANT_ID_US,
	POWERREVIEWS_MERCHANT_ID_CA,
	POWERREVIEWS_READ_API_KEY,
} = process.env;
const exclude = ['global'];
const langs = ['en-us', 'en-ca', 'en-gb'];
const reviewPageToReviewId = {
	'air-reviews': 'molekule-air',
	'air-mini-reviews': 'molekule-air-mini',
	'air-mini-plus-reviews': 'molekule-air-mini-plus',
	'air-pro-reviews': 'molekule-air-pro',
};
const reviewPages = Object.keys(reviewPageToReviewId);

async function getLastmodForReviewPage(reviewPage, locale) {
	locale = locale === 'en-ca' ? 'en_CA' : 'en_US';
	const getUrl = (locale) => {
		const merchantId = locale === 'en_CA' ? POWERREVIEWS_MERCHANT_ID_CA : POWERREVIEWS_MERCHANT_ID_US;
		return `${POWERREVIEWS_READ_URL}/m/${merchantId}/l/${locale}/product/${reviewPageToReviewId[reviewPage]}/reviews?apikey=${POWERREVIEWS_READ_API_KEY}&sort=Newest&paging.size=1`;
	};

	const {
		data: {
			results: [review],
		},
	} = await axios.get(getUrl(locale));

	return new Date(review.reviews[0].details.updated_date);
}

export default async () => {
	const api = await Prismic.getApi(PRISMIC_ENDPOINT, { accessToken: PRISMIC_ACCESS_TOKEN });
	const prismicDocuments = [];
	const queryConfig = { pageSize: 50, lang: '*' };
	const initialPrismicResponse = await api.query('', { ...queryConfig, page: 1 });
	prismicDocuments.push(...initialPrismicResponse.results);

	if (initialPrismicResponse.total_pages > 1) {
		for (let i = 0; i < initialPrismicResponse.total_pages - 1; i++) {
			const res = await api.query('', { ...queryConfig, page: i + 2 });
			prismicDocuments.push(...res.results);
		}
	}

	const prismicDocumentsNormalized = prismicDocuments.reduce(
		(final, doc) => {
			if (exclude.includes(doc.type)) return final;

			const { lang } = doc;
			if (!langs.includes(lang)) return final;

			const endpoint = prismicLinkResolver(doc);

			const canonicalPath = doc.data.seo_canonical_path;
			const hasNoindex = asArray(doc.data.seo_additional_meta_tags).some(
				({ property, value }) => property === 'robots' && value === 'noindex'
			);

			if (!hasNoindex && (!canonicalPath || (canonicalPath && endpoint === canonicalPath))) {
				final[lang][endpoint] = { lastmod: doc.last_publication_date };
			}

			return final;
		},
		{ ...Object.fromEntries(langs.map((lang) => [lang, {}])) }
	);

	const routes = [];

	await Promise.all(
		Object.entries(prismicDocumentsNormalized).reduce((final, [lang, endpoints]) => {
			final.push(
				...Object.entries(endpoints).map(async ([endpoint, doc]) => {
					const endpointWithoutSlash = endpoint.slice(1);
					const lastmod = new Date(doc.lastmod);
					let reviewLastmod;
					if (reviewPages.includes(endpointWithoutSlash)) {
						try {
							reviewLastmod = await getLastmodForReviewPage(endpointWithoutSlash, lang);
						} catch (e) {
							console.error('Error getting reviews data:', e);
						}
					}

					routes.push({
						url: lang === 'en-ca' ? `/ca-en${endpoint}` : endpoint,
						name: lang === 'en-ca' ? `${endpointWithoutSlash}___en-ca` : `${endpointWithoutSlash}___en`,
						lastmod: reviewLastmod ? new Date(Math.max(reviewLastmod, lastmod)).toISOString() : lastmod,
					});
				})
			);

			return final;
		}, [])
	);

	return routes;
};
