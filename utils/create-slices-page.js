import getMetadata from '@/utils/get-metadata';
import { loadComponentsBeforeRender, getSliceComponents } from '@/utils/async-components-helpers';

const runCallback = (fn, defaultVal, ...params) =>
	(typeof fn === 'function' && fn(...params)) ||
	(typeof defaultVal === 'function' && defaultVal(...params)) ||
	defaultVal;

const renameSlices = (slices, sliceNamesMap) =>
	slices.map((slice) => {
		const { sliceType } = slice;

		return {
			...slice,
			sliceType: sliceNamesMap[sliceType] || sliceType,
		};
	});

export default (
	pageName,
	{
		pageRequest,
		additionalRequests,
		additionalSliceProps,
		pageComponentData,
		sliceNamesMap = {},
		asyncDataResponseFormatter = (res) => res,
	} = {}
) => (PageComponent) => {
	if (!PageComponent) PageComponent = require('@components/slices-renderer').default;

	return {
		name: pageName,

		computed: {
			sliceComponents() {
				return getSliceComponents(
					renameSlices(this.page.slices, sliceNamesMap),
					runCallback(additionalSliceProps, {}, this)
				);
			},
		},

		async asyncData(ctx) {
			const return404 = () => ctx.error({ statusCode: 404, message: 'This page could not be found' });
			let response = {};

			try {
				const responses = await Promise.all(
					[
						runCallback(
							pageRequest,
							async () => await ctx.store.dispatch('pages/FETCH_PAGE_DATA', { pageName, req: ctx.req }),
							ctx
						),
						runCallback(additionalRequests, null, ctx),
					].filter(Boolean)
				);

				response = { page: responses[0] };
				if (responses[1]) response = { ...response, ...responses[1] };

				if (!Object.keys(response.page).length) {
					return404();
					return;
				}
			} catch (e) {
				if (e.response && e.response.status === 404) {
					return404();
					return;
				} else {
					const { status = 500, message = '' } = e.response;

					ctx.error(status && message ? { status, message } : {});
				}
			}

			await loadComponentsBeforeRender(response.page.slices);

			return asyncDataResponseFormatter(response);
		},

		render(createElement) {
			const dataObject = runCallback(pageComponentData, {}, this);

			return createElement(PageComponent, {
				...dataObject,
				props: {
					...dataObject.props,
					sliceComponents: this.sliceComponents,
				},
			});
		},

		head() {
			return this.page
				? getMetadata(
						{
							...this.page.seo,
							path: this.$route && this.$route.path,
						},
						this.$sentry
				  )
				: {};
		},
	};
};
