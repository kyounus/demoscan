export const camelCase = (str) => str.replace(/_+(.)/g, (_, c) => c.toUpperCase());

export const camelToSnakeCase = (str) => str.replace(/[A-Z]/g, (letter) => `_${letter.toLowerCase()}`);

export const pascalCase = (str) => camelCase(str).replace(/^(.)/, (_, c) => c.toUpperCase());

export const asArray = (obj) => (!obj ? [] : Array.isArray(obj) ? obj : [obj]);

export const fromEntries = (iterable) =>
	[...iterable].reduce((obj, [key, val]) => {
		obj[key] = val;
		return obj;
	}, {});

export const chunkArray = (myArray, chunkSize) => {
	const arrayLength = myArray.length;
	const results = [];

	for (let index = 0; index < arrayLength; index += chunkSize) {
		const myChunk = myArray.slice(index, index + chunkSize);
		results.push(myChunk);
	}

	return results;
};
