import propsFromComponentNames from '@/utils/props-from-component-names';
import { fromEntries } from '@/utils';

const componentNamesFromSliceNames = {
	ProductsGrouped: 'BlockProductsGrouped',
	Faq: 'BlockFaq',
	ProductsSpecial: 'BlockProductsSpecial',
	Alert: 'BlockAlert',
	ComparisonTable4Columns: 'BlockComparisonTable',
	ComparisonTable: 'BlockComparisonTable',
	AddToCartV2: 'BlockAddToCartV2',
	PageBanner: 'BlockPageBanner',
	Timeline: 'BlockTimeline',
	Video: 'BlockVideo',
	ProductBanner: 'BlockProductBanner',
	ProductsV2: 'BlockProductsV2',
	Product: 'BlockProduct',
	MultiColumnLayout: 'BlockMultiColumnLayout',
	TechPath: 'BlockTechPath',
	Review: 'BlockReview',
	ComparisonTable3Columns: 'BlockSpecsTables',
	Awards: 'BlockAwards',
	Testimonials: 'BlockTestimonials',
	TwoVideosCta: 'BlockTwoVideosAndCTA',
	Social: 'BlockSocial',
	Filters: 'BlockFilters',
	Microscope: 'BlockMicroscopeNeverHydrate',
	Reports: 'BlockReports',
	Downloads: 'BlockDownloads',
	ReviewsReviews: 'BlockListing',
	ReviewsSocial: 'BlockSocialReviews',
	Cta: 'BlockCTA',
	Ctas: 'BlockCtas',
	StatisticsV3: 'BlockStatisticsV3',
	B2bTestimonials: 'BlockB2bTestimonials',
	AnimatedGraphics: 'BlockAnimatedGraphics',
	TwoSlideVideoCarousel: 'BlockTwoVideoTabPanels',
	SegmentForm: 'BlockSheeridForm',
	FaqSection: 'BlockSheeridCopy',
	Features: 'BlockFeaturesNeverHydrate',
	BannerInfo: 'BlockBannerInfoNeverHydrate',
	Heading: 'BlockHeadingNeverHydrate',
	Founders: 'BlockFoundersNeverHydrate',
	Reviews: 'BlockReviewsNeverHydrate',
	Specifications: 'BlockSpecsNeverHydrate',
	ContentSection: 'BlockContentNeverHydrate',
	ImagesGrid: 'BlockImagesGridNeverHydrate',
	Science: 'BlockScienceNeverHydrate',
	HeaderBanner: 'BlockHeaderBannerNeverHydrate',
	MultiColumnText: 'BlockMultiColumnTextNeverHydrate',
	CaptionCopy: 'BlockCaptionCopyNeverHydrate',
	VideoCarousel: 'BlockVideoCarousel',
	ComparisonTableV2: 'BlockComparisonTableV2',
	PromoBarVariations: 'BlockPromoBarVariations',
	Alert1: 'BlockAlertV1',
	FeatureVariations: 'BlockFeatureVariations',
};

const componentLoaders = {
	BlockProductsGrouped: () => import('@components/block-products-grouped'),
	BlockFaq: () => import('@components/block-faq'),
	BlockProductsSpecial: () => import('@components/block-products-special'),
	BlockAlert: () => import('@components/block-alert'),
	BlockComparisonTable: () => import('@components/block-comparison-table'),
	BlockAddToCartV2: () => import('@components/block-add-to-cart-v2'),
	BlockPageBanner: () => import('@components/block-page-banner'),
	BlockTimeline: () => import('@components/block-timeline'),
	BlockVideo: () => import('@components/block-video'),
	BlockProductBanner: () => import('@components/block-product-banner'),
	BlockProductsV2: () => import('@components/block-products-v2'),
	BlockProduct: () => import('@components/block-product'),
	BlockMultiColumnLayout: () => import('@components/block-multi-column-layout'),
	BlockTechPath: () => import('@components/block-tech-path'),
	BlockReview: () => import('@components/block-review'),
	BlockSpecsTables: () => import('@/pages/air-purifier-mini/-block-specs-tables'),
	BlockAwards: () => import('@components/block-awards'),
	BlockTestimonials: () => import('@/pages/home/-block-testimonials'),
	BlockTwoVideosAndCTA: () => import('@components/block-two-videos-and-cta'),
	BlockSocial: () => import('@components/block-social'),
	BlockFilters: () => import('@/pages/technology/-block-filters'),
	BlockReports: () => import('@/pages/technology/-block-reports'),
	BlockDownloads: () => import('@/pages/papers/-block-downloads'),
	BlockListing: () => import('@/pages/reviews/-block-listing'),
	BlockSocialReviews: () => import('@/pages/reviews/-block-social'),
	BlockCTA: () => import('@components/block-cta'),
	BlockCtas: () => import('@components/block-ctas'),
	BlockStatisticsV3: () => import('@components/block-statistics-v3'),
	BlockB2bTestimonials: () => import('@components/block-b2b-testimonials'),
	BlockAnimatedGraphics: () => import('@components/block-animated-graphics'),
	BlockTwoVideoTabPanels: () => import('@components/block-two-video-tab-panels'),
	BlockSheeridForm: () => import('@components/block-sheerid-form'),
	BlockSheeridCopy: () => import('@components/block-sheerid-copy'),
	BlockComparisonTableV2: () => import('@components/block-comparison-table-v2'),
	BlockPromoBarVariations: () => import('@components/block-promo-bar-variations'),
	// We add 'NeverHydrate' to components that consist of static content
	BlockFeaturesNeverHydrate: () => import('@components/block-features'),
	BlockReviewsNeverHydrate: () => import('@components/block-reviews'),
	BlockBannerInfoNeverHydrate: () => import('@components/block-banner-info'),
	BlockHeadingNeverHydrate: () => import('@components/block-heading'),
	BlockFoundersNeverHydrate: () => import('@/pages/about/-block-founders'),
	BlockSpecsNeverHydrate: () => import('@components/block-specs'),
	BlockContentNeverHydrate: () => import('@components/block-content'),
	BlockImagesGridNeverHydrate: () => import('@components/block-images-grid'),
	BlockScienceNeverHydrate: () => import('@/pages/technology/-block-science'),
	BlockMicroscopeNeverHydrate: () => import('@/pages/technology/-block-microscope'),
	BlockHeaderBannerNeverHydrate: () => import('@components/block-header-banner'),
	BlockMultiColumnTextNeverHydrate: () => import('@components/block-multi-column-text'),
	BlockCaptionCopyNeverHydrate: () => import('@components/block-caption-copy'),
	BlockVideoCarousel: () => import('@components/block-video-carousel'),
	BlockAlertV1: () => import('@components/block-alert-v1'),
	BlockFeatureVariations: () => import('@components/block-feature-variations'),
};

const resolvedComponents = {};

const components = process.server
	? componentLoaders
	: fromEntries(
			Object.entries(componentLoaders).map(([name, loader]) => [
				name,
				(resolve) => {
					if (resolvedComponents[name]) {
						resolve(resolvedComponents[name]);
						return;
					}

					const loadingPromise = loader();
					loadingPromise.then((data) => {
						resolvedComponents[name] = data;
					});
					return loadingPromise;
				},
			])
	  );

const loadComponentsBeforeRender = (slices) => {
	if (process.server) return;
	return Promise.all(
		slices
			.map((slice) => componentNamesFromSliceNames[slice.sliceType])
			.filter((name, i, arr) => name && arr.indexOf(name) === i && !resolvedComponents[name] && componentLoaders[name])
			.map((name) => componentLoaders[name]().then((res) => (resolvedComponents[name] = res)))
	);
};

const getSliceComponents = (slices, additionalSliceProps = {}) => {
	return slices.map((slice, i) => {
		const componentName = componentNamesFromSliceNames[slice.sliceType];
		if (!componentName) return { slice, props: {}, name: slice.sliceType };
		const component = components[componentName];
		const manualProps =
			propsFromComponentNames[componentName] && propsFromComponentNames[componentName](slice, additionalSliceProps);
		additionalSliceProps = {
			index: i,
			...additionalSliceProps,
		};

		return {
			is: component,
			props: manualProps
				? manualProps.usesAutoProps
					? { ...slice.props, ...manualProps }
					: manualProps
				: { ...slice.props, sliceLabel: slice.sliceLabel },
			name: componentName,
			...additionalSliceProps,
		};
	});
};

export { loadComponentsBeforeRender, getSliceComponents };
