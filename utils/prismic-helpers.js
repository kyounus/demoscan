export const prismicLinkToComponent = (link, newWindow = false) => {
	if (!link) return { element: 'span', props: {} };

	let { url } = link;

	if (url.startsWith('https://molekule.com/')) {
		url = url.substr(20);
	}

	let element = '';
	let props = null;

	if (url.startsWith('/go') || !url.startsWith('/')) {
		element = 'a';
		props = {
			href: url,
			rel: 'noopener',
		};
		if (newWindow) {
			props.target = '_blank';
		}
	} else {
		element = 'nuxt-link';
		props = {
			to: url,
		};
	}

	return { element, props };
};

const knownDocuments = {
	air_purifier_mh1: 'air-purifier-air',
	air_purifier_pro: 'air-purifier-air-pro',
	homepage: '',
	homepage_b: 'homeb',
	channel_hp: 'channelhp',
	global: '',
	minimo: 'air-purifier-mini',
	fda_cleared_mini: 'fda-cleared-mini',
	product_page: 'air-purifiers',
	write_a_review: 'write-a-review',
	returns: 'returns-and-warranty',
	a11y_commitment: 'accessibility-commitment',
	reviews_air: 'air-reviews',
	reviews_air_mini: 'air-mini-reviews',
	reviews_air_mini_plus: 'air-mini-plus-reviews',
	reviews_air_pro: 'air-pro-reviews',
	air_pro_rx: 'air-purifier-pro-rx',
	healthcare_landing: 'healthcare',
	press_page: 'press',
	return_to_office: 'return-to-office',
	enterprise: 'business',
	b2b_inquiries: 'business/inquiries',
	uofm2020_webinar: 'webinars/UofM',
	sheerid_landing_page: 'military-healthcare-professionals-first-responder-teacher-discount',
	healthcare_inquiries: 'healthcare-inquiries',
	nominate_a_small_business: 'nominate-a-small-business',
	join_small_business_community: 'join-small-business-community',
	crrsa_act_education: 'crrsa-act-education',
	hvac_inquiries: 'business/hvac-inquiries',
	about: 'about',
};

const unusualPrefixes = { 'en-us': '/' };

export const prismicLinkResolver = (doc) => {
	//   id: 'XOe0qioAAD4A8zVe',
	//   type: 'air_purifier_mh1',
	//   tags: [],
	//   slug: 'air-purifier-mh1',
	//   lang: 'en-us',
	//   linkType: 'Document',
	//   isBroken: false
	// }
	let link = '';

	if (knownDocuments[doc.type] || knownDocuments[doc.type] === '') {
		link = knownDocuments[doc.type];
	} else if (doc.type === 'page') {
		link = doc.uid.replace(/----/g, '/');
	} else if (!doc.type.includes('_')) {
		link = doc.type;
	}

	return `${unusualPrefixes[doc.lang] || (doc.lang && `/${doc.lang.split('-').reverse().join('-')}/`) || ''}${link}`;
};

export const currentPrismicPage = (doc, page) => {
	return knownDocuments[doc] === page;
};
