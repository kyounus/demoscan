export default (url, route) => {
	let path = '';
	let appendMolekule = true;

	if (url.startsWith('https://molekule.com/')) {
		path = url.slice(20);
	} else if (url.startsWith('https://id/')) {
		path = `${this.$route.path}#${url.slice(11)}`;
	} else if (url.startsWith('http')) {
		path = url;
		appendMolekule = false;
	} else {
		path = url;
	}

	return !appendMolekule ? path : `https://molekule.com${path}`;
};
