export const alpha2ToDefaultLocale = {
	US: 'en-us',
	CA: 'ca-en',
	IN: 'in-en',
	JP: 'jp-ja',
	KR: 'kr-ko',
	GB: 'gb-en',
	FR: 'eu-en',
	DE: 'eu-en',
	SE: 'eu-en',
};

export const supportedCountryAlpha2Values = ['US', 'CA', 'IN', 'JP', 'KR', 'GB', 'FR', 'DE', 'SE'];

export const otherLocales = ['ca-en', 'in-en', 'jp-ja', 'kr-ko', 'gb-en', 'eu-en', 'eu-en', 'eu-en'];

export const defaultLocaleToAlpha2 = Object.fromEntries(
	Object.entries(alpha2ToDefaultLocale).map(([alpha2, locale]) => [locale, alpha2])
);

export const locales = Object.values(alpha2ToDefaultLocale);

export const getLocaleMatchInRoutePath = (path) => {
	const routeMatch = path.match(/^\/([a-z]{2}-[a-z]{2})(\/|$)/);
	const localeMatchInRoute =
		Array.isArray(routeMatch) && routeMatch[1] && Object.values(alpha2ToDefaultLocale).includes(routeMatch[1])
			? routeMatch[1]
			: '';

	return localeMatchInRoute;
};
