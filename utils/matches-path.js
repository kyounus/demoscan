import { pathToRegexp } from 'path-to-regexp';

const pathToRegexpCache = {};

export default (currentPath, paths) =>
	paths
		.map((p) => p.trim())
		.filter(Boolean)
		.some((path) => {
			if (!pathToRegexpCache[path]) {
				pathToRegexpCache[path] = pathToRegexp(path);
			}

			const pathRegex = pathToRegexpCache[path];
			return currentPath.match(pathRegex);
		});
