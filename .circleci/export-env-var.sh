export COMMIT_HASH=${CIRCLE_SHA1}

touch .env
export SENTRY_DSN=$SENTRY_DSN_TEST
export SENTRY_RELEASE=$COMMIT_HASH
export SENTRY_DISABLED=false
export API_URL='http://localhost:3000'
export API_URL_BROWSER='/'
export POWERREVIEWS_READ_URL='https://readservices-b2c.powerreviews.com'
export POWERREVIEWS_WRITE_URL='https://writeservices.powerreviews.com'
export PRISMIC_ACCESS_TOKEN=$PRISMIC_ACCESS_TOKEN
export PRISMIC_ENDPOINT='https://molekule.cdn.prismic.io/api/v2'
export ITERABLE_API_KEY='f5ff7276a5a04c7489c28c39f00c7065'
export ITERABLE_NEWSLETTER_CAMPAIGN_ID=309742
export CACHE_AWS_ACCESS_KEY_ID='AKIA3ECNH3P5FRVDFD4P'
export CACHE_AWS_SECRET_ACCESS_KEY='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'
export MAGENTO_CART_HASH_CREATE='--create--'
export SG_API_KEY='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'
export SG_PURCHASE_INQUIRY_TEMPLATE_ID='d-7f159bd54aa3499bafda876382f50b89'
export SG_CONTENT_DOWNLOAD_TEMPLATE_ID='d-f9a4d61b88884e99b44e61e2290d0d78'
export SG_CRRSA_DOWNLOAD_TEMPLATE_ID='d-37e1820b66404ab294732a7ea0944371'
export SG_SMB_NOMINATION_TEMPLATE_ID="d-baa8184b4c704448aa4b75434675f8a3"
export SG_SMB_JOIN_TEMPLATE_ID="d-bbde723d1b404ca4aded83eaeb66ce92"
export ZENDESK_SELL_LEADS_ENDPOINT='https://api.getbase.com/v2/leads'
export ZENDESK_SELL_API_KEY='b8d49f50f0c408bb81b4c40a10a4a950ff16339faa4f4ed272df6201ebe7f0fe'
export ZENDESK_SOURCE_ID=1431558
export ZENDESK_OWNER_ID=1826747
export ZENDESK_WEB_WIDGET_ID='edd021cc-7c89-4ec0-9cfa-ba3ee6579e45' # https://molekule1624563179.zendesk.com/ || Molekule Sandbox V2
export FORMS_PURCHASE_ENDPOINT='https://script.google.com/a/molekule.com/macros/s/AKfycbzxhpO1x__F8ItUNsf1SKQmeBEea8AwuyI17Gt1/exec'
export FORMS_SMB_ENDPOINT='https://script.google.com/macros/s/AKfycbxZ6u-vnfnh24sp-ZfBX0-qFVTx0Ei6OlDPtiZzJWLNUXqRF_ioGILQ/exec'
export FORMS_FROM='enterprise@molekule.com'
export FORMS_FROM_NAME='Molekule'
export FORMS_PURCHASE_TO='b2binquiries@molekule.com'
export FORMS_SMB_TO="nominate@molekule.com"
export FORMS_SMB_FROM='nominate@molekule.com'
export FORMS_SHEETS_SECRET='Co9ufyxH0x9inAYnCI16XkcIXs2XBb'
export RECAPTCHA_SITE_KEY='6Lci-JYaAAAAAMtkDyY4K-V5DzReCP-zFloLPGZA'
export RECAPTCHA_SECRET_KEY='6Lci-JYaAAAAAEBfkk2wO6IcKcZV2CTvuqg-_Z9j'
export TALKABLE_SITE_ID='molekule'
export CMS_DEFAULT_LOCALE='en-us'

if
  [ "${CIRCLE_TAG}" = "test" ] || [ "${CIRCLE_TAG}" = "test-magento-anatta" ] ||
  [ "${CIRCLE_TAG}" = "aurora-dev" ] || [ "${CIRCLE_TAG}" = "aurora-qa" ] || [ "${CIRCLE_TAG}" = "aurora-preprod" ] || [ "${CIRCLE_TAG}" = "aurora-prod" ] ||
  [ "${CIRCLE_TAG}" = "apac-dev" ] || [ "${CIRCLE_TAG}" = "uk-qa" ] || [ "${CIRCLE_TAG}" = "uk-preprod" ]; then
  export MK_ENV=test
  export MAGENTO_URL='https://dev-anatta.molekule.com/go/'
  export POWERREVIEWS_READ_API_KEY='77c06d91-9034-4777-af0b-6e52db7d51bb'
  export POWERREVIEWS_WRITE_API_KEY='b7d6e869-a6bd-40d2-a07a-e8446d38ef80'
  export POWERREVIEWS_MERCHANT_GROUP_ID='79865'
  export POWERREVIEWS_MERCHANT_ID_US='428472'
  export POWERREVIEWS_MERCHANT_ID_CA='233582'
  export POWERREVIEWS_IOVATION_URL='https://ci-mpsnare.iovation.com/snare.js'
  export TEALIUM_SYNC_URL='//tags.tiqcdn.com/utag/molekule/main/dev/utag.sync.js'
  export TEALIUM_ASYNC_URL='//tags.tiqcdn.com/utag/molekule/main/dev/utag.js'
  export CLYDE_URL="https://api.sb.joinclyde.com/scripts/widget.js"
  export CLYDE_KEY="ck_live_ZD9d44cCYjEyEzsm"
  export CACHE_CLOUDFRONT_ID='E1Q32BZ03G7BHM'
  export GTM_URL='https://www.googletagmanager.com/gtag/js?id=UA-76485335-6'
  export CUSTOM_APIS_BASE_URL='https://dev-anatta.molekule.com'
fi

if [ "${CIRCLE_TAG}" = "aurora-dev" ] || [ "${CIRCLE_TAG}" = "aurora-qa" ] || [ "${CIRCLE_TAG}" = "aurora-preprod" ] || [ "${CIRCLE_TAG}" = "aurora-prod" ] ||
   [ "${CIRCLE_TAG}" = "uk-qa" ] || [ "${CIRCLE_TAG}" = "uk-preprod" ] || [ "${CIRCLE_TAG}" = "uk-prod" ]; then
  export PRISMIC_ACCESS_TOKEN=$PRISMIC_DEV_ACCESS_TOKEN
fi

if [ "${CIRCLE_TAG}" = "test-magento-anatta" ]; then
  export MK_ENV=test-magento-anatta
  export CLYDE_URL="https://api.sb.joinclyde.com/scripts/widget.js"
  export CLYDE_KEY="ck_live_ZD9d44cCYjEyEzsm"
  export MAGENTO_URL='https://staging-anatta.molekule.com/go/'
  export CACHE_CLOUDFRONT_ID='E1DVTNVWVTWVAJ'
  export CUSTOM_APIS_BASE_URL='https://staging-anatta.molekule.com'
fi

if [ "${CIRCLE_TAG}" = "production" ] || [ "${CIRCLE_TAG}" = "apac" ]; then
  export MK_ENV=production
  export MAGENTO_URL='https://molekule.com/go/'
  export POWERREVIEWS_READ_API_KEY='610c126e-4704-473b-8462-28a0f2f99e9d'
  export POWERREVIEWS_WRITE_API_KEY='064fbb80-66ec-4b94-bda9-a501351f9249'
  export POWERREVIEWS_MERCHANT_GROUP_ID='80307'
  export POWERREVIEWS_MERCHANT_ID_US='483539'
  export POWERREVIEWS_MERCHANT_ID_CA='610550'
  export POWERREVIEWS_IOVATION_URL='https://mpsnare.iesnare.com/snare.js'
  export TEALIUM_ASYNC_URL='//tags.tiqcdn.com/utag/molekule/main/prod/utag.js'
  export TEALIUM_SYNC_URL='//tags.tiqcdn.com/utag/molekule/main/prod/utag.sync.js'
  export CLYDE_URL="https://js.joinclyde.com/widget.js"
  export CLYDE_KEY="ck_live_MNbaSdyHX4g4KQgy"
  export CACHE_CLOUDFRONT_ID='E347Z7EMCHHSE3'
  export GTM_URL='https://www.googletagmanager.com/gtag/js?id=UA-76485335-2'
  export CUSTOM_APIS_BASE_URL='https://molekule.com'
  export ZENDESK_WEB_WIDGET_ID='2861e6e4-53dc-4557-a041-482d2d09af67' # Production ID, used in molekule.com
fi

if
  [ "${CIRCLE_TAG}" = "aurora-dev" ] || [ "${CIRCLE_TAG}" = "aurora-qa" ] || [ "${CIRCLE_TAG}" = "aurora-preprod" ] || [ "${CIRCLE_TAG}" = "aurora-prod" ] ||
  [ "${CIRCLE_TAG}" = "uk-qa" ] || [ "${CIRCLE_TAG}" = "uk-preprod" ] || [ "${CIRCLE_TAG}" = "uk-prod" ]; then
  export MK_ENV=test
  export MAGENTO_URL='https://molekule.com/go/'
  export POWERREVIEWS_READ_API_KEY='610c126e-4704-473b-8462-28a0f2f99e9d'
  export POWERREVIEWS_WRITE_API_KEY='064fbb80-66ec-4b94-bda9-a501351f9249'
  export POWERREVIEWS_MERCHANT_GROUP_ID='80307'
  export POWERREVIEWS_MERCHANT_ID_US='483539'
  export POWERREVIEWS_MERCHANT_ID_CA='610550'
  export POWERREVIEWS_IOVATION_URL='https://mpsnare.iesnare.com/snare.js'
  export PRISMIC_ACCESS_TOKEN=$PRISMIC_DEV_ACCESS_TOKEN
  export PRISMIC_ENDPOINT='https://molekule-molekule-dev.cdn.prismic.io/api/v2'
  export TEALIUM_ASYNC_URL='//tags.tiqcdn.com/utag/molekule/main/prod/utag.js'
  export TEALIUM_SYNC_URL='//tags.tiqcdn.com/utag/molekule/main/prod/utag.sync.js'
  export CLYDE_URL="https://api.sb.joinclyde.com/scripts/widget.js"
  export CLYDE_KEY="ck_live_ZD9d44cCYjEyEzsm"
  export CACHE_CLOUDFRONT_ID=''
  export GTM_URL='https://www.googletagmanager.com/gtag/js?id=UA-76485335-2'
  export CUSTOM_APIS_BASE_URL='https://aurora-qa.molekule.com/'

  ## Credit Card Service Key
  export STRIPE_PK='pk_test_TYooMQauvdEDq54NiTphI7jx'

  ## CT Microservices
  export CT_PRODUCT_API='https://product-service-dev.molekule/api/v1'
  export CT_CART_API='https://cart-service-dev.molekule.com/api/v1'
  export CT_CHECKOUT_API='https://checkout-service-dev.molekule.com/api/v1'
  export CT_ORDER_API='https://order-service-dev.molekule.com/api/v1'
  export CT_ACCOUNT_API='https://useraccount-service-dev.molekule.com/api/v1'

  ## CT AWS Configs
  export COGNITO_IDENTITYPOOLID='arn:aws:cognito-idp:us-west-2:787105343984:userpool/us-west-2_OzNe1iT7P'
  export COGNITO_REGION='us-west-2'
  export COGNITO_USERPOOLID='us-west-2_OzNe1iT7P'
  export COGNITO_USERPOOLWEBCLIENTID='3lbibf9435pkatlpmvmnp8j08e'

  ## AFFIRM
  export AFFIRM_PUBLIC_KEY='1VYM05QS77BSXMOD'
  export AFFIRM_PRIVATE_KEY='22MgUPaUH34lp3m031pxMHDiYgzxWyQg'
fi
# if [ "${CIRCLE_TAG}" = "aurora-dev" ]; then
#   export MK_ENV=aurora-dev
# fi

if [ "${CIRCLE_TAG}" = "aurora-qa" ]; then
  export MK_ENV=aurora-qa
  export CT_PRODUCT_API='https://product-service-qa.molekule.com/api/v1'
  export CT_CART_API='https://cart-service-qa.molekule.com/api/v1'
  export CT_CHECKOUT_API='https://checkout-service-qa.molekule.com/api/v1'
  export CT_ORDER_API='https://order-service-qa.molekule.com/api/v1'
  export CT_ACCOUNT_API='https://useraccount-service-qa.molekule.com/api/v1'
fi

if [ "${CIRCLE_TAG}" = "aurora-preprod" ]; then
  export MK_ENV=aurora-preprod
  export CT_PRODUCT_API='https://product-service-preprod.molekule.com/api/v1'
  export CT_CART_API='https://cart-service-preprod.molekule.com/api/v1'
  export CT_CHECKOUT_API='https://checkout-service-preprod.molekule.com/api/v1'
  export CT_ORDER_API='https://order-service-preprod.molekule.com/api/v1'
  export CT_ACCOUNT_API='https://useraccount-service-preprod.molekule.com/api/v1'
fi

if [ "${CIRCLE_TAG}" = "aurora-prod" ]; then
  export MK_ENV=aurora-prod
  export CT_PRODUCT_API='https://product-service-prod.molekule.com/api/v1'
  export CT_CART_API='https://cart-service-prod.molekule.com/api/v1'
  export CT_CHECKOUT_API='https://checkout-service-prod.molekule.com/api/v1'
  export CT_ORDER_API='https://order-service-prod.molekule.com/api/v1'
  export CT_ACCOUNT_API='https://useraccount-service-prod.molekule.com/api/v1'
fi

if [ "${CIRCLE_TAG}" = "uk-qa" ] || [ "${CIRCLE_TAG}" = "uk-preprod" ] || [ "${CIRCLE_TAG}" = "uk-prod" ]; then
  #export CMS_DEFAULT_LOCALE='gb-en'
  #export ZENDESK_WEB_WIDGET_ID='3aeff1da-12cc-4d4c-8217-4cf91bbbaf1f' # https://molekule1624563179.zendesk.com/agent/admin/widget | Molekule UK
  export ZENDESK_WEB_WIDGET_ID='9c270072-5928-4bda-bbb9-1aa047aa27fd' # https://ukmlk.zendesk.com/agent/admin/widget | Developer's temporary widget;

fi

if [ "${CIRCLE_TAG}" = "uk-qa" ]; then
  #Custom APIs base URL
  export CUSTOM_APIS_BASE_URL='https://qa.molekule.uk'

  #Commercetools - Springboot microservices.
  export CT_PRODUCT_API='https://product-service-qa.molekule.uk/api/v1'
  export CT_CART_API='https://cart-service-qa.molekule.uk/api/v1'
  export CT_CHECKOUT_API='https://checkout-service-qa.molekule.uk/api/v1'
  export CT_ORDER_API='https://order-service-qa.molekule.uk/api/v1'
  export CT_ACCOUNT_API='https://useraccount-service-qa.molekule.uk/api/v1'

  #Cloudfront ID
  export CACHE_CLOUDFRONT_ID='EQUD32FRSORBK'
fi

if [ "${CIRCLE_TAG}" = "uk-preprod" ]; then
  #Custom APIs base URL
  export CUSTOM_APIS_BASE_URL='https://preprod.molekule.uk'

  #Commercetools - Springboot microservices.
  export CT_PRODUCT_API='https://product-service-preprod.molekule.uk/api/v1'
  export CT_CART_API='https://cart-service-preprod.molekule.uk/api/v1'
  export CT_CHECKOUT_API='https://checkout-service-preprod.molekule.uk/api/v1'
  export CT_ORDER_API='https://order-service-preprod.molekule.uk/api/v1'
  export CT_ACCOUNT_API='https://useraccount-service-preprod.molekule.uk/api/v1'

  #Cloudfront ID
  export CACHE_CLOUDFRONT_ID='E1NDHAS60123DO'
fi
if [ "${CIRCLE_TAG}" = "uk-prod" ]; then
  #Custom APIs base URL
  # export CUSTOM_APIS_BASE_URL='https://www.molekule.uk'
  export CUSTOM_APIS_BASE_URL='https://preprod.molekule.uk'

  #Commercetools - Springboot microservices.
  export CT_PRODUCT_API='https://product-service-prod.molekule.uk/api/v1'
  export CT_CART_API='https://cart-service-prod.molekule.uk/api/v1'
  export CT_CHECKOUT_API='https://checkout-service-prod.molekule.uk/api/v1'
  export CT_ORDER_API='https://order-service-prod.molekule.uk/api/v1'
  export CT_ACCOUNT_API='https://useraccount-service-prod.molekule.uk/api/v1'

  #Cloudfront ID
  export CACHE_CLOUDFRONT_ID='E3M0QZIOCPYA3F'
fi

echo "MK_ENV=$MK_ENV" >> .env
echo "SENTRY_DSN=$SENTRY_DSN" >> .env
echo "SENTRY_RELEASE=$SENTRY_RELEASE" >> .env
echo "SENTRY_DISABLED=$SENTRY_DISABLED" >> .env
echo "API_URL=$API_URL" >> .env
echo "API_URL_BROWSER=$API_URL_BROWSER" >> .env
echo "POWERREVIEWS_READ_URL=$POWERREVIEWS_READ_URL" >> .env
echo "POWERREVIEWS_READ_API_KEY=$POWERREVIEWS_READ_API_KEY" >> .env
echo "POWERREVIEWS_WRITE_URL=$POWERREVIEWS_WRITE_URL" >> .env
echo "POWERREVIEWS_WRITE_API_KEY=$POWERREVIEWS_WRITE_API_KEY" >> .env
echo "POWERREVIEWS_MERCHANT_GROUP_ID=$POWERREVIEWS_MERCHANT_GROUP_ID" >> .env
echo "POWERREVIEWS_MERCHANT_ID_US=$POWERREVIEWS_MERCHANT_ID_US" >> .env
echo "POWERREVIEWS_MERCHANT_ID_CA=$POWERREVIEWS_MERCHANT_ID_CA" >> .env
echo "POWERREVIEWS_IOVATION_URL=$POWERREVIEWS_IOVATION_URL" >> .env
echo "PRISMIC_ENDPOINT=$PRISMIC_ENDPOINT" >> .env
echo "PRISMIC_ACCESS_TOKEN=$PRISMIC_ACCESS_TOKEN" >> .env
echo "ITERABLE_API_KEY=$ITERABLE_API_KEY" >> .env
echo "ITERABLE_NEWSLETTER_CAMPAIGN_ID=$ITERABLE_NEWSLETTER_CAMPAIGN_ID" >> .env
echo "MAGENTO_URL=$MAGENTO_URL" >> .env
echo "MAGENTO_CART_HASH_CREATE=$MAGENTO_CART_HASH_CREATE" >> .env
echo "CUSTOM_APIS_BASE_URL"=$CUSTOM_APIS_BASE_URL >> .env
echo "TEALIUM_ASYNC_URL=$TEALIUM_ASYNC_URL" >> .env
echo "TEALIUM_SYNC_URL=$TEALIUM_SYNC_URL" >> .env
echo "CLYDE_URL=$CLYDE_URL" >> .env
echo "CLYDE_KEY=$CLYDE_KEY" >> .env
echo "CACHE_AWS_ACCESS_KEY_ID=$CACHE_AWS_ACCESS_KEY_ID" >> .env
echo "CACHE_AWS_SECRET_ACCESS_KEY=$CACHE_AWS_SECRET_ACCESS_KEY" >> .env
echo "CACHE_CLOUDFRONT_ID=$CACHE_CLOUDFRONT_ID" >> .env
echo "SG_API_KEY=$SG_API_KEY" >> .env
echo "SG_PURCHASE_INQUIRY_TEMPLATE_ID=$SG_PURCHASE_INQUIRY_TEMPLATE_ID" >> .env
echo "SG_PURCHASE_INQUIRY_TEMPLATE_ID=$SG_PURCHASE_INQUIRY_TEMPLATE_ID" >> .env
echo "SG_CONTENT_DOWNLOAD_TEMPLATE_ID=$SG_CONTENT_DOWNLOAD_TEMPLATE_ID" >> .env
echo "SG_CRRSA_DOWNLOAD_TEMPLATE_ID=$SG_CRRSA_DOWNLOAD_TEMPLATE_ID" >> .env
echo "SG_SMB_NOMINATION_TEMPLATE_ID"=$SG_SMB_NOMINATION_TEMPLATE_ID >> .env
echo "SG_SMB_JOIN_TEMPLATE_ID"=$SG_SMB_JOIN_TEMPLATE_ID >> .env
echo "ZENDESK_SELL_LEADS_ENDPOINT=$ZENDESK_SELL_LEADS_ENDPOINT" >> .env
echo "ZENDESK_SELL_API_KEY=$ZENDESK_SELL_API_KEY" >> .env
echo "ZENDESK_SOURCE_ID=$ZENDESK_SOURCE_ID" >> .env
echo "ZENDESK_OWNER_ID=$ZENDESK_OWNER_ID" >> .env
echo "ZENDESK_WEB_WIDGET_ID=$ZENDESK_WEB_WIDGET_ID" >> .env
echo "FORMS_PURCHASE_ENDPOINT=$FORMS_PURCHASE_ENDPOINT" >> .env
echo "FORMS_SMB_ENDPOINT=$FORMS_SMB_ENDPOINT" >> .env
echo "FORMS_SHEETS_SECRET=$FORMS_SHEETS_SECRET" >> .env
echo "FORMS_FROM=$FORMS_FROM" >> .env
echo "FORMS_FROM_NAME=$FORMS_FROM_NAME" >> .env
echo "FORMS_PURCHASE_TO=$FORMS_PURCHASE_TO" >> .env
echo "FORMS_SMB_TO=$FORMS_SMB_TO" >> .env
echo "FORMS_SMB_FROM=$FORMS_SMB_FROM" >> .env
echo "RECAPTCHA_SITE_KEY=$RECAPTCHA_SITE_KEY" >> .env
echo "RECAPTCHA_SECRET_KEY=$RECAPTCHA_SECRET_KEY" >> .env
echo "GTM_URL=$GTM_URL" >> .env
echo "# AURORA Configurations" >> .env
echo "CT_PRODUCT_API=$CT_PRODUCT_API" >> .env
echo "CT_CART_API=$CT_CART_API" >> .env
echo "CT_CHECKOUT_API=$CT_CHECKOUT_API" >> .env
echo "CT_ORDER_API=$CT_ORDER_API" >> .env
echo "CT_ACCOUNT_API=$CT_ACCOUNT_API" >> .env
echo "AFFIRM_PUBLIC_KEY=$AFFIRM_PUBLIC_KEY" >> .env
echo "AFFIRM_PRIVATE_KEY=$AFFIRM_PRIVATE_KEY" >> .env
echo "COGNITO_IDENTITYPOOLID=$COGNITO_IDENTITYPOOLID" >> .env
echo "COGNITO_REGION=$COGNITO_REGION" >> .env
echo "COGNITO_USERPOOLID=$COGNITO_USERPOOLID" >> .env
echo "COGNITO_USERPOOLWEBCLIENTID=$COGNITO_USERPOOLWEBCLIENTID" >> .env
echo "STRIPE_PK=$STRIPE_PK" >> .env
echo "TALKABLE_SITE_ID=$TALKABLE_SITE_ID" >> .env
echo "CMS_DEFAULT_LOCALE=$CMS_DEFAULT_LOCALE" >> .env

cat .env
