#!/bin/bash
set -eo pipefail
S3_BUCKET='molekule-frontend-static' # S3 Bucket name for AWS::mo-web

echo "Start deploy"

mv ./.elasticbeanstalk/config.ci.yml ./.elasticbeanstalk/config.yml

if [ "${CIRCLE_TAG}" = "apac" ] || [ "${CIRCLE_TAG}" = "apac-dev" ]; then
  mv ./.elasticbeanstalk/config.apac.yml ./.elasticbeanstalk/config.yml
fi

if [ "${CIRCLE_TAG}" = "uk-qa" ] || [ "${CIRCLE_TAG}" = "uk-preprod" ] || [ "${CIRCLE_TAG}" = "uk-prod" ]; then
  mv ./.elasticbeanstalk/config.uk.yml ./.elasticbeanstalk/config.yml
  S3_BUCKET='molekule-frontend-eu' # S3 Bucket name for AWS::mo-aurora
fi

if [ "${CIRCLE_TAG}" = "uk-qa" ] || [ "${CIRCLE_TAG}" = "uk-preprod" ]; then
  mv ./static/nr-nonprod-uk.js ./static/nr.js
fi

if [ "${CIRCLE_TAG}" = "uk-prod" ]; then
  mv ./static/nr-prod-uk.js ./static/nr.js
fi

# Compress static files with gzip and brotli
# https://blog.e-kursy.it/aws-lambda-edge-brotli/
# https://medium.com/@felice.geracitano/brotli-compression-delivered-from-aws-7be5b467c2e1
cd ./.nuxt/dist/
find ./client -name '*.js' -o -name '*.json' -o -name '*.svg' -o -name '*.woff' -o -name '*.woff2' \
 | xargs -n 1 -I {} sh -c 'mkdir -p ./gz/`dirname $1` && cp {} ./gz/{} && gzip --best ./gz/{} && mv ./gz/{}.gz ./gz/{}' sh {}
find ./client -name '*.js' -o -name '*.json' -o -name '*.svg' -o -name '*.woff' -o -name '*.woff2' \
 | xargs -n 1 -I {} sh -c 'mkdir -p ./br/`dirname $1` && cp {} ./br/{} && brotli --best ./br/{} && mv ./br/{}.br ./br/{}' sh {}
cd ../..

cd ./storybook/
find . \
 -path './gz' -prune \
 -o -path './br' -prune \
 -o -name '*.js' -print \
 -o -name '*.json' -print \
 -o -name '*.svg' -print \
 -o -name '*.woff' -print \
 -o -name '*.woff2' -print \
 | xargs -n 1 -I {} sh -c 'mkdir -p ./gz/`dirname $1` && gzip --best -c {} > ./gz/{}' sh {}
find . \
 -path './gz' -prune \
 -o -path './br' -prune \
 -o -name '*.js' -print \
 -o -name '*.json' -print \
 -o -name '*.svg' -print \
 -o -name '*.woff' -print \
 -o -name '*.woff2' -print \
 | xargs -n 1 -I {} sh -c 'mkdir -p ./br/`dirname $1` && brotli --best -c {} > ./br/{}' sh {}
cd ../

aws s3 cp --recursive --cache-control 'public, max-age=31536000' ./.nuxt/dist/client s3://${S3_BUCKET}/${CIRCLE_TAG}/_nuxt/
aws s3 cp --recursive --cache-control 'public, max-age=31536000' --content-encoding gzip ./.nuxt/dist/gz/client s3://${S3_BUCKET}/${CIRCLE_TAG}/gz/_nuxt/
aws s3 cp --recursive --cache-control 'public, max-age=31536000' --content-encoding br ./.nuxt/dist/br/client s3://${S3_BUCKET}/${CIRCLE_TAG}/br/_nuxt/

aws s3 cp --recursive --cache-control 'public, max-age=31536000' ./storybook s3://${S3_BUCKET}/${CIRCLE_TAG}/storybook/
aws s3 cp --recursive --cache-control 'public, max-age=31536000' --content-encoding gzip ./storybook/gz s3://${S3_BUCKET}/${CIRCLE_TAG}/gz/storybook/
aws s3 cp --recursive --cache-control 'public, max-age=31536000' --content-encoding br ./storybook/br s3://${S3_BUCKET}/${CIRCLE_TAG}/br/storybook/

rm -rf node_modules

yarn --prod --frozen-lockfile

source .env

if [ "${CIRCLE_TAG}" = "test" ] || [ "${CIRCLE_TAG}" = "apac-dev" ]; then
  eb deploy MolekuleFrontend-Test --timeout 40
fi

if [ "${CIRCLE_TAG}" = "test-magento-anatta" ]; then
  eb deploy MolekuleFrontend-TestMagentoAnatta --timeout 40
fi

# Disabling temporarily, to avoid accidntal deployment in production
# if [ "${CIRCLE_TAG}" = "production" ] || [ "${CIRCLE_TAG}" = "apac" ]; then
#   eb deploy MolekuleFrontend-Production --timeout 40
# fi

if [ "${CIRCLE_TAG}" = "aurora-dev" ]; then
  eb deploy MolekuleFrontend-Aurora-Dev --timeout 40
fi

if [ "${CIRCLE_TAG}" = "aurora-preprod" ]; then
  eb deploy MolekuleFrontend-Aurora-Preprod --timeout 40
fi

if [ "${CIRCLE_TAG}" = "aurora-qa" ]; then
  eb deploy MolekuleFrontend-Aurora-QA --timeout 40
fi

if [ "${CIRCLE_TAG}" = "uk-qa" ]; then
  eb deploy MolekuleFrontend-UK-QA --timeout 40
fi

if [ "${CIRCLE_TAG}" = "uk-preprod" ]; then
  eb deploy MolekuleFrontend-UK-Preprod --timeout 40
fi

if [ "${CIRCLE_TAG}" = "uk-prod" ]; then
  eb deploy MolekuleFrontend-UK-Prod --timeout 40
fi

if [[ ! -z "$CACHE_CLOUDFRONT_ID" ]]; then # ==> if not empty of CACHE_CLOUDFRONT_ID
  invalidation=$(aws cloudfront create-invalidation --distribution-id $CACHE_CLOUDFRONT_ID --paths '/*')
  echo $invalidation;

  if [ "${CIRCLE_TAG}" = "test" ] || [ "${CIRCLE_TAG}" = "test-magento-anatta" ] || [ "${CIRCLE_TAG}" = "apac-dev" ] || [ "${CIRCLE_TAG}" = "aurora-dev" ] || [ "${CIRCLE_TAG}" = "aurora-preprod" ] || [ "${CIRCLE_TAG}" = "aurora-qa" ]; then
    # Wait for invlalidation to end so we can do E2E tests
    id=$(echo $invalidation | jq -r '.Invalidation.Id')
    echo "Waiting for invalidation $id"
    aws cloudfront wait invalidation-completed --id $id --distribution-id $CACHE_CLOUDFRONT_ID
  fi
fi
