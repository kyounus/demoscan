import BlockTechPath from './block-tech-path.vue';

describe('@views/technology/BlockTechPath', () => {
	it('exports a valid component', () => {
		expect(BlockTechPath).toBeAComponent();
	});
});
