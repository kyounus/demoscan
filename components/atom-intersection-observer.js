export default {
	props: {
		options: {
			type: Object,
			default: () => ({}),
		},
	},

	data: () => ({
		observer: null,
	}),

	mounted() {
		this.observer = new IntersectionObserver(([entry]) => {
			if (entry && entry.isIntersecting) {
				this.$emit('enter', entry);
			} else if (entry && !entry.isIntersecting) {
				this.$emit('leave', entry);
			}
		}, this.options);

		this.observer.observe(this.$el);
	},

	destroyed() {
		this.observer.disconnect();
	},

	render() {
		return this.$scopedSlots.default();
	},
};
