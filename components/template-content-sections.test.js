import TemplateTemplateContentSections from './template-content-sections.vue';

describe('@components/TemplateTemplateContentSections', () => {
	it('exports a valid component', () => {
		expect(TemplateTemplateContentSections).toBeAComponent();
	});
});
