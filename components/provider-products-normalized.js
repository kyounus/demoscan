import { mapState } from 'vuex';

const generateCtaLink = (ctaLink, products) =>
	ctaLink && ctaLink.url
		? ctaLink
		: { url: `https://molekule.com/add-to-cart?skus=${products.map((product) => product.sku).join(',')}` };

export default {
	props: {
		items: {
			type: Array,
			default: () => [],
		},

		isProductsGrouped: {
			type: Boolean,
			default: false,
		},
	},

	computed: {
		...mapState('magento', ['products']),
		...mapState({
			isUSWebsite: 'isUSWebsite',
			specialPrices: (state) => state.globalData.magento.specialPrices,
		}),

		productsNormalized() {
			const { products, isUSWebsite } = this;

			return this.items.map((item) => {
				const { product, productsFromPrismic } = item;

				// Bundle with products from Prismic
				if (productsFromPrismic && productsFromPrismic.length) {
					const bundleProducts = productsFromPrismic.reduce((final, { multiplier, product }) => {
						multiplier = multiplier && multiplier > 0 ? multiplier : 1;

						for (let i = 0; i < multiplier; i++) {
							final.push(product);
						}

						return final;
					}, []);
					const listPrice = bundleProducts.reduce((acc, product) => acc + product.price, 0);
					const currentPrice = listPrice - (item.discount || 0);

					return {
						...item,
						cta_link: generateCtaLink(item.cta_link, bundleProducts),
						ctaLink: generateCtaLink(item.ctaLink, bundleProducts),
						enableAffirm: item.enableAffirm && isUSWebsite,
						productOrBundle: { isBundle: true, bundleProducts, listPrice, currentPrice },
					};
				}

				// Single with product from prismic
				if (product && Object.keys(product).length) {
					const currentPrice = (Number(this.specialPrices[product.sku]) || product.price) - (item.discount || 0);
					return {
						...item,
						cta_link: generateCtaLink(item.cta_link, [product]),
						ctaLink: generateCtaLink(item.ctaLink, [product]),
						enableAffirm: item.enableAffirm && isUSWebsite,
						productOrBundle: {
							isBundle: false,
							product,
							sku: product.sku,
							listPrice: product.price,
							currentPrice,
						},
					};
				}

				const { sku } = item;

				// No sku in Products Grouped
				if (this.isProductsGrouped) {
					if (!sku) {
						// TODO: Remove 'oldData' when products grouped is fully updated in Prismic
						const oldData = {
							...item,
							hasLabel: true,
							price: item.labelPrice || 0,
							priceRegular: item.labelPriceStrikethrough || 0,
						};

						return { ...item, enableAffirm: false, oldData };
					}
				}

				// No sku "regular"
				if (!sku) return { ...item, enableAffirm: false };

				const skusArray = (sku || '').split(',').map((sku) => sku.trim());
				const isBundle = skusArray.length > 1;

				// Single
				if (!isBundle) {
					const product = products[sku];
					const listPrice = product ? product.priceRegular : undefined;
					const currentPrice = product ? product.price - (item.discount || 0) : undefined;

					return {
						...item,
						discount: item.discount || listPrice - currentPrice,
						enableAffirm: item.enableAffirm && isUSWebsite,
						productOrBundle: { isBundle, product, sku, listPrice, currentPrice },
					};
				}

				// Bundle
				const bundleProducts = skusArray.map((sku) => products[sku]).filter(Boolean);
				const listPrice = bundleProducts.reduce((acc, product) => acc + product.priceRegular, 0);
				const currentPrice = listPrice - (item.discount || 0);

				return {
					...item,
					enableAffirm: item.enableAffirm && isUSWebsite,
					productOrBundle: { isBundle, bundleProducts, sku, skusArray, listPrice, currentPrice },
				};
			});
		},

		hasAnyAvailableItems() {
			return true;
			return this.productsNormalized.some(
				(item) =>
					!item.productOrBundle ||
					(item.productOrBundle.isBundle
						? item.productOrBundle.bundleProducts.length > 0
						: item.productOrBundle.product)
			);
		},
	},

	render() {
		return this.$scopedSlots.default({
			productsNormalized: this.productsNormalized,
			hasAnyAvailableItems: this.hasAnyAvailableItems,
		});
	},
};
