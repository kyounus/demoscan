import MoleculeCarouselSlide from './molecule-carousel-slide.vue';

describe('@components/MoleculeCarouselSlide', () => {
	it('exports a valid component', () => {
		expect(MoleculeCarouselSlide).toBeAComponent();
	});
});
