import BaseButtonAuto from './base-button-auto.vue';

describe('@components/BaseButtonAuto', () => {
	it('exports a valid component', () => {
		expect(BaseButtonAuto).toBeAComponent();
	});
});
