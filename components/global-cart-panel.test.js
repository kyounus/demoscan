import GlobalCartPanel from './global-cart-panel.vue';

describe('@components/GlobalCartPanel', () => {
	it('exports a valid component', () => {
		expect(GlobalCartPanel).toBeAComponent();
	});
});
