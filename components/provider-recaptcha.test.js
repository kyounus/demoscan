import ProviderRecaptcha from './provider-recaptcha.vue';

describe('@components/ProviderRecaptcha', () => {
	it('exports a valid component', () => {
		expect(ProviderRecaptcha).toBeAComponent();
	});
});
