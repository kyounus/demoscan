import BlockAffirm from './block-affirm.vue';

describe('@components/BlockAffirm', () => {
	it('exports a valid component', () => {
		expect(BlockAffirm).toBeAComponent();
	});
});
