import BlockAddToCartV2 from './block-add-to-cart-v2.vue';

describe('@components/BlockAddToCartV2', () => {
	it('exports a valid component', () => {
		expect(BlockAddToCartV2).toBeAComponent();
	});
});
