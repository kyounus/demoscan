import BlockStatisticsV3Desktop from './block-statistics-v3-desktop.vue';

describe('@components/BlockStatisticsV3Desktop', () => {
	it('exports a valid component', () => {
		expect(BlockStatisticsV3Desktop).toBeAComponent();
	});
});
