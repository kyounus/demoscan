import BlockImagesGrid from './block-images-grid.vue';

describe('@components/BlockImagesGrid', () => {
	it('exports a valid component', () => {
		expect(BlockImagesGrid).toBeAComponent();
	});
});
