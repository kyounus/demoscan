import BlockTimeline from './block-timeline.vue';

describe('@components/BlockTimeline', () => {
	it('exports a valid component', () => {
		expect(BlockTimeline).toBeAComponent();
	});
});
