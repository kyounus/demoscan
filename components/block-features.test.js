import BlockFeatures from './block-features.vue';

describe('@components/BlockFeatures', () => {
	it('exports a valid component', () => {
		expect(BlockFeatures).toBeAComponent();
	});
});
