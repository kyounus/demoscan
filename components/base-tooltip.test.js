import BaseTooltip from './base-tooltip.vue';

describe('@components/BaseTooltip', () => {
	it('exports a valid component', () => {
		expect(BaseTooltip).toBeAComponent();
	});
});
