import BlockPromoBarVariations from './block-promo-bar-variations.vue';

describe('@components/BlockPromoBarVariations', () => {
	it('exports a valid component', () => {
		expect(BlockPromoBarVariations).toBeAComponent();
	});
});
