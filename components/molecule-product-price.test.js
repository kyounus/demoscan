import MoleculeProductPrice from './molecule-product-price.vue';

describe('@components/MoleculeProductPrice', () => {
	it('exports a valid component', () => {
		expect(MoleculeProductPrice).toBeAComponent();
	});
});
