import BlockSocial from './block-social.vue';

describe('@components/BlockSocial', () => {
	it('exports a valid component', () => {
		expect(BlockSocial).toBeAComponent();
	});
});
