import ProviderCustomColors from './provider-custom-colors.vue';

describe('@components/ProviderCustomColors', () => {
	it('exports a valid component', () => {
		expect(ProviderCustomColors).toBeAComponent();
	});
});
