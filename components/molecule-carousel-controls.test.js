import MoleculeCarouselControls from './molecule-carousel-controls.vue';

describe('@components/MoleculeCarouselControls', () => {
	it('exports a valid component', () => {
		expect(MoleculeCarouselControls).toBeAComponent();
	});
});
