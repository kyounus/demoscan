import GlobalCartPanelUpsellSameDevice from './global-cart-panel-upsell-same-device.vue';

describe('@components/GlobalCartPanelUpsellSameDevice', () => {
	it('exports a valid component', () => {
		expect(GlobalCartPanelUpsellSameDevice).toBeAComponent();
	});
});
