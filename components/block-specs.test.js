import BlockSpecs from './block-specs.vue';

describe('@components/BlockSpecs', () => {
	it('exports a valid component', () => {
		expect(BlockSpecs).toBeAComponent();
	});
});
