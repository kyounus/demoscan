import BlockComparisonTableV2 from './block-comparison-table-v2.vue';

describe('@components/BlockComparisonTableV2', () => {
	it('exports a valid component', () => {
		expect(BlockComparisonTableV2).toBeAComponent();
	});
});
