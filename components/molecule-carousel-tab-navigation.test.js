import MoleculeCarouselTabNavigation from './molecule-carousel-tab-navigation.vue';

describe('@components/MoleculeCarouselTabNavigation', () => {
	it('exports a valid component', () => {
		expect(MoleculeCarouselTabNavigation).toBeAComponent();
	});
});
