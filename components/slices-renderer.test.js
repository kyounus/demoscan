import SlicesRenderer from './slices-renderer';

describe('@components/SlicesRenderer', () => {
	it('exports a valid component', () => {
		expect(SlicesRenderer).toBeAComponent();
	});
});
