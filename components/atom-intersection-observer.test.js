import AtomIntersectionObserver from './atom-intersection-observer.js';

describe('@components/AtomIntersectionObserver', () => {
	it('exports a valid component', () => {
		expect(AtomIntersectionObserver).toBeAComponent();
	});
});
