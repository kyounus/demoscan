import AtomTransitionExpand from './atom-transition-expand.vue';

describe('@components/AtomTransitionExpand', () => {
	it('exports a valid component', () => {
		expect(AtomTransitionExpand).toBeAComponent();
	});
});
