import GlobalHeader from './global-header.vue';

describe('@components/GlobalHeader', () => {
	it('exports a valid component', () => {
		expect(GlobalHeader).toBeAComponent();
	});
});
