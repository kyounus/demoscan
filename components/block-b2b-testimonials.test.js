import BlockB2BTestimonials from './block-b2b-testimonials.vue';

describe('@components/BlockB2BTestimonials', () => {
	it('exports a valid component', () => {
		expect(BlockB2BTestimonials).toBeAComponent();
	});
});
