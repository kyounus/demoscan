import BlockTimelineStep from './block-timeline-step.vue';

describe('@components/BlockTimelineStep', () => {
	it('exports a valid component', () => {
		expect(BlockTimelineStep).toBeAComponent();
	});
});
