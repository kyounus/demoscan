import BlockHeaderBanner from './block-header-banner.vue';

describe('@components/BlockHeaderBanner', () => {
	it('exports a valid component', () => {
		expect(BlockHeaderBanner).toBeAComponent();
	});
});
