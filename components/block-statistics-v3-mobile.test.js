import BlockStatisticsV3Mobile from './block-statistics-v3-mobile.vue';

describe('@components/BlockStatisticsV3Mobile', () => {
	it('exports a valid component', () => {
		expect(BlockStatisticsV3Mobile).toBeAComponent();
	});
});
