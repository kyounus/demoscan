import BlockReviews from './block-reviews.vue';

describe('@components/BlockReviews', () => {
	it('exports a valid component', () => {
		expect(BlockReviews).toBeAComponent();
	});
});
