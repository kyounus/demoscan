import BlockComparisonTable from './block-comparison-table.vue';

describe('@components/BlockComparisonTable', () => {
	it('exports a valid component', () => {
		expect(BlockComparisonTable).toBeAComponent();
	});
});
