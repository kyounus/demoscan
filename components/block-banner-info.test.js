import BlockBannerInfo from './block-banner-info.vue';

describe('@components/BlockBannerInfo', () => {
	it('exports a valid component', () => {
		expect(BlockBannerInfo).toBeAComponent();
	});
});
