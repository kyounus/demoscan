import BlockMultiColumnText from './block-multi-column-text.vue';

describe('@components/BlockMultiColumnText', () => {
	it('exports a valid component', () => {
		expect(BlockMultiColumnText).toBeAComponent();
	});
});
