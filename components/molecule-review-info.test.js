import MoleculeReviewInfo from './molecule-review-info.vue';

describe('@components/MoleculeReviewInfo', () => {
	it('exports a valid component', () => {
		expect(MoleculeReviewInfo).toBeAComponent();
	});
});
