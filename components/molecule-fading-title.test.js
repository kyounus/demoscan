import MoleculeFadingTitle from './molecule-fading-title.vue';

describe('@components/MoleculeFadingTitle', () => {
	it('exports a valid component', () => {
		expect(MoleculeFadingTitle).toBeAComponent();
	});
});
