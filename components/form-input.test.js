import FormInput from './form-input.vue';

describe('@components/FormInput', () => {
	it('exports a valid component', () => {
		expect(FormInput).toBeAComponent();
	});
});
