import BlockProductsGrouped from './block-products-grouped.vue';

describe('@components/BlockProductsGrouped', () => {
	it('exports a valid component', () => {
		expect(BlockProductsGrouped).toBeAComponent();
	});
});
