import GlobalModal from './global-modal.vue';

describe('@components/GlobalModal', () => {
	it('exports a valid component', () => {
		expect(GlobalModal).toBeAComponent();
	});
});
