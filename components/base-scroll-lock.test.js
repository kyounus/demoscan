import BaseScrollLock from './base-scroll-lock.vue';

describe('@components/BaseScrollLock', () => {
	it('exports a valid component', () => {
		expect(BaseScrollLock).toBeAComponent();
	});
});
