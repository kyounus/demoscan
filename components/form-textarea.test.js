import FormTextarea from './form-textarea.vue';

describe('@components/FormTextarea', () => {
	it('exports a valid component', () => {
		expect(FormTextarea).toBeAComponent();
	});
});
