import BlockProductBanner from './block-product-banner.vue';

describe('@components/BlockProductBanner', () => {
	it('exports a valid component', () => {
		expect(BlockProductBanner).toBeAComponent();
	});
});
