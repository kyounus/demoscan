import BlockFaq from './block-faq.vue';

describe('@components/BlockFaq', () => {
	it('exports a valid component', () => {
		expect(BlockFaq).toBeAComponent();
	});
});
