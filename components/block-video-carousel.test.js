import BlockVideoCarousel from './block-video-carousel.vue';

describe('@components/BlockVideoCarousel', () => {
	it('exports a valid component', () => {
		expect(BlockVideoCarousel).toBeAComponent();
	});
});
