import GlobalCartPanelQtyInput from './global-cart-panel-qty-input.vue';

describe('@components/GlobalCartPanelQtyInput', () => {
	it('exports a valid component', () => {
		expect(GlobalCartPanelQtyInput).toBeAComponent();
	});
});
