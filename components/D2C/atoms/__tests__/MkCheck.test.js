import MkCheck from '~/components/D2C/atoms/MkCheck.vue';

describe('@components/MkCheck', () => {
	it('exports a valid component', () => {
		expect(MkCheck).toBeAComponent();
	});
});
