import MkCheckoutEmail from '~/components/D2C/molecules/MkCheckoutEmail.vue';

describe('@components/MkCheckoutEmail', () => {
	it('exports a valid component', () => {
		expect(MkCheckoutEmail).toBeAComponent();
	});
});
