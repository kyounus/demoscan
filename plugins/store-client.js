export default function initializeStoreScreen(ctx) {
	const { store } = ctx;
	store.dispatch('magento/INITIALIZE_CLIENT', ctx);
	store.dispatch('screen/SETUP_BREAKPOINTS');
}
