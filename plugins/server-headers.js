import fontPlantin400 from '@assets/fonts/plantin/30BBC3_0_0.woff2';
import fontAkkurat400 from '@assets/fonts/akkurat/lineto-akkurat-pro-regular.woff2';
import fontAkkurat300 from '@assets/fonts/akkurat/lineto-akkurat-pro-light.woff2';

const fonts = [fontPlantin400, fontAkkurat400, fontAkkurat300];

const linkHeaders = ({ tealiumAsyncUrl, gtmUrl, isUSWebsite }) =>
	[
		'<https://hello.myfonts.net/>; rel=preconnect',
		'<https://tags.tiqcdn.com/>; rel=preconnect',
		isUSWebsite && '<https://cdn1.affirm.com/>; rel=preconnect',
		...fonts.map((font) => `<${font}>; rel=preload; as=font; crossorigin`),
		tealiumAsyncUrl && `<${tealiumAsyncUrl}>; rel=preload; as=script`,
		gtmUrl && `<${gtmUrl}>; rel=preload; as=script`,
	].filter(Boolean);

export default function serverHeaders({ res, $config, store }) {
	if (!process.server || !res || res.headersSent) return;
	res.setHeader(
		'Link',
		linkHeaders({
			gtmUrl: $config.gtmUrl,
			tealiumAsyncUrl: $config.tealium.asyncUrl,
			isUSWebsite: store && store.state && store.state.isUSWebsite,
		})
	);
}
