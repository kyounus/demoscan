
/**
 *
 * @param {object} data
 * @returns cleaned object that removes undefined or null key:value pairs
 */
 const CLEAN_OBJECT = (data) => {
  return data
  // Object.entries(data)
  //   .filter(([_, val]) => val != null)
  //   .reduce(
  //     (acc, [key, val]) => ({
  //       ...acc,
  //       [key]: val === Object(val) ? CLEAN_OBJECT(val) : val
  //     }), {}
  //   );
}

export default ({ app }, inject) => {
  inject('T_VIEW_TRACK', async (event_type = null, event_data = {}) => {  if (typeof window.utag !== 'undefined') {

    try {
      await utag.view({
        "tealium_event": event_type,
        ...event_data
      })

    } catch (e) {
      console.log(e)
      //TODO Decide error reporting solution
    }

  }}),
  inject('T_LINK_TRACK', async (event_type = null, event_data = {}) => {  if (typeof window.utag !== 'undefined') {

    try {
      await utag.link({
        "tealium_event": event_type,
        ...event_data
      })
    } catch (e) {
      console.log(e)

    }

  }})
  const setTealiumPayload = async (key, payload, trackType='T_LINK_TRACK') => {
    let $defaultPayload = {
        page_url: window.location.href,
        event_platform: "web",
        session_id: 'Test'
    }
    $defaultPayload.tealium_event = key
    if(key == 'product_view' || key == 'cart_view' || key == 'account_portal_view'){
      delete $defaultPayload.event_platform
    }
    let $tealiumPayload = {...$defaultPayload, ...payload}
    if(trackType == 'T_LINK_TRACK') {
      app.$T_LINK_TRACK(key, $tealiumPayload)
    } 
    if(trackType == 'T_VIEW_TRACK') {
      app.$T_VIEW_TRACK(key, $tealiumPayload)
    }
  }

  //Export as telium
  const telium = {
    setTealiumPayload
  }
  inject('telium', telium)
}





// const T_VIEW_TRACK = async (event_type = null, data = {}) => {
//   if (typeof window.utag !== 'undefined') {
//     //let customer_email;

//     try {
//       const cleaned_object = await CLEAN_OBJECT(DATA);

//       await utag.view({
//         'tealium_event': event_type,
//         ...cleaned_object
//       })
//     } catch (e) {
//       //TODO Decide error reporting solution
//     }

//   }
// }


// const T_LINK_TRACK = () => {
//   if (typeof window.utag !== 'undefined') {
//     //let customer_email;
//     try {
//       utag.view({
//         "tealium_event": "product_view",
//         "page_type": "product_quick_view",
//         "page_name": "Quick View: Shirts: Lucky Shirt",
//         "product_id": ["12345"],
//         "product_name": ["Lucky Shirt"]
//       });
//     } catch (e) {
//       //TODO Decide error reporting solution
//     }
//   }
// }


// module.exports = {
//   T_VIEW_TRACK,
//   T_LINK_TRACK
// }
