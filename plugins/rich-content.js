import Vue from 'vue';
import smartLinkButton from '~/mixins/smart-link-button';

let router = null;

function internalClickHandler(vm, url, e) {
	e.preventDefault();
	if (url.startsWith('/add-to-cart?')) {
		const addToCartSourceElem = e.target && e.target.closest('div[data-rich-content-atc-source]');
		// We're hacking a bit to use logic from smart-link-button mixin
		// and access openPromoModal/openCart on the layout component
		const addToCartSkus = smartLinkButton.computed.addToCartSkus.call({
			$router: vm.$router,
			$store: vm.$store,
			isAddToCart: true,
			smart: url,
		});
		const promoCode = smartLinkButton.computed.promoCode.call({
			$router: vm.$router,
			smart: url,
		});

		const layout = vm.$nuxt.$children.find((child) => child.openCart);
		smartLinkButton.methods.addToCartAction.call({
			addToCartSkus,
			promoCode,
			addToCartSource: addToCartSourceElem && addToCartSourceElem.dataset.richContentAtcSource,
			isAddingToCart: false,
			openPromoModal: layout.openPromoModal,
			openCart: layout.openCart,
			$store: vm.$store,
		});
		return;
	}
	router.push(url);
}

function analyzeLinksInElement(el, vm) {
	const links = Array.from(el.querySelectorAll('a:not([data-molekule-link-analyzed])'));
	links.forEach((link) => {
		link.dataset.molekuleLinkAnalyzed = true;
		let url = link.getAttribute('href');

		if (url.startsWith('https://molekule.com/')) {
			url = url.substr(20);
		}

		if (url.startsWith('/')) {
			link.addEventListener('click', internalClickHandler.bind(this, vm, url), false);

			if (url.startsWith('/add-to-cart?')) {
				link.role = 'button';
				link.href = '#';
			}
		}

		if (url.endsWith('.pdf')) {
			link.target = '_blank';
		}
	});
}

Vue.directive('rich-content', {
	bind(el, binding, vnode) {
		// console.log('bind', { el, binding, vnode });
		analyzeLinksInElement(el, vnode.context);
	},

	update(el, binding, vnode) {
		// console.log('update', { el, binding, vnode });
		analyzeLinksInElement(el, vnode.context);
	},
});

export default function initializeRichContent({ app }) {
	router = app.router;
}
