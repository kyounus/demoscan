const cacheControl = `public, max-age=${60 * 60 * 24 * 31}, stale-while-revalidate=${60 * 60}`;

export default function blogRedirect({ route: { path, fullPath }, res, redirect }) {
	if (!process.server) return;
	let inBlogPath = '';

	if (path === '/blog' || path.startsWith('/blog/')) {
		inBlogPath = fullPath.substring(5);
	} else if (path === '/ca-en/blog' || path.startsWith('/ca-en/blog/')) {
		inBlogPath = fullPath.substring(11);
	}

	if (inBlogPath) {
		res.setHeader('cache-control', cacheControl);
		redirect(301, `https://molekule.science/${inBlogPath}`);
	}
}
