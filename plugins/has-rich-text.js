import Vue from 'vue';

Vue.prototype.$hasRichText = function $hasRichText(val) {
	return !(!val || val.length === 0 || val === '<p></p>');
};
