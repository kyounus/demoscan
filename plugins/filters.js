import Vue from 'vue';

/**
 * Format number to have commas
 */
Vue.filter('numberWithCommas', (number) => {
	return (number || 0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
});

/**
 * Truncate text
 */
Vue.filter('truncate', function (text, length, clamp) {
	text = text || '';
	clamp = clamp || '...';
	length = length || 30;

	if (text.length <= length) return text;

	let tcText = text.slice(0, length - clamp.length);
	let last = tcText.length - 1;

	while (last > 0 && tcText[last] !== ' ' && tcText[last] !== clamp[0]) last -= 1;

	// Fix for case when text dont have any `space`
	last = last || length - clamp.length;

	tcText = tcText.slice(0, last);

	return tcText + clamp;
});

/**
 * Format date
 */
Vue.filter('formatDate', (date) => {
	date = new Date(date);

	const day = date.getDate();
	const month = date.getMonth() + 1;
	const displayDay = day < 10 ? `0${day}` : day;
	const displayYear = date.getFullYear();
	const displayMonth = month < 10 ? `0${month}` : month;

	return `${displayMonth}/${displayDay}/${displayYear}`;
});

/**
 * File Size Filter
 * @param{number, string}
 * @example: 33003
 * @usage : {{33003 | prettyFileSize}}
 * @output : 32 kb
 */
Vue.filter('prettyFileSize', function (value) {
	if (!value) return '';
	const units = ['bytes', 'kb', 'mb', 'gb', 'tb'];
	let k = 0;
	let v = parseInt(value, 10) || 0;
	while (v >= 1024 && ++k) {
		v = v / 1024;
	}
	return v.toFixed(v < 10 && k > 0 ? 1 : 0) + ' ' + units[k];
});

// FOR CONVERTING UTC to Date
// EX. {{2021-05-04T11:16:49.868Z | ustToDate}}
Vue.filter('formatDateAurora', function (value, format) {
	if (!value) return '';
	let $dateFormat = [];
	let $dateSplitter = '/';
	const date = new Date(value);
	if (format) {
		if (format.includes('/')) {
			$dateSplitter = '/';
		} else if (format.includes('-')) {
			$dateSplitter = '-';
		}
		const $split = format.split($dateSplitter);
		$dateFormat = [];
		$split.forEach(($obj) => {
			if ($obj === 'MM') {
				const monthFormat = date.getMonth() + 1;
				$dateFormat.push(monthFormat < 10 ? '0' + monthFormat : monthFormat);
			}
			if ($obj === 'DD' || $obj === 'dd') {
				const dateForm = ('0' + date.getDate()).slice(-2);
				$dateFormat.push(dateForm);
			}
			if ($obj === 'YYYY' || $obj === 'yyyy') {
				$dateFormat.push(date.getFullYear());
			}
			if ($obj === 'YY') {
				const $year = date.getFullYear();
				$dateFormat.push($year.toString().substr(-2));
			}
		});
	}
	let formatedDate = '';
	if ($dateFormat.length > 0) {
		formatedDate = $dateFormat[0] + $dateSplitter + $dateFormat[1] + $dateSplitter + $dateFormat[2];
	} else {
		formatedDate = date
			.toLocaleString('en', {
				month: '2-digit',
				day: '2-digit',
				year: 'numeric',
			})
			.replace(/(\d+)\/(\d+)\/(\d+)/, '$1' + $dateSplitter + '$2' + $dateSplitter + '$3');
	}
	return formatedDate;
});

// FOR CONVERTING CENTS TO DOLLARS
// EX. {{49900 | centsToDollars}}

/**
 * Converting Cents to dollars
 * @param{number, string}
 * @example: 49900
 * @usage : {{49900 | centsToDollars}}
 * @output : 499.00
 */
Vue.filter('centsToDollars', function (value) {
	if (!value) return 0;
	value = (value + '').replace(/[^\d.-]/g, '');
	value = parseFloat(value);
	const final = value ? value / 100 : 0;

	return final.toFixed(2);
});

// FOR CONVERTING UTC to Date
// EX. {{2021-05-04T11:16:49.868Z | ustToDate}}

/**
 * Converting Cents to dollars
 * @param{string}
 * @example: 2021-05-04T11:16:49.868Z
 * @usage : {{2021-05-04T11:16:49.868Z | ustToDate}}
 * @output : "04/05/2021"
 */
Vue.filter('ustToDate', function (value) {
	if (!value || !value.includes('T')) return value;
	// const date = value.split('T')[0].split('-').reverse().join('/');
	const date = value.split('T')[0].split('-');
	const [year, month, day] = date;
	const formattedDate = [month, day, year].join('/'); // formate
	return formattedDate;
});

/**
 * Converting 0 to -"
 * @param{string}
 * @example: 0
 * @usage : {{0 | zeroDollarDisplay}}
 * @output : "-"
 */
Vue.filter('zeroDollarDisplay', function (value) {
	if (!value || value === 0.0) return '-';
	return '$' + value;
});
/**
 * Converting 0 to 000.00"
 * @param{string}
 * @example: 0
 * @usage : {{0 | zeroDisplay}}
 * @output : "000.00"
 */
Vue.filter('zeroDisplay', function (value) {
	if (!value || value === 0.0) return '000.00';
	return value;
});
Vue.filter('formatPrice', function (value) {
	return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
});

// phone mask filter
Vue.filter('phone', function (value) {
	if (!value) return '';
	const phone = value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
	return !phone[2] ? phone[1] : '(' + phone[1] + ') ' + phone[2] + (phone[3] ? '-' + phone[3] : '');
});

Vue.filter('monthFormatter', function (value) {
	const array = value.split('/');
	const month = ('0' + array[0]).slice(-2);
	return [month, array[1]].join('/');
});
