import {
  extend,
  setInteractionMode
} from "vee-validate";
import {
  required,
  numeric,
  alpha,
  alpha_spaces,
  oneOf,
  min,
  max,
  regex,
  confirmed
} from "vee-validate/dist/rules";

setInteractionMode('lazy');

extend("required", {
  ...required,
  message: 'Required'
});

extend("required_for_application", {
  ...required,
  message: 'Required to process your application'
});

extend("alpha", {
  ...alpha,
  message: "Please enter only alphabetic characters"
});

extend("alpha_spaces", {
  ...alpha_spaces,
  message: "Please enter only alphabets and space"
});

const phoneRule = {
  message: field => 'Please enter a valid phone number',
  validate: value => {
    const unmaskedValue = value.replace(/[^0-9]/g, '');
    return unmaskedValue.length == 10;
  }
};
const emailRule = {
  message: field => 'Please enter a valid email address',
  validate: value => {
    const regex = /^\w+([\+.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return regex.test(value);
  }
};
extend("email",emailRule);

extend("phone", phoneRule);

extend('oneOf', oneOf);

extend('min', min);

extend('numeric', numeric)

extend('minmax', {
  validate(value, { min, max }) {
    return value.length >= min && value.length <= max;
  },
  params: ['min', 'max'],
  message: 'Please enter minimum {min} and maximum {max} characters'
});

extend("postal_code", {
  message: "Please enter a valid postal code",
  validate: value => {
      var strongRegex = new RegExp("^[0-9-]*$");
      return strongRegex.test(value);
  }
});

extend("max", {
  ...max,
  message: "Exceeds character count limit."
});

extend('regex', regex);

// Password Confirmation
extend('confirmed', {
  ...confirmed,
  message: 'Passwords don\'t match.'
})

//Password Combination
extend('valid_password', {
  message: 'Password must be a minimum of 8 characters, 1 uppercase, 1 lowercase and 1 special character.',
  validate: value => {
      var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#\$%\^&\*])(?=.{8,})");
      return strongRegex.test(value);
  }
});
// Date Format mm/dd/yyyy
extend('date', {
  message: 'Please enter a valid date',
  validate: value => {
    let returnValue = false
    let d1 = new Date('01/27/2020');
    let d2 = new Date(value)
    let d3 = new Date(new Date().setDate(new Date().getDate()+1))
    let len = value.split('/').length-1
    if(len === 2){
      const month = value.split('/')[0]
      const day = value.split('/')[1]
      const year = value.split('/')[2]
      if(month > 12 || month <= 0){
        returnValue = false
      }else{
        if(day > 31 || day <= 0){
          returnValue = false
        }else{
          if(year > new Date().getFullYear()){
            returnValue = false
          }else{
            if(d2 <= d1 || d2 >= d3){
              returnValue = false
            }else{
              returnValue = true
            }
          }
        }
      }
    }else{
      returnValue = false
    }
    return returnValue
  }
});
