import createRepository from '~/repositories/Repository';

export default (ctx, inject) => {
	let apiCountry = ctx.store.getters.apiCountry;
	if (!apiCountry) {
		apiCountry = 'US';
		console.log('setting apiCountry as default:US');
	}
	ctx.$axios.interceptors.request.use((config) => {
		config.params = config.params || {};
		if (!config.params.country && !config.url.includes('country=')) {
			config.params.country = apiCountry;
		}
		config.headers.country = apiCountry;
		return config;
	});
	inject('repositories', createRepository(ctx.$axios, apiCountry));
};
