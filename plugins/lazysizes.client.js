import lazysizes from 'lazysizes';
const { connection: conn } = navigator;
const hasBadConnection = conn && ((conn.effectiveType || '').includes('2g') || conn.saveData);
lazysizes.cfg.preloadAfterLoad = !hasBadConnection;
lazysizes.init();

document.addEventListener('lazybeforeunveil', (e) => {
	const bg = e.target.getAttribute('data-bg');
	if (bg) {
		e.target.style.backgroundImage = 'url(' + bg + ')';
	}
});
