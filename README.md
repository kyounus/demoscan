# Molekule

> PWA Build

## Build Setup

To build and start the application, an .env file is needed. Since the .env file contains sensitive information, the file is not pushed to GitHub. Please contact a developer who currently works on this project to get the .env file and place it at the root directory of the project.

```bash
# install dependencies
$ yarn

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

## Documentation

This project includes a docs folder with more details on:

1. [Setup and development](docs/development.md)
2. [Project Structure](docs/project-structure.md)
3. [Languages and technologies](docs/tech.md)
4. [Routing, layouts, views, and localization](docs/routing.md)
5. [State management](docs/state.md)
6. [Prismic](docs/prismic.md)
7. [Custom APIs](docs/custom-apis.md)
8. [Tests](docs/tests.md)
9. [Linting and formatting](docs/linting.md)
10. [Editor integration](docs/editors.md)
11. [Deployment and server infrastructure](docs/deployment.md)
12. [Storybook](docs/storybook.md)
13. [Troubleshooting](docs/troubleshooting.md)
