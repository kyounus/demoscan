#!/bin/bash
# This scripts finds CloudWatch Log Groups without retention and sets it to 30 days
# especially useful for Lambda@Edge logs
# It's good to run it from time to time to make sure that no logs are stored indefinitely
# https://renaghan.com/posts/lambda-at-edge-cloudfront-debugging/

set -eo pipefail


AWS="aws --no-paginate --output json"
JQ="jq --raw-output"

for REGION in `$AWS --region us-east-1 ec2 describe-regions | $JQ '.Regions[].RegionName'`
do
  echo "Region $REGION"
  for GROUP in `$AWS --region "$REGION" logs describe-log-groups | $JQ '.logGroups[] | select (has("retentionInDays") | not).logGroupName'`
  do
    echo "  $REGION $GROUP"
    $AWS --region "$REGION" logs put-retention-policy --log-group-name "$GROUP" --retention-in-days 30
  done
done
