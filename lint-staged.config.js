module.exports = {
	'*.js': ['yarn :eslint', 'yarn :prettier'],
	'*.vue': ['yarn :eslint', 'yarn :stylelint', 'yarn :prettier'],
	'*.json': ['yarn :prettier'],
	'*.md': ['yarn :markdownlint', 'yarn :prettier'],
	'*.{css,scss}': ['yarn :stylelint', 'yarn :prettier'],
	'*.{png,jpeg,jpg,gif,svg}': ['imagemin-lint-staged'],
};
