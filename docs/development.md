# Setup and development

- [First-time setup](#first-time-setup)
- [Installation](#installation)
- [Dev server](#dev-server)
- [Generators](#generators)
- [Aliases](#aliases)
- [Globals](#globals)
  - [Base and Shared components](#base-and-shared-components)
- [Components](#components)
  - [Naming Conventions](#naming-conventions)
    - [Base Components](#base-components)
    - [Global Components](#global-components)
    - [Atom Components](#atom-components)
    - [Molecule Components](#molecule-components)
    - [Block Components](#block-components)
    - [Form Components](#form-components)
    - [Provider Components](#provider-components)

## First-time setup

Make sure you have the following installed:

- [Node](https://nodejs.org/en/) (at least the latest LTS)
- [Yarn](https://yarnpkg.com/lang/en/docs/install/) (at least 1.0)

## Installation

```bash
# Install dependencies from package.json
yarn
```

## Dev server

> Note: If you're on Linux and see an `ENOSPC` error when running the commands below, you must [increase the number of available file watchers](https://stackoverflow.com/questions/22475849/node-js-error-enospc#answer-32600959).

```bash
# Launch the dev server
yarn dev
```

## Generators

This project includes generators to speed up common development tasks. Commands include:

```bash
# Generate a new component with adjacent unit test
yarn new component

# Generate a new page component with adjacent unit test
yarn new page

# Generate a new layout component with adjacent unit test
yarn new layout

# Generate a new Vuex module with adjacent unit test
yarn new module

# Generate a new end-to-end spec
yarn new e2e
```

Update existing or create new generators in the `.templates` folder, with help from the [Hygen docs](http://www.hygen.io/).

## Aliases

To simplify referencing local modules and refactoring, you can set aliases to be shared between dev and unit tests in `aliases.config.js`. As a convention, this project uses an `@` prefix to denote aliases.

## Globals

### Base and Shared components

[**Base components**](https://vuejs.org/v2/style-guide/#Base-component-names-strongly-recommended) (a.k.a. presentational, dumb, or pure components) that apply app-specific styling and conventions should all begin with the `base-` prefix. Since these components are typically used in place of raw HTML element (and thus used as frequently), they're automatically globally registered for convenience. This means you don't have to import and locally register them to use them in templates.

Available Base Components:

- [**BaseButton**](/components/base-button.vue)
- [**BaseButtonArrow**](/components/base-button-arrow.vue) - Button with arrow
- [**BaseButtonAuto**](/components/base-button-auto.vue) - Button that automatically chooses which button to use based on `appearance` prop.
- [**BaseIcon**](/components/base-icon.vue) - Displays one of the SVG icons based on the `name` prop.
- [**BaseImage**](/components/base-image.vue) - Image component
  - Supports normal urls and Prismic Images
  - Supports different images on different breakpoint
  - Supports responsive images (width based) for Prismic images
  - Preserves space before image is loaded based on aspect ratio of the image or predefined one
  - Can `fit` parent (like background image)
  - Automatically adjust image to download (when fitting parent or fixed aspect ratio is defined)
  - Automatically lazy loads and/or preloads all images
  - Supports critical images
- [**BaseScrollLock**](/components/base-scroll-lock.vue) - Locks the scroll when rendered
- [**BaseSpinner**](/components/base-spinner.vue)
- [**BaseTooltip**](/components/base-tooltip.vue)

**Global/Shared Components** are components that appear on most pages and are generally imported in the [layouts components](/layouts).

Available Global/Shared Components:

- [**GlobalHeader**](/components/global-header.vue), [**GlobalHeaderPromoBar**](/components/global-header-promo-bar.vue)
- [**GlobalFooter**](/components/global-footer.vue)
- [**GlobalCartPanel**](/components/global-cart-panel.vue)
- [**GlobalChatWidgetZendesk**](/components/global-chat-widget-zendesk.vue)
- [**GlobalCarousel**](/components/global-carousel.vue), [**GlobalCarouselSlide**](/components/global-carousel-slide.vue)
- [**GlobalModal**](/components/global-modal.vue)
- [**GlobalPromoModal**](/components/global-promo-modal.vue)

## Components

### Naming Conventions

We name components using snake case and we organize components into 7 separate categories:

- [Base](#base-components)
- [Global](#global-components)
- [Atom](#atom-components)
- [Molecule](#molecule-components)
- [Block](#block-components)
- [Form](#form-components)
- [Provider](#provider-components)

You can identify to which category a component belongs to by looking at the component's prefix. For example, all block components start with `block-` and all form components start with `form-`.

#### Base Components

Read about [Base Components](#base-and-shared-components).

#### Global Components

Read about [Global Components](#base-and-shared-components).

#### Atom Components

Inspired by [Atomic Design](https://bradfrost.com/blog/post/atomic-web-design/). Atoms are our HTML tags and small UI elements/abstractions that aren't very useful on their own, such as a button, a heading element, a Vue transition.

#### Molecule Components

Inspired by [Atomic Design](https://bradfrost.com/blog/post/atomic-web-design/). Molecules are more useful components that are often made up of atoms.

For example, a form label, input or button would be considered atoms and aren’t too useful by themselves, but combine them together as a form and now they can actually do something together and that would be a molecule component.

#### Block Components

Continuing with the [Atomic Design](https://bradfrost.com/blog/post/atomic-web-design/) system, block components could be considered organism components. These are distinct, standalone UI sections that are the building blocks of pages.

Additionally, all block components must map to a specific slice in Prismic.

#### Form Components

Form components are self-explanatory. They're components that we can use to build a form.

#### Provider Components

Provider components are alternates to mixins. These should be [renderless components](https://markus.oberlehner.net/blog/reusing-logic-with-renderless-vue-frame-components/) (if possible) that provide extra functionality to other components. For example, we have a component called [`provider-element-activity-tracker.js`](components/provider-element-activity-tracker.js) that doesn't render anything, it just passes variables through slot props with information about whether the default slot has a focused, hovered, or generally active state.

This allows us to add extra functionality to components in a more declarative way using composition instead of using an object oriented programming pattern and it also helps us avoid [issues with mixins](https://reactjs.org/blog/2016/07/13/mixins-considered-harmful.html).
