# Project Structure

- [`.circleci`](#circleci)
- [`.ebextensions`](#ebextensions)
- [`.elasticbeanstalk`](#elasticbeanstalk)
- [`.storybook`](#storybook)
- [`.templates`](#templates)
- [`.vscode`](#vscode)
- [`app`](#app)
- [`assets`](#assets)
- [`bin`](#bin)
- [`components`](#components)
- [`custom-apis`](#custom-apis)
- [`design`](#design)
- [`middleware`](#middleware)
- [`mixins`](#mixins)
- [`modules`](#modules)
- [`pages`](#pages)
- [`plugins`](#plugins)
- [`store`](#store)
- [`tests`](#tests)

## `.circleci`

Where we keep all the configuration files for CircleCI. See [](deployment.md#deployment) for more.

## `.ebextensions`

Extensions for Elastic Beanstalk for any custom behavior. This includes the nginx rewrite logic.

## `.elasticbeanstalk`

Where we keep the config file for Elastic Beanstalk.

## `.storybook`

Where we keep all the configuration files for Storybook.

## `.templates`

Generator templates to speed up development. See [the development doc](development.md#generators) for more.

## `.vscode`

Settings and extensions specific to this project, for Visual Studio Code. See [the editors doc](editors.md#visual-studio-code) for more.

## `app`

Where we keep some files that Nuxt automatically uses to change certain behaviors. Currently, this is being used to customize the error.html page and the router's scrollBehavior.

## `assets`

Nuxt manages assets using vue-loader. Learn more about [its asset handling here](https://nuxtjs.org/guide/assets).

## `bin`

Where we keep scripts to do automate regular maintenance tasks.

## `components`

Where most of the components in our app will live, including our [global base components](development.md#base-components).

## `custom-apis`

Where we keep all the code related to the Koa app that we add as serverMiddleware in nuxt.config.js. This Koa app has several endpoints that are helpful with optimizations, logic abstraction, and reducing the amount of processing on the frontend. See [the custom-apis doc](custom-apis.md) for more.

## `design`

Where we keep our [design variables and tooling](tech.md#design-variables-and-tooling).

## `middleware`

Nuxt uses middleware to enhance/customize application, see [its documentation](https://nuxtjs.org/examples/middleware) to know more.

## `mixins`

Vue uses mixins to provide a flexible way to distribute reusable functionalities, see [its documentation](https://vuejs.org/v2/guide/mixins.html) to know more.

## `modules`

Modules are Nuxt.js extensions which can extend the framework's core functionality and add endless integrations, see [its documentation](https://nuxtjs.org/docs/2.x/directory-structure/modules/) to know more.

## `pages`

Where the routing-related components live. Nuxt uses directory based routing, see [the routing doc](https://nuxtjs.org/guide/routing) for more.

## `plugins`

Where we keep local Vue plugins to use third party packages.

## `store`

Where all our global state management lives. See [the state management doc](state.md) for more.

## `tests`

Where all our tests go. See [the tests doc](tests.md) for more.
