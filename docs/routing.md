# Routing, layouts, and views

- [Overview](#overview)
- [Layouts](#layouts)
- [Views](#views)
- [Localization](#localization)

## Overview

This project uses directory based routing. Using the path information, Nuxt generates vue-router configuration. There are also two folders, both containing route-specific components: `layouts` and `pages`.

## Layouts

Every view component must use a layout component as its base and specify it in `layout` option, as this convention helps us mock out layout components when testing views. Layouts usually aren't very complex, often containing only shared HTML like headers, footers, and navigation to surround the main content in the view.

## Pages

Each view component will be used registered to one route, to provide a template for the page. They can technically include some additional properties from Vue Router [to control navigation](https://router.vuejs.org/guide/advanced/navigation-guards.html), for example to [fetch data](https://router.vuejs.org/guide/advanced/data-fetching.html#fetching-before-navigation). Read [nuxt routing documentation](https://nuxtjs.org/guide/routing) to know more.

## Localization

We handle localization using subdirectories. For example, the pages for Canada are under the /ca-en/ subdirectory. We create those routes using the router.extendRoutes setting in `nuxt.config.js`. We duplicate all the static routes and add the locale's prefix.

```js
router: {
  extendRoutes(routes, resolve) {
    const allRoute = routes.find(({ path }) => path === '/*');
    const routesWithoutAll = routes.filter(({ path }) => path !== '/*');
    routesWithoutAll.push({
      name: 'holiday',
      path: '/holiday',
      component: resolve(__dirname, 'pages/index'),
    });
    const canadaRoutes = routesWithoutAll.map((route) => ({
      ...route,
      name: `${route.name}___en-ca`,
      path: `/ca-en${route.path}`,
    }));
    return [...routesWithoutAll, ...canadaRoutes, allRoute];
  },
  ...
}
```
