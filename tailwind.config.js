module.exports = {
  corePlugins: {
    preflight: false,
  },
  purge: {
    enabled: true,
    content: [
    './components/D2C/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/D2C/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}'
    ]
},
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      inset: {
        '64': '64px',
        '18': '75px',
        '-26': '-26px',
      },
      fontSize: {
        //DEFAULT-STUB START >>
        xs: ['12px', { lineHeight: '16px' }],
        sm: ['14px', { lineHeight: '20px' }],
        base: ['16px', { lineHeight: '24px' }],
        lg: ['18px', { lineHeight: '28px' }],
        xl: ['20px', { lineHeight: '28px' }],
        '2xl': ['24px', { lineHeight: '32px' }],
        '3xl': ['30px', { lineHeight: '36px' }],
        '4xl': ['36px', { lineHeight: '40px' }],
        '5xl': ['48px', { lineHeight: '1' }],
        '6xl': ['60px', { lineHeight: '1' }],
        '7xl': ['72px', { lineHeight: '1' }],
        '8xl': ['96px', { lineHeight: '1' }],
        '9xl': ['128px', { lineHeight: '1' }],
        //DEFAULT-STUB END <<
        'base2': '32px',
        '22': '22px',
        '28': '28px',
        'tablet-title': '28px',
        'mobile-title': '24px',
        'xxs': '10px',
        '5.5xl': '56px'
      },
      padding: {
        '42': '168px',
      },
      margin: {
        'rSlide-lg': '405px',
        '22': '88px',
      },
      width: {
        'rSlide-lg': '435px',
        'rSlide-md': '309px',
        '75': '75px',
        '23': '370px',
        '30': '320px',
        'tab-btn': '125px',
        'tab-xs-btn': '100px',
        '45': '720px',
        'cart-card-lg': '170px',
        'cart-card-md': '390px',
        'cart-order-summary-lg': '435px',
        'cart-register-md': '399px',
        'cart-order-summary-md': '309px',
        'cart-register-md-inner': '335px',
        '81': '81px',
        '104': '104px',
        '119': '119px',
        '130': '130px',
        '210': '210px',
        '230': '230px',
        '240': '240px',
        '243': '243px',
        '248': '248px',
        '250': '250px',
        '260': '260px',
        '270': '270px',
        '285': '285px',
        '345': '345px',
        '369': '369px',
        '450': '450px',
        '460': '460px',
        '470': '470px',
        '476': '476px',
        '496': '496px',
        '514': '514px',
        '546': '546px',
        '560': '560px',
        '570': '570px',
        '580': '580px',
        '608': '608px',
        '610': '610px',
        '642': '642px',
        '690': '690px',
        '720': '720px',
        '722': '722px',
        '768': '768px',
        '960': '960px'
      },
      minWidth: {
        '90': '90px',
        '100': '100px',
        '150': '150px',
        '4xl': '850px',
      },
      maxWidth: {
        //DEFAULT STUB START >>
        0: '0px',
        xs: '320px',
        sm: '384px',
        md: '448px',
        lg: '512px',
        xl: '576px',
        '2xl': '672px',
        '3xl': '768px',
        '4xl': '896px',
        '5xl': '1024px',
        '6xl': '1152px',
        '7xl': '1280px',
        //DEFAULT STUB END <<
        'screen-desk': '1170px',
        'large-desk': '1176px',
        '64': '64px',
        '152': '152px',
        '198': '198px',
        '200': '200px',
        '390': '300px',
        '1240': '1240px',
        '1440': '1440px',
      },
      lineHeight: {
        //DEFAULT STUB START >>
        3: '12px',
        4: '16px',
        5: '20px',
        6: '24px',
        7: '28px',
        8: '32px',
        9: '36px',
        10: '40px',
        // DEFAULT STUB END <<
        'tablet-title': '1.14',
        'mobile-title': '1.17',
        'desktop-title': '1.33',
        '19': '19px',
        '6.5': '27px',
        '56': '56px'
      },
      minHeight: {
        '40': '40px',
        '100': '100px',
        '478': '478px',
        '291': '291px',
        '670': '670px',
      },
      maxHeight: {
        'header-75': '75px',
        '456': '456px'
      },
      height: {
        'cart-card-lg': '432px',
        'cart-card-md': '192px',
        '72': '72px',
        '75': '75px',
        '100': '100px',
        '104': '104px',
        '130': '130px',
        '184': '184px',
        '216': '216px',
        '396': '396px',
        '400': '400px',
        '500': '500px',
        '90-screen': '90vh',
        '50-screen': '50vh',
        '35': '35px',
        '610': '610px',
        '720': '720px',
        '770': '770px',
        '270': '270px',
      },
      transformOrigin: {
        "0": "0%",
      },
      zIndex: {
        "-1": "-1",
        "999": "999",
        "-999": "-999",
        "1": "1",
        "2": "2",
      },
      colors: {
        secondary: '#7befb2',
        grey: {
          10: '#f4f2f2',
          20: '#d7d5d4',
          30: '#b0b0b0',
          40: '#535354',
          50: '#f7f6f6',
          60: '#848484',
          70: '#3c3b3b',
        },
        mint: {
          10: '#eaf2ee',
          20: '#cfe9db',
          40: '#7f9194',
          30: '#20e47d',
        },
        error: {
          10: '#eeb1c1',
          20: '#ed5d71',
        },
        information: {
          text: '#fdfad1',
          bg: '#d4c0e8',
        },
        white: '#fff',
        success: '#cfe9db',
        yellow: '#ffea6c',
        violet: '#6360ef',
        lightviolet: '#a36fef',
        darkest: '#000000'
      },
      screens: {
        xs: '480px',
        sm: '640px',
        // => @media (min-width: 640px) { ... }
        // smd: '711px',
        // // => @media (min-width: 768px) { ... }
        md: '768px',
        // => @media (min-width: 768px) { ... }
        mlg: '1000px',
        // => @media (min-width: 1000px) { ... }
        lg: '1170px',
        // => @media (min-width: 1170px) { ... }
        xl: '1280px',
        // => @media (min-width: 1280px) { ... }
        '2xl': '1536px',
      },
      spacing: {
        //DEFAULT STUB START >>
        px: '1px',
        0: '0px',
        0.5: '2px',
        1: '4px',
        1.5: '6px',
        2: '8px',
        2.5: '10px',
        3: '12px',
        3.5: '14px',
        4: '16px',
        5: '20px',
        6: '24px',
        7: '28px',
        8: '32px',
        9: '36px',
        10: '40px',
        11: '44px',
        12: '48px',
        14: '56px',
        16: '64px',
        20: '80px',
        24: '96px',
        28: '112px',
        32: '128px',
        36: '144px',
        40: '160px',
        44: '176px',
        48: '192px',
        52: '208px',
        56: '224px',
        60: '240px',
        64: '256px',
        72: '288px',
        80: '320px',
        96: '384px',
        // DEFAULT STUB END
        '15': '60px',
        '17': '72px',
        '39': '39px',
        '5px': '5px',
        '21': '21px',
        '31': '31px',
        '135': '135px',
        '33px': '33px',
        '55px': '55px',
        '25px': '25px',
        '19px': '19px',
        '29px': '29px',
        '15px': '15px',
      },
      //NOTE : TO REMOVE UNUSED RADIUS SETTINGS ON STYLE FINALIZATION
      borderRadius: {
        none: '0px',
        sm: '2px',
        DEFAULT: '4px',
        md: '6px',
        lg: '8px',
        xl: '12px',
        '2xl': '16px',
        '3xl': '24px',
        full: '9999px',
      },
      //NOTE : TO REMOVE UNUSED RADIUS SETTINGS ON STYLE FINALIZATION
      borderWidth: {
        DEFAULT: '1px',
        0: '0px',
        2: '2px',
        4: '4px',
        8: '8px',
      },
      fontFamily: {
        'primary-font-light': 'Akkurat Pro Light',
        'primary-font-light-italic': 'Akkurat Pro Light Italic',
        'primary-font-regular': 'Akkurat Pro Regular',
        'primary-font-bold': 'Akkurat Pro Bold',
        'primary-font-bold-italic': 'Akkurat Pro Bold Italic',
        'primary-font-italic': 'Akkurat Pro Italic',
        'secondary-font-italic': 'Plantin MT Pro Italic',
        'secondary-font-regular': 'Plantin MT Pro Regular',
        'secondary-font-bold': 'Plantin MT Pro Bold',
        'Akkurat': 'Akkurat',
        'Plantin': 'Plantin'
      },
      rotate: {
        135: '135deg',
        225: '225deg',
        316: '316deg'
      },
    }
  },
  variants: {
    extend: {
      display: ["group-hover"],
      borderColor: ["responsive", "hover", "focus", "focus-within"]
    }
  },
  plugins: [
    require('@tailwindcss/forms'),
  ]
}
