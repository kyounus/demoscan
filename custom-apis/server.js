/* global BigInt */

import Koa from 'koa';
import Router from '@koa/router';
import bodyParser from 'koa-bodyparser';

import addToCart from './add-to-cart';
import prismicPreview from './prismic-preview';
import prismicShare from './prismic-share';
import pages from './pages';
import careersPostings from './careers-postings';
import magento from './magento';
import powerReviews from './power-reviews';
import forms from './forms';
import subscribe from './subscribe';
import prismicProductsCatalog from './prismic-products-catalog';
import prismicImage from './prismic-image';

export default (options = {}) => {
	const app = new Koa();
	const router = new Router();

	app.use(bodyParser());

	app.use(async (ctx, next) => {
		const start = process.hrtime.bigint();
		await next();
		const diff = Number((process.hrtime.bigint() - start) / BigInt(1000)) / 1000;
		if (typeof ctx.body === 'object') {
			ctx.body.customApisServerTime = diff;
		}
		if (diff > 20) {
			// eslint-disable-next-line no-console
			console.log(`${ctx.method} ${ctx.url} - ${diff.toFixed(0)}ms`);
		}
		ctx.append('Server-Timing', `total;dur=${diff.toFixed(3)}`);
	});

	router.post('/add-to-cart', addToCart);
	router.get('/prismic-preview', prismicPreview);
	router.get('/prismic-share/(.*)', prismicShare);
	router.use('/pages', pages.routes(), pages.allowedMethods());
	router.get('/careers-postings', careersPostings);
	router.use('/magento', magento.routes(), magento.allowedMethods());
	router.get('/power-reviews/(.*)', powerReviews);
	router.use('/forms', forms.routes(), forms.allowedMethods());
	router.post('/subscribe', subscribe);
	router.get('/prismic-products-catalog', prismicProductsCatalog);
	router.get('/prismic-image/(.*)', prismicImage);

	app.use(router.routes()).use(router.allowedMethods());

	return app;
};
