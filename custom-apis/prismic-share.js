export default async function prismicShare(ctx) {
	const url = ctx.params[0];
	ctx.assert(url, 'URL is required');

	ctx.cookies.set('io.prismic.molekulePreview', url, { path: '/', httpOnly: false });
	ctx.redirect(url);
}
