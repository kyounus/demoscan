if (process.env.NODE_ENV === 'nodemon') require('dotenv').config();

// eslint-disable-next-line import/no-extraneous-dependencies
const { default: server } = require('esm')(module, {})('./server');

const app = server();

app.listen(3001);
