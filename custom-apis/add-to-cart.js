import Axios from 'axios';

function createAxios({ urlPrefix }) {
	const magentoUrl = process.env.magentoUrl || process.env.MAGENTO_URL;
	const instance = Axios.create({
		baseURL: `${magentoUrl}${urlPrefix}rest/V1`,
	});

	for (let method of ['get', 'post', 'put', 'delete', 'head', 'options', 'request']) {
		instance['$' + method] = function() {
			return this[method].apply(this, arguments).then((res) => res && res.data);
		}.bind(instance);
	}

	return instance;
}

const axiosInstances = {
	'': createAxios({ urlPrefix: '' }),
	'ca-en/': createAxios({ urlPrefix: 'ca-en/' }),
};

export default async function addToCart(ctx) {
	const { body } = ctx.request;

	const axios = axiosInstances[body.urlPrefix];
	ctx.assert(axios, 'urlPrefix is invalid');
	let { cartHash, cartItems: items, cartItem } = body;
	if (cartItem && !items) {
		// temporary for backward compatibility
		items = [cartItem];
	}
	ctx.assert(items && Array.isArray(items) && items.length > 0, 'No items passed');

	if (!cartHash) {
		cartHash = await axios.$post('/guest-carts');
	}

	let lastItem;

	for (const item of items) {
		lastItem = await axios.$post(`/guest-carts/${cartHash}/items`, {
			cartItem: {
				...item,
				quote_id: cartHash,
			},
		});
	}

	// based on email from Mark (In the meantime we recommend a temporary fix on the frontend. ..., 2019-10-02/03)
	// we update quantity to the same to force totals/discount recalculation on the backend
	await axios.$put(`/guest-carts/${cartHash}/items/${lastItem.item_id}`, {
		cartItem: {
			item_id: lastItem.item_id,
			sku: lastItem.sku,
			qty: lastItem.qty,
			quote_id: cartHash,
			product_option: lastItem.product_option,
		},
	});

	const carts = await Promise.all([
		axios.$get(`/guest-carts/${cartHash}/items`),
		axios.$get(`/guest-carts/${cartHash}/totals`),
	]);

	ctx.body = { cartHash, cart: { items: carts[0], totals: carts[1] } };
}
