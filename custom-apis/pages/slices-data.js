import axios from 'axios';

const {
	POWERREVIEWS_READ_API_KEY: readApiKey,
	POWERREVIEWS_MERCHANT_ID_US: merchantIdUS,
	POWERREVIEWS_MERCHANT_ID_CA: merchantIdCA,
} = process.env;

const locales = {
	'en-us': 'en_US',
	'en-ca': 'en_CA',
	'en-gb': 'en_GB',
};

const reviewsPageIds = ['molekule-air', 'molekule-air-mini', 'molekule-air-mini-plus', 'molekule-air-pro'];

const formatProductReview = (review) => ({
	results: review.results[0].reviews,
	rollup: review.results[0].rollup,
	pageId: review.results[0].page_id,
	paging: review.paging,
});

const fetchReviewsSnippet = async (ctx, pageId) => {
	const locale = locales[ctx.params.locale] || 'en_US';
	const merchantId = locale === 'en_CA' ? merchantIdCA : merchantIdUS;
	try {
		let {
			data: snippet,
		} = await axios.get(
			`/custom-apis/power-reviews/m/${merchantId}/l/${locale}/product/${pageId}/snippet?apikey=${readApiKey}`,
			{ baseURL: process.env.CUSTOM_APIS_BASE_URL || 'http://localhost:3000' }
		);

		snippet = snippet.results[0];

		return Promise.resolve(snippet);
	} catch (err) {
		console.error(err.message);
	}
};

const fetchReviewsSnippets = () => async (ctx) => {
	return {
		objectName: 'reviewsSnippets',
		data: (await Promise.all(reviewsPageIds.map(async (pageId) => await fetchReviewsSnippet(ctx, pageId)))).filter(
			Boolean
		),
	};
};

const fetchProductReviews = (config) => async (ctx) => {
	if (!Array.isArray(config)) config = [config];
	const locale = locales[ctx.params.locale] || 'en_US';
	const merchantId = locale === 'en_CA' ? merchantIdCA : merchantIdUS;
	const urls = config.map(
		({ pageId, sort = 'HighestRating', pageFrom = 0, pageSize = 8 }) =>
			`/custom-apis/power-reviews/m/${merchantId}/l/${locale}/product/${pageId}/reviews?apikey=${readApiKey}&sort=${sort}&paging.from=${pageFrom}&paging.size=${pageSize}`
	);

	try {
		const reviews = await Promise.all(
			urls.map(
				async (url) =>
					(await axios.get(url, { baseURL: process.env.CUSTOM_APIS_BASE_URL || 'http://localhost:3000' })).data
			)
		);

		return {
			objectName: 'productReviews',
			data:
				reviews.length === 1
					? formatProductReview(reviews[0])
					: Object.fromEntries(config.map((c, i) => [c.pageId, formatProductReview(reviews[i])])),
		};
	} catch (err) {
		console.error(err.message);
	}
};

const slicesWithReviewsSnippets = [
	'ProductsV2',
	'Review',
	'Reviews',
	'AddToCartV2',
	'products_v2',
	'review',
	'reviews',
	'add_to_cart_v2',
];

const documentsWithProductReviews = {
	air_purifier_mh1: ['molekule-air'],
	homepage: ['molekule-air-mini', 'molekule-air-mini-plus'],
	minimo: ['molekule-air-mini', 'molekule-air-mini-plus'],
	fda_cleared_mini: ['molekule-air-mini', 'molekule-air-mini-plus'],
	air_pro_rx: ['molekule-air'],
	reviews_air: ['molekule-air'],
	reviews_air_mini: ['molekule-air-mini'],
	reviews_air_mini_plus: ['molekule-air-mini-plus'],
	reviews_air_pro: ['molekule-air-pro'],
	'air-purifier-air': ['molekule-air'],
	'air-purifier-air-pro': ['molekule-air'],
	'air-purifier-air-mini': ['molekule-air-mini', 'molekule-air-mini-plus'],
};

export const slicesDataLoaders = {
	...Object.fromEntries(slicesWithReviewsSnippets.map((sliceType) => [sliceType, fetchReviewsSnippets()])),
	...Object.fromEntries(
		Object.entries(documentsWithProductReviews).map(([doc, ids]) => [
			doc,
			fetchProductReviews(ids.map((id) => ({ pageId: id }))),
		])
	),
};
