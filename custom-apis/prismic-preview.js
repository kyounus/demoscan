import Prismic from 'prismic-javascript';
import { prismicLinkResolver } from '../utils/prismic-helpers';

const { PRISMIC_ENDPOINT, PRISMIC_ACCESS_TOKEN } = process.env;

export default async function prismicPreview(ctx) {
	const { token, documentId } = ctx.query;
	ctx.append('Cache-Control', 'private, no-cache, no-store');
	ctx.assert(token, 'Token is required');
	ctx.assert(documentId, 'documentId is required');

	const api = await Prismic.getApi(PRISMIC_ENDPOINT, { req: ctx.request, accessToken: PRISMIC_ACCESS_TOKEN });
	const url = await api.getPreviewResolver(token, documentId).resolve(prismicLinkResolver, '/');

	// with cookie set here we are already loading preview page,
	// without setting cookie here, first normal page would load and then Prismic Toolbar
	// would set cookie and reload
	ctx.cookies.set(Prismic.previewCookie, token, { path: '/', httpOnly: false });
	ctx.cookies.set('fromCustomApisPrismicPreview', 'true', { path: '/', httpOnly: true });
	ctx.redirect(url);
}
