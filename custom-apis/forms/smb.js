import Joi from '@hapi/joi';
import sgMail from '@sendgrid/mail';
import axios from 'axios';
import pSettle from 'p-settle';
import pRetry from 'p-retry';
import { validateBody } from '../validation';
import { camelToSnakeCase, pascalCase } from '../../utils';
import { pRetryConfig, now, log, queryStringEncode, getBodyFromForm } from './utils';

const {
	FORMS_SMB_ENDPOINT,
	FORMS_SHEETS_SECRET,
	FORMS_SMB_TO,
	FORMS_SMB_FROM,
	FORMS_FROM,
	FORMS_FROM_NAME,
	SG_API_KEY,
	SG_SMB_NOMINATION_TEMPLATE_ID,
	SG_SMB_JOIN_TEMPLATE_ID,
} = process.env;

sgMail.setApiKey(SG_API_KEY);

const googleSheet = FORMS_SMB_ENDPOINT && FORMS_SHEETS_SECRET;

const updatePropNames = (obj, propNamesMapping) =>
	Object.fromEntries(Object.entries(obj).map(([key, val]) => [propNamesMapping[key] || pascalCase(key), val]));

const getGoogleSheetsData = (formLog) => {
	const originalToGoogleSheetProps = { businessUrl: 'URL', source: 'source', channelSource: 'channelSource' };

	const form = updatePropNames(formLog.form, originalToGoogleSheetProps);

	return { ...formLog, form };
};

const smbFormSchema = Joi.object({
	recaptchaResponse: Joi.string().required(),
	form: Joi.object()
		.pattern(
			Joi.string(),
			Joi.object({
				label: Joi.string().required(),
				value: Joi.alternatives().try(Joi.string().allow(''), Joi.number()).required(),
			})
		)
		.required(),
	type: Joi.string().valid('Consumer', 'SMB Retail').required(),
}).required();

export default async function smbFormMiddleware(ctx) {
	await validateBody(ctx, smbFormSchema);

	const { form, type } = ctx.request.body;

	const body = getBodyFromForm(form);

	const res = { success: true };

	const formLog = { formReceivedTimestamp: now(), form, type };

	log('forms/smb/received', formLog);

	try {
		const recaptchaRes = await axios.post(
			'https://www.google.com/recaptcha/api/siteverify',
			queryStringEncode({
				secret: process.env.RECAPTCHA_SECRET_KEY,
				response: ctx.request.body.recaptchaResponse,
			}),
			{ timeout: 10000 }
		);

		if (!(recaptchaRes.data && recaptchaRes.data.success)) {
			throw new Error(`Error when verifying recaptcha: ${recaptchaRes.data && recaptchaRes.data['error-codes']}`);
		}

		// TODO: those `your_[email|first_name|last_name]` fields are `not optimal` and require refactoring
		const dynamicTemplateData = Object.fromEntries(
			Object.entries(form).map(([key, { value }]) => [
				key === 'yourEmail' ? 'email' : camelToSnakeCase(key) || key,
				value,
			])
		);

		if (!dynamicTemplateData.first_name && dynamicTemplateData.your_first_name) {
			dynamicTemplateData.first_name = dynamicTemplateData.your_first_name;
		}

		if (!dynamicTemplateData.last_name && dynamicTemplateData.your_last_name) {
			dynamicTemplateData.last_name = dynamicTemplateData.your_last_name;
		}

		const emailToUserConfig = {
			to: { email: dynamicTemplateData.email, name: dynamicTemplateData.first_name },
			from: { email: FORMS_SMB_FROM, name: FORMS_FROM_NAME },
			templateId: type === 'Consumer' ? SG_SMB_NOMINATION_TEMPLATE_ID : SG_SMB_JOIN_TEMPLATE_ID,
			dynamic_template_data: dynamicTemplateData,
		};

		const responses = await pSettle(
			[
				pRetry(
					() =>
						sgMail.send({
							to: FORMS_SMB_TO,
							from: { email: FORMS_FROM, name: FORMS_FROM_NAME },
							subject: 'New form submission',
							text: body,
							trackingSettings: { clickTracking: { enable: false } },
						}),
					pRetryConfig
				),
				pRetry(() => sgMail.send(emailToUserConfig), pRetryConfig),
				googleSheet &&
					pRetry(
						() =>
							axios
								.post(
									FORMS_SMB_ENDPOINT,
									{
										googleSheetsSecret: FORMS_SHEETS_SECRET,
										...getGoogleSheetsData(formLog),
									},
									{ timeout: 10000 }
								)
								.then(({ data }) => {
									if (!(typeof data === 'object' && data.success)) {
										throw new Error(`No success returned: ${data}`);
									}

									return data;
								}),
						pRetryConfig
					),
			].filter(Boolean)
		);

		if (responses.some((p) => p.isRejected)) {
			const e = new Error('One of promises failed');
			const reason = responses.find((p) => p.isRejected).reason;

			e.reason = {
				message: reason && reason.message,
				stack: reason && reason.stack,
			};
			e.responses = responses;
			throw e;
		}

		log('forms/smb/success', { formSuccessResponse: responses, ...formLog });
	} catch (e) {
		log('forms/smb/error', {
			formError: { message: e && e.message, stack: e && e.stack, reason: e && e.reason, responses: e && e.responses },
			...formLog,
		});

		console.error(e);
		res.success = false;
	}

	ctx.body = res;
}
