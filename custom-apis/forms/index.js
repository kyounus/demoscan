import Router from '@koa/router';

import purchase from './purchase';
import smb from './smb';

const router = new Router();

router.post('/purchase', purchase);
router.post('/smb', smb);

export default router;
