import jsonStringifySafe from 'json-stringify-safe';

export const pRetryConfig = {
	retries: 4,
	onFailedAttempt: (error) => {
		console.error(`Attempt ${error.attemptNumber} failed. There are ${error.retriesLeft} retries left.`);
		console.error(error);
	},
};

export const now = () => Math.floor(Date.now() / 1000);

export function log(kind, obj) {
	// eslint-disable-next-line no-console
	console.log(jsonStringifySafe({ formEvent: kind, ...obj }));
}

export function queryStringEncode(obj) {
	return Object.entries(obj)
		.map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
		.join('&');
}

export const getBodyFromForm = (form) =>
	Object.entries(form)
		.map(([_, { label, value }]) => `${label}:\n${value}`)
		.join('\n\n');
