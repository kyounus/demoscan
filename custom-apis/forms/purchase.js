import Joi from '@hapi/joi';
import sgMail from '@sendgrid/mail';
import axios from 'axios';
import pSettle from 'p-settle';
import pRetry from 'p-retry';
import { validateBody } from '../validation';
import { pRetryConfig, now, log, queryStringEncode, getBodyFromForm } from './utils';

const {
	SG_API_KEY,
	SG_PURCHASE_INQUIRY_TEMPLATE_ID,
	SG_CONTENT_DOWNLOAD_TEMPLATE_ID,
	SG_CRRSA_DOWNLOAD_TEMPLATE_ID,
	ZENDESK_SELL_LEADS_ENDPOINT,
	ZENDESK_SELL_API_KEY,
	ZENDESK_SOURCE_ID,
	ZENDESK_OWNER_ID,
	FORMS_PURCHASE_ENDPOINT,
	FORMS_SHEETS_SECRET,
	FORMS_PURCHASE_TO,
	FORMS_FROM,
	FORMS_FROM_NAME,
} = process.env;

sgMail.setApiKey(SG_API_KEY);

const googleSheet = FORMS_PURCHASE_ENDPOINT && FORMS_SHEETS_SECRET;

const purchaseFormSchema = Joi.object({
	recaptchaResponse: Joi.string().required(),
	form: Joi.object()
		.pattern(
			Joi.string(),
			Joi.object({
				label: Joi.string().required(),
				value: Joi.alternatives().try(Joi.string().allow(''), Joi.number()).required(),
			})
		)
		.required(),
}).required();

const getGoogleSheetsFormLog = (formLog) => {
	const originalToGoogleSheetsProps = {
		first_name: 'firstName',
		last_name: 'lastName',
		organization_name: 'companyName',
		email: 'workEmail',
		phone: 'phoneNumber' || '',
		line1: 'address1' || '',
		postal_code: 'postCode' || '',
		industry: 'category',
		'Other Industry': 'category',
		air_pro_rx_quantity: 'airProRXQuantity' || 0,
		air_pro_quantity: 'airProQuantity' || 0,
		air_quantity: 'airQuantity' || 0,
		air_mini_plus_quantity: 'airMiniPlusQuantity' || 0,
		air_mini_quantity: 'airMiniQuantity' || 0,
		description: 'message',
		channel_source: 'channelSource',
	};

	return {
		...formLog,
		form: {
			...Object.fromEntries(
				Object.entries(formLog.form).map(([key, val]) => [originalToGoogleSheetsProps[key] || key, val])
			),
		},
	};
};

const zendeskAddressFields = ['line1', 'city', 'state', 'postal_code'];
const zendeskProductsFields = [
	'air_pro_rx_quantity',
	'air_pro_quantity',
	'air_quantity',
	'air_mini_quantity',
	'air_mini_plus_quantity',
];
const zendeskCustomFields = ['Other Industry', 'Marketing Opt-in', 'Lead Type'];
const zendeskPropsToExclude = ['channel_source', 'source'];
const formatProductsFieldKey = (key) =>
	key
		.replace(/_/g, ' ')
		.split(' ')
		.map((x) => `${x[0].toUpperCase() + x.slice(1)}`)
		.join(' ');

export default async function purchaseFormMiddleware(ctx) {
	await validateBody(ctx, purchaseFormSchema);

	const { form } = ctx.request.body;

	const body = getBodyFromForm(form);

	const res = { success: true };

	const formLog = { formReceivedTimestamp: now(), form };

	log('forms/purchase/received', formLog);

	try {
		const recaptchaRes = await axios.post(
			'https://www.google.com/recaptcha/api/siteverify',
			queryStringEncode({
				secret: process.env.RECAPTCHA_SECRET_KEY,
				response: ctx.request.body.recaptchaResponse,
			}),
			{ timeout: 10000 }
		);

		if (!(recaptchaRes.data && recaptchaRes.data.success)) {
			throw new Error(`Error when verifying recaptcha: ${recaptchaRes.data && recaptchaRes.data['error-codes']}`);
		}

		const dynamicTemplateData = Object.fromEntries(Object.entries(form).map(([key, { value }]) => [key, value]));

		const zendeskData = {
			source_id: parseInt(ZENDESK_SOURCE_ID),
			owner_id: parseInt(ZENDESK_OWNER_ID),
			address: { country: 'US' },
			custom_fields: { Products: '' },
		};

		Object.entries(form).forEach(([key, { value }]) => {
			if (zendeskAddressFields.includes(key)) {
				zendeskData.address[key] = value;
			} else if (zendeskCustomFields.includes(key)) {
				zendeskData.custom_fields[key] = value;
			} else if (zendeskProductsFields.includes(key)) {
				if (value > 0) {
					const productsFields = zendeskData.custom_fields.Products;

					zendeskData.custom_fields.Products =
						productsFields + (productsFields.length ? `\n` : '') + `${formatProductsFieldKey(key)}: ${value}`;
				}
			} else if (!zendeskPropsToExclude.includes(key)) {
				zendeskData[key] = value;
			}
		});

		const getTemplateId = (value) => {
			switch (value) {
				case 'Purchase Inquiry':
					return SG_PURCHASE_INQUIRY_TEMPLATE_ID;
				case 'Content Download':
					return SG_CONTENT_DOWNLOAD_TEMPLATE_ID;
				default:
					return SG_CRRSA_DOWNLOAD_TEMPLATE_ID;
			}
		};
		const emailToUserConfig = {
			to: { email: dynamicTemplateData.email, name: dynamicTemplateData.first_name },
			from: { email: FORMS_FROM, name: FORMS_FROM_NAME },
			templateId: getTemplateId(form['Lead Type'].value),
			dynamic_template_data: dynamicTemplateData,
		};

		const responses = await pSettle(
			[
				pRetry(
					() =>
						axios.post(
							ZENDESK_SELL_LEADS_ENDPOINT,
							{ data: zendeskData },
							{
								headers: {
									Authorization: `Bearer ${ZENDESK_SELL_API_KEY}`,
								},
							}
						),
					pRetryConfig
				),
				pRetry(
					() =>
						sgMail.send({
							to: FORMS_PURCHASE_TO,
							from: { email: FORMS_FROM, name: FORMS_FROM_NAME },
							subject: 'New form submission',
							text: body,
							trackingSettings: { clickTracking: { enable: false } },
						}),
					pRetryConfig
				),
				pRetry(() => sgMail.send(emailToUserConfig), pRetryConfig),
				googleSheet &&
					pRetry(
						() =>
							axios
								.post(
									FORMS_PURCHASE_ENDPOINT,
									{
										googleSheetsSecret: FORMS_SHEETS_SECRET,
										...getGoogleSheetsFormLog(formLog),
									},
									{ timeout: 10000 }
								)
								.then(({ data }) => {
									if (!(typeof data === 'object' && data.success)) {
										throw new Error(`No success returned: ${data}`);
									}

									return data;
								}),
						pRetryConfig
					),
			].filter(Boolean)
		);

		if (responses.some((p) => p.isRejected)) {
			const e = new Error('One of promises failed');
			const reason = responses.find((p) => p.isRejected).reason;

			e.reason = {
				message: reason && reason.message,
				stack: reason && reason.stack,
			};
			e.responses = responses;
			throw e;
		}

		log('forms/purchase/success', { formSuccessResponse: responses, ...formLog });
	} catch (e) {
		log('forms/purchase/error', {
			formError: { message: e && e.message, stack: e && e.stack, reason: e && e.reason, responses: e && e.responses },
			...formLog,
		});

		console.error(e);
		res.success = false;
	}

	ctx.body = res;
}
