import LRUCache from 'lru-cache';
import Axios from 'axios';

const cache = new LRUCache({
	max: 50 * 1024 * 1024, // 50 MB
	length: (val) => val.length,
	noDisposeOnSet: true,
	dispose: (key, val) => {
		console.log(
			`Cached Request: Item ${key} (${val.length} bytes, created ${nowSeconds() - val.created}s ago) removed from cache`
		);
	},
});

const inProgress = new Map();

const nowSeconds = () => Math.floor(Date.now() / 1000);

function fetchUrl(url, { ttl, grace }) {
	// console.log('fetch', url);
	if (inProgress.has(url)) {
		// console.log('in progress', url);
		return inProgress.get(url);
	}

	if (typeof ttl !== 'number' || typeof grace !== 'number') throw new Error('Cached Request Fetch: invalid params');

	// console.log('request', url);
	const dataPromise = Axios.get(url).then((res) => res.data);
	inProgress.set(url, dataPromise);
	dataPromise
		.then(
			(data) => {
				const now = nowSeconds();

				cache.set(
					url,
					{
						body: data,
						created: now,
						expires: nowSeconds() + ttl,
						length: JSON.stringify(data).length,
					},
					(ttl + grace) * 1000
				);
			},
			() => {
				// ignore errors
			}
		)
		.finally(() => {
			inProgress.delete(url);
		});

	return dataPromise;
}

export default async function cachedRequest(url, { ttl = 900, grace = 900 } = {}) {
	const cached = cache.get(url);

	if (cached) {
		const shouldUpdate = cached.expires < nowSeconds();
		if (shouldUpdate) {
			fetchUrl(url, { ttl, grace });
		}

		return cached.body;
	}

	return fetchUrl(url, { ttl, grace });
}
