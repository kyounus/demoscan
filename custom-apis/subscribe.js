import { validateBody } from './validation';
import Joi from '@hapi/joi';
import Axios from 'axios';

const subscribeSchema = Joi.object({
	email: Joi.string().required(),
}).required();

export default async function subscribeMiddleware(ctx) {
	await validateBody(ctx, subscribeSchema);

	const { email } = ctx.request.body;

	const res = await Axios.post(
		`https://api.iterable.com/api/lists/subscribe?apiKey=${process.env.ITERABLE_API_KEY}`,
		{
			listId: Number(process.env.ITERABLE_NEWSLETTER_CAMPAIGN_ID),
			subscribers: [{ email }],
		},
		{ timeout: 10000 }
	);

	if (res.data.failCount) {
		ctx.throw(400, JSON.stringify(res.data));
	}

	ctx.body = res.data;
}
