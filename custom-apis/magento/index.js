import Router from '@koa/router';
import Joi from '@hapi/joi';

import { createValidateParamsMiddleware } from '../validation';
import { initializeAxiosMiddleware } from './axios';
import { ensureAndValidateCartMiddleware } from './cart';
import products from './products';
import { itemsPostValidateBody, itemsPost, itemsPostFast } from './items-post';
import upsells from './upsells';

const router = new Router();

const validateStore = createValidateParamsMiddleware(
	Joi.object({
		store: Joi.valid('US', 'CA').required(),
	})
);

const validateCartHash = createValidateParamsMiddleware(
	Joi.object({
		cartHash: Joi.string().required(),
	})
);

const baseMiddlewares = [validateStore, initializeAxiosMiddleware];

router.get('/:store/products', ...baseMiddlewares, products);
router.post(
	'/:store/cart/:cartHash/items',
	...baseMiddlewares,
	validateCartHash,
	itemsPostValidateBody,
	itemsPostFast,
	ensureAndValidateCartMiddleware,
	itemsPost
);
router.get('/:store/upsells', ...baseMiddlewares, upsells);

export default router;
