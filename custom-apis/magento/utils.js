export const isNotBuild =
	typeof process !== 'undefined' && process.release.name === 'node' && !process.argv.find((arg) => arg === 'build');

export const isDev = process.env.NODE_ENV === 'development';
