import isEqual from 'lodash/isEqual';
import Prismic from 'prismic-javascript';
import { getAxiosInstanceForStore } from './axios';
import { isNotBuild, isDev } from './utils';
import { getProductsData } from './products';
import { CART_HASH_CREATE } from './cart';
import { cartsWithProducts, generateId, createdBuffersPromise } from './items-post';
import invalidateCloudFrontCache from './invalidate-cloudfront-cache';

const localesByStore = {
	US: 'en-us',
	CA: 'en-ca',
};

class UpsellsDataForStore {
	constructor(store, axios) {
		this.store = store;
		this.upsells = [];
		this.axios = axios;
		this.load = this.load.bind(this);
		this.cancelInitialLoad = false;
		this.finishedInitial = false;
		this.loadTime = 0;

		if (isNotBuild && !isDev) {
			this.load(true);
			setTimeout(() => {
				setInterval(this.load, 5 * 60 * 1000);
			}, (10 + Math.floor(Math.random() * 60)) * 1000);
		}
	}

	async load(isInitial = false, isPrismicPreviewReq = false, headersCookie) {
		if (!isPrismicPreviewReq && !isInitial) {
			this.cancelInitialLoad = true;
		}
		const upsells = [];
		const { products } = await getProductsData(this.store);
		const global = await this.axios.get(`/api/${localesByStore[this.store]}/global.json`, {
			headers: { Cookie: headersCookie || '' },
			baseURL: process.env.CUSTOM_APIS_BASE_URL || 'http://localhost:3000',
		});

		if (!global.data.upsells || !global.data.upsells.length) return;

		const { skuSets: prismicGlobalUpsellConfig } = global.data.upsells.reduce(
			(final, { items }) => {
				if (!items || !items.length) return final;

				// TODO: Use the products data from prismic without running into the issue of other environments (not production)
				// // not being able to use this data in magento requests due to "product.productOption" being different in each environment
				// items.forEach(({ product }) => product && product.sku && (final.products[product.sku] = product));
				const skuSet = items.map(({ product }) => product && product.sku).filter(Boolean);
				if (skuSet.length) final.skuSets.push(skuSet);

				return final;
			},
			{ skuSets: [] }
		);

		const upsellCartItemSets = prismicGlobalUpsellConfig
			.map((skus) => {
				if (skus.length < 2 || !skus.every((sku) => products[sku])) return null;

				const skusObj = {};

				for (let i = 0; i < skus.length; i++) {
					const skusInSkusObj = Object.keys(skusObj);
					const sku = skus[i];
					if (skusInSkusObj.includes(sku)) continue;

					const tempSkus = [...skus];
					let skuCount = 1;
					tempSkus.splice(i, 1);

					if (tempSkus.includes(sku)) {
						tempSkus.forEach((tempSku) => {
							if (sku === tempSku) skuCount++;
						});
					}

					skusObj[sku] = skuCount;
				}
				return skusObj;
			})
			.filter(Boolean)
			.map((skuObj) =>
				Object.entries(skuObj).map(([sku, qty]) => {
					return { sku, qty, product_option: products[sku].productOption };
				})
			);

		if (upsellCartItemSets.length) {
			if (isInitial) {
				await createdBuffersPromise;
			}

			await Promise.all(
				upsellCartItemSets.map(async (cartItems) => {
					let preCreatedCart;
					if (isInitial) {
						preCreatedCart = cartsWithProducts[generateId({ cartItems })];
					}

					const originalOrder = cartItems.map((cartItem) => cartItem.sku);

					if (!isInitial || !preCreatedCart) {
						return {
							originalOrder,
							...(await this.axios.$post(
								`/custom-apis/magento/${this.store}/cart/${CART_HASH_CREATE}/items`,
								{ cartItems },
								{ baseURL: process.env.CUSTOM_APIS_BASE_URL || 'http://localhost:3000' }
							)),
						};
					} else {
						return {
							originalOrder,
							...(await cartsWithProducts[generateId({ cartItems })].firstItemInBufferPromise),
						};
					}
				})
			)
				.then((res) => {
					if (isInitial && this.cancelInitialLoad) {
						this.finishedInitial = true;
						return;
					}

					upsells.push(
						...res.map(({ originalOrder, cart: { items, totals } }) => ({
							items: originalOrder
								.map((originalSku) => items.find((item) => item.sku === originalSku))
								.map(({ sku, qty, name, price }) => ({
									sku,
									qty,
									name,
									price,
								})),
							total: totals.grand_total,
							discount: totals.discount_amount,
						}))
					);

					if (isPrismicPreviewReq) return;

					if (!this.finishedInitial) this.finishedInitial = true;

					if (this.upsells.length && !isEqual(this.upsells, upsells)) {
						console.log(`Upsells difference found for ${this.store}, clearing cache`);
						invalidateCloudFrontCache();
					}

					this.loadTime = Date.now();
					this.upsells = upsells;
				})
				.catch((e) => {
					console.error(e);
				});

			if (isPrismicPreviewReq) return { upsells, upsellsTime: Date.now() };
		}
	}

	async get(prismicPreviewCookie, headersCookie) {
		if (prismicPreviewCookie) {
			return await this.load(false, true, headersCookie);
		}

		if (!this.upsells.length && !this.finishedInitial) {
			await this.load();
		}

		return { upsells: this.upsells, upsellsTime: this.loadTime };
	}
}

const upsellsData = {
	US: new UpsellsDataForStore('US', getAxiosInstanceForStore('US')),
	CA: new UpsellsDataForStore('CA', getAxiosInstanceForStore('CA')),
};

export default async function upsells(ctx) {
	ctx.body = await upsellsData[ctx.params.store].get(ctx.cookies.get(Prismic.previewCookie), ctx.headers.cookie);
}
