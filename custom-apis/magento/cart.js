import CartsBuffer from './carts-buffer';
import { isNotBuild, isDev } from './utils';

export const CART_HASH_CREATE =
	(process.env.magento && process.env.magento.cartHashCreate) || process.env.MAGENTO_CART_HASH_CREATE;

if (!CART_HASH_CREATE) {
	throw new Error('Magento cart hash create not provided');
}

class GuestCartBuffer extends CartsBuffer {
	constructor(store) {
		super({ store, id: 'empty' });
	}

	async refill() {
		return createCart(this.axios);
	}
}

const guestCartsBuffers = {
	US: new GuestCartBuffer('US'),
	CA: new GuestCartBuffer('CA'),
};

if (isNotBuild && !isDev) {
	guestCartsBuffers.US.scheduleRefillIfNecessary();
	guestCartsBuffers.CA.scheduleRefillIfNecessary();
}

export async function getOrCreateCart(store, axios) {
	return guestCartsBuffers[store].get() || createCart(axios);
}

export async function createCart(axios) {
	return axios.$post('/guest-carts');
}

export async function ensureAndValidateCartMiddleware(ctx, next) {
	let { cartHash } = ctx.params;

	if (cartHash === CART_HASH_CREATE) {
		cartHash = await getOrCreateCart(ctx.params.store, ctx.axiosMagento);
	}

	ctx.state.cartHash = cartHash;

	await next();

	if (ctx.params.cartHash !== cartHash && typeof ctx.body === 'object') {
		ctx.body.cartHash = cartHash;
	}
}

export async function loadCartByHash(axios, hash) {
	const carts = await Promise.all([
		axios.$get(`/guest-carts/${hash}`),
		axios.$get(`/guest-carts/${hash}/items`),
		axios.$get(`/guest-carts/${hash}/totals`),
	]);
	return { cart: carts[0], items: carts[1], totals: carts[2] };
}

export async function loadCart(ctx) {
	const { axiosMagento: axios } = ctx;
	const { cartHash } = ctx.state;
	return loadCartByHash(axios, cartHash);
}
