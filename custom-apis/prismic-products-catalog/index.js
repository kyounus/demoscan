import { getProductsData } from '../magento/products';

const pageSize = 50;
const stores = ['US', 'CA'];

const pipe = (...fns) => (x) => fns.reduce((y, f) => f(y), x);

// Format the products in the format that Prismic needs for integration fields
const formatProducts = (lastUpdate, store) => (products) =>
	Object.entries(products).map(([sku, val]) => ({
		id: String(val.id),
		title: `[${store}] ${val.name}`,
		description: `sku: ${val.sku}, type: ${val.type}`,
		image_url: val.image.url,
		last_update: lastUpdate,
		blob: val,
	}));

// The main devices have a bundle and a simple version
// The bundle version is what the frontend uses to add to the cart
// This function filters out the simple versions
const filterProducts = (products) => {
	const bundleProducts = products
		.filter(({ blob: { type } }) => type === 'bundle')
		.map(({ blob: { name } }) => name.trim());

	return products.filter(({ blob: { type, name } }) => !(type === 'simple' && bundleProducts.includes(name.trim())));
};

// Put bundle types at the top of the array
const sortProducts = (products) => {
	return products.sort((next, prev) => {
		const isNextBundle = next.blob.type === 'bundle';
		const isPrevBundle = prev.blob.type === 'bundle';

		return isNextBundle === isPrevBundle ? 0 : isNextBundle ? -1 : 1;
	});
};

export default async (ctx) => {
	let { page = 1 } = ctx.query;
	page = Number(page);
	const response = await Promise.all(stores.map(getProductsData));

	const results = response.reduce(
		(final, { products, productsLastUpdate }, i) => [
			...final,
			...pipe(formatProducts(productsLastUpdate, stores[i]), filterProducts, sortProducts)(products),
		],
		[]
	);

	const resultsSize = results.length;

	const currentResults = results.slice(pageSize * (page - 1), pageSize * page);

	ctx.body = { results_size: resultsSize, results: currentResults };
};
